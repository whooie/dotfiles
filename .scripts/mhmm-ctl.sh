#!/bin/bash

thedir="/home/whooie/Stuff/mhmm-topic-queue"
case $1 in
    pull)
        git -C "$thedir" pull
        ;;
    stash)
        git -C "$thedir" stash
        ;;
    commit|push)
        git -C "$thedir" commit -am "${2:-Will}"
        git -C "$thedir" push
        ;;
    write|add|vim)
        vim "$thedir/README.md"
        ;;
    *)
        echo invalid
        ;;
esac
