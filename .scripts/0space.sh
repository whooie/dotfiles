#!/bin/bash

prefix=".wine-basic"
case $1 in
	-sound|--sound)
		prefix=".wine-0space"
		;;
esac

cd /home/whooie/Stuff/Games/0space_ExtraV2
WINEARCH=win32 WINEPREFIX=/home/whooie/$prefix wine /home/whooie/Stuff/Games/0space_ExtraV2/0Space_ExtraV2.exe
