#!/usr/bin/python

"""
Thin wrapper for setting up pulseaudio sinks, sources, and loopbacks using
`pulsectl`, which is available on PyPI.
"""
import pulsectl as pulse
import getopt
import re
import sys
from typing import Union as U
from typing import Tuple as T
from typing import List as L

default_pulse = pulse.Pulse()
default_pulse_info = default_pulse.server_info()
config = type("Config", (), dict(
    main_out = default_pulse.get_sink_by_name(default_pulse_info.default_sink_name),
    main_out_monitor = default_pulse.get_source_by_name(default_pulse_info.default_sink_name+".monitor"),
    main_in = default_pulse.get_source_by_name(default_pulse_info.default_source_name),
    discord = False,
    discord_wait = False,
    operations = []
))()

helptext = f"""
Thin wrapper for setting up pulseaudio sinks, sources, and loopbacks using
`pulsectl`.

Options:
    -D, --discord
        Set sinks, sources, and loopbacks up such that Discord's voice chat will
        pull audio input from the former default source as well as applications
        other than itself, and output audio to the former default sink. This is
        equivalent to
            pamuxer -s NullSink1 -s NullSink2 \\
                -l NullSink1.monitor:{config.main_out.name} \\
                -l NullSink1.monitor:NullSink2 \\
                -l {config.main_in.name}:NullSink2 \\
                -m playStream:{config.main_out.name} \\
                -M recStream:NullSink2.monitor \\
                -O NullSink1 -I NullSink2.monitor
        Discord voice chat should be active before this option is used.

    -W, --discord-wait
        After performing setup for Discord voice chat, wait for user input to
        kill all associated modules and return to the sink/source/module
        configuration from before pamuxer was called.

    -s, --make-sink <sink_name>[:<set_default_sink>:<set_default_source>]
            <sink_name>             : str
            <set_default_sink>      : in {{0,1}}
            <set_default_source>    : in {{0,1}}
        Load a null sink module with name and description set to <sink_name>.
        Optionally set the sink as the default sink and/or its monitor as the
        default source.
    
    -l, --make-loopback <source>:<sink>
            <source>    : int|str
            <sink>      : int|str
        Load a loopback module linking <source> to <sink>. The source and sink
        can be specified as either their indices or names.

    -m, --move-sink-input <sink_input>:<sink>
            <sink_input>    : int|str
            <sink>          : int|str
        Move <sink_input> to <sink>. The sink input and sink can be specified as
        either indices or names.

    -M, --move-source-output <source_output>:<source>
            <source_output> : int|str
            <source>        : int|str
        Move <source_output> to <source>. The source output and source can be
        specified as either indices or names.

    -k, --kill-module <module_index>[,<module_index>,...]
            <module_index>  : int
        Kill (unload) the module(s) with index <module_index>. Must be specified
        by its indices since module names are not unique.

    -O, --set-default-sink <sink>
            <sink>      : int|str
        Set the default sink to be <sink>. The sink can be specified as either
        its index or name.

    -I, --set-default-source <source>
            <source>    : int|str
        Set the default source to be <source>. The source can be specidied as
        either its index or name.

    -p, --print
        Print information on all sinks, sink inputs, sources, source inputs, and
        modules, then exit.

    -P, --print-select <sinks><sources><modules>
            <sinks>     : in {{0,1}}
            <sources>   : in {{0,1}}
            <modules>   : in {{0,1}}
        Print information on all sinks, all sources, all modules, or a subset of
        the three, then exit.

    -h, --help
        Print this text and exit.
"""

#def run(command: t.List[str], **kwargs) -> sp.CompletedProcess:
#    X = sp.run(command, **kwargs)
#    if X.returncode > 0:
#        raise Exception(f"Subprocess exited with error code > 0:\n  command: {command}")
#    return X

def get_module(index: int, P: pulse.Pulse=None) -> pulse.PulseModuleInfo:
    _P = default_pulse if P is None else P
    reslist = list(filter(lambda x: x.index == index, _P.module_list()))
    if len(reslist) > 0:
        return reslist[0]
    else:
        raise pulse.PulseIndexError(index)

def get_sink(sink: U[int, str], P: pulse.Pulse=None) -> pulse.PulseSinkInfo:
    _P = default_pulse if P is None else P
    if isinstance(sink, int):
        return _P.sink_info(sink)
    elif isinstance(sink, str):
        return _P.get_sink_by_name(sink)
    else:
        raise Exception("Invalid type for arg `sink`")

def get_sink_input(sink_input: U[int, str], P: pulse.Pulse=None) \
        -> pulse.PulseSinkInputInfo:
    _P = default_pulse if P is None else P
    if isinstance(sink_input, int):
        return _P.sink_input_info(sink_input)
    elif isinstance(sink_input, str):
        reslist = list(filter(lambda x: x.name == sink_input, _P.sink_input_list()))
    else:
        raise Exception("Invalid type for arg `sink_input`")
    if len(reslist) > 0:
        return reslist[0]
    else:
        raise pulse.PulseIndexError(sink_input)

def get_inputs_by_sink(sink: pulse.PulseSinkInfo, P: pulse.Pulse=None) \
        -> L[pulse.PulseSinkInputInfo]:
    _P = default_pulse if P is None else P
    return list(filter(lambda x: x.sink == sink.index, _P.sink_input_list()))

def get_source(source: U[int, str], P: pulse.Pulse=None) -> pulse.PulseSourceInfo:
    _P = default_pulse if P is None else P
    if isinstance(source, int):
        return _P.source_info(source)
    elif isinstance(source, str):
        return _P.get_source_by_name(source)
    else:
        raise Exception("Invalid type for arg `source`")

def get_source_output(source_output: U[int, str], P: pulse.Pulse=None) \
        -> pulse.PulseSourceOutputInfo:
    _P = default_pulse if P is None else P
    if isinstance(source_output, int):
        return _P.source_output_info(source_output)
    elif isinstance(source_output, str):
        reslist = list(filter(lambda x: x.name == source_output, _P.source_output_list()))
    else:
        raise Exception("Invalid type for arg `source_output`")
    if len(reslist) > 0:
        return reslist[0]
    else:
        raise pulse.PulseIndexError(source_output)

def get_outputs_by_source(source: pulse.PulseSourceInfo, P: pulse.Pulse=None) \
        -> L[pulse.PulseSourceOutputInfo]:
    _P = default_pulse if P is None else P
    return list(filter(lambda x: x.source == source.index, _P.source_output_list()))

def make_null_sink(name: str, P: pulse.Pulse=None, printflag=False) \
        -> T[pulse.PulseModuleInfo, pulse.PulseSinkInfo, pulse.PulseSourceInfo]:
    _P = default_pulse if P is None else P
    M = _P.module_load(
        "module-null-sink",
        f"sink_name={name} sink_properties=device.description={name}"
    )
    module = get_module(M)
    sink = _P.get_sink_by_name(name)
    monitor = _P.get_source_by_name(name+".monitor")
    if printflag:
        print(f"Create null sink '{name}':")
        print(f"  module index: {module.index}")
        print(f"  sink index: {sink.index}")
        print(f"  monitor index: {monitor.index}")
    return module, sink, monitor

def make_loopback(source: pulse.PulseSourceInfo, sink: pulse.PulseSinkInfo,
        P: pulse.Pulse=None, printflag=False) -> pulse.PulseModuleInfo:
    _P = default_pulse if P is None else P
    index = _P.module_load(
        "module-loopback",
        f"source={source.name} sink={sink.name}"
    )
    module = get_module(index)
    if printflag:
        print(f"Create loopback:")
        print(f"  module index: {index}")
        print(f"  source: '{source.name}' ({source.index})")
        print(f"  sink: '{sink.name}' ({sink.index})")
    return module

def kill_module(*index: int, P: pulse.Pulse=None, printflag=False) -> None:
    _P = default_pulse if P is None else P
    for i in index:
        _P.module_unload(i)
        if printflag:
            print(f"Kill module {i}")
    return None

def move_sink_input(sink_input: pulse.PulseSinkInputInfo,
        sink: pulse.PulseSinkInfo, P: pulse.Pulse=None, printflag=False) \
        -> None:
    _P = default_pulse if P is None else P
    _P.sink_input_move(sink_input.index, sink.index)
    if printflag:
        print(f"Move sink input:")
        print(f"  sink input: '{sink_input.name}' ({sink_input.index})")
        print(f"  sink: '{sink.name}' ({sink.index})")
    return None

def move_source_output(source_output: pulse.PulseSourceOutputInfo,
        source: pulse.PulseSourceInfo, P: pulse.Pulse=None, printflag=False) \
        -> None:
    _P = default_pulse if P is None else P
    _P.source_output_move(source_output.index, source.index)
    if printflag:
        print(f"Move source output:")
        print(f"  source output: '{source_output.name}' ({source_output.index})")
        print(f"  source: '{source.name}' ({source.index})")
    return None

def set_default_sink(sink: pulse.PulseSinkInfo, P: pulse.Pulse=None,
        printflag=False) -> None:
    _P = default_pulse if P is None else P
    _P.sink_default_set(sink)
    if printflag:
        print(f"Set default sink: '{sink.name}' ({sink.index})")
    return None

def set_default_source(source: pulse.PulseSourceInfo, P: pulse.Pulse=None,
        printflag=False) -> None:
    _P = default_pulse if P is None else P
    _P.source_default_set(source)
    if printflag:
        print(f"Set default source: '{source.name}' ({source.index})")
    return None

def get_discord_io(P: pulse.Pulse=None) \
        -> T[pulse.PulseSinkInputInfo, pulse.PulseSourceOutputInfo]:
    _P = default_pulse if P is None else P
    discord_sink_input = get_sink_input("playStream")
    discord_source_output = get_source_output("recStream")
    return discord_sink_input, discord_source_output

def print_info(sinks=True, sources=True, modules=True, P: pulse.Pulse=None) \
        -> None:
    _P = default_pulse if P is None else P
    sink_list = _P.sink_list()
    sink_input_list = _P.sink_input_list()
    source_list = _P.source_list()
    source_output_list = _P.source_output_list()
    module_list = _P.module_list()
    if sinks:
        print("SINKS:")
        for sink in sink_list:
            sink_inputs = get_inputs_by_sink(sink)
            print(f"  name: '{sink.name}'")
            print(f"    description: '{sink.description}'")
            print(f"    index: {sink.index}")
            print(f"    volume: {sink.volume.values}")
            print(f"    mute: {sink.mute}")
            print(f"    inputs:" if len(sink_inputs) > 0 else "    inputs: None")
            for sink_input in sink_inputs:
                print(f"      name: {sink_input.name}")
                print(f"        index: {sink_input.index}")
                print(f"        volume: {sink_input.volume.values}")
                print(f"        mute: {sink_input.mute}")
    if sources:
        print("SOURCES:")
        for source in source_list:
            source_outputs = get_outputs_by_source(source)
            print(f"  name: '{source.name}'")
            print(f"    description: '{source.description}'")
            print(f"    index: {sink.index}")
            print(f"    volume: {sink.volume.values}")
            print(f"    outputs:" if len(source_outputs) > 0 else "    outputs: None")
            for source_output in source_outputs:
                print(f"      name: {source_output.name}")
                print(f"        index: {source_output.index}")
                print(f"        volume: {source_output.volume.values}")
                print(f"        mute: {source_output.mute}")
    if modules:
        print("MODULES:")
        for module in module_list:
            print(f"  name: '{module.name}'")
            print(f"    index: {module.index}")
            print(f"    args: '{module.argument}'")
    return None

def main(argv):
    shortopts = "DWs:l:m:M:k:O:I:pP:h"
    longopts = [
        "discord",
        "discord-wait",
        "make-sink=",
        "make-loopback=",
        "move-sink-input=",
        "move-source-output=",
        "kill-module=",
        "set-default-sink=",
        "set-default-source=",
        "print",
        "print-select=",
        "help"
    ]
    make_sink_pattern = re.compile(r'([a-zA-Z0-9\-_.]+)(\:[01]\:[01])?')
    make_loopback_pattern = re.compile(r'([a-zA-Z0-9\-_.]+)\:([a-zA-Z0-9\-_.]+)')
    move_pattern = re.compile(r'([a-zA-Z0-9\-_.]+)\:([a-zA-Z0-9\-_.]+)')
    kill_pattern = re.compile(r'([,]?[0-9]+)')
    set_default_pattern = re.compile(r'([a-zA-Z0-9\-_.]+)')
    print_select_pattern = re.compile(r'([01])([01])([01])')
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
        for opt, optarg in opts:
            if opt in ["-D", "--discord"]:
                config.discord = True

            elif opt in ["-W", "--discord-wait"]:
                config.discord = True
                config.discord_wait = True

            elif opt in ["-s", "--make-sink"]:
                match = make_sink_pattern.match(optarg)
                if match:
                    config.operations.append((
                        0,
                        match.group(1),
                        bool(match.group(2)[1]) if match.group(2) else False,
                        bool(match.group(2)[3]) if match.group(2) else False
                    ))
                else:
                    raise Exception(f"Error parsing option '{opt} {optarg}': invalid format")

            elif opt in ["-l", "--make-loopback"]:
                match = make_loopback_pattern.match(optarg)
                if match:
                    try:
                        source = int(match.group(1))
                    except ValueError:
                        source = match.group(1)
                    try:
                        sink = int(match.group(2))
                    except ValueError:
                        sink = match.group(2)
                    config.operations.append((
                        1,
                        source,
                        sink
                    ))
                else:
                    raise Exception(f"Error parsing option '{opt} {optarg}': invalid format")

            elif opt in ["-m", "--move-sink-input"]:
                match = move_pattern.match(optarg)
                if match:
                    try:
                        sink_input = int(match.group(1))
                    except ValueError:
                        sink_input = match.group(1)
                    try:
                        sink = int(match.group(2))
                    except ValueError:
                        sink = match.group(2)
                    config.operations.append((
                        2,
                        sink_input,
                        sink
                    ))
                else:
                    raise Exception(f"Error parsing option '{opt} {optarg}': invalid format")

            elif opt in ["-M", "--move-source-output"]:
                match = move_pattern.match(optarg)
                if match:
                    try:
                        source_output = int(match.group(1))
                    except ValueError:
                        source_output = match.group(1)
                    try:
                        source = int(match.group(2))
                    except ValueError:
                        source = match.group(2)
                    config.operations.append((
                        3,
                        source_output,
                        source
                    ))
                else:
                    raise Exception(f"Error parsing option '{opt} {optarg}': invalid format")

            elif opt in ["-k", "--kill-module"]:
                matches = kill_pattern.findall(optarg)
                if len(matches) > 0:
                    for group in matches:
                        config.operations.append((
                            4,
                            int(group[1:]) if "," in group else int(group)
                        ))
                else:
                    raise Exception(f"Error parsing option '{opt} {optarg}': invalid format")

            elif opt in ["-O", "--set-default-sink"]:
                match = set_default_pattern.match(optarg)
                if match:
                    try:
                        sink = int(match.group(1))
                    except ValueError:
                        sink = match.group(1)
                    config.operations.append((
                        5,
                        sink
                    ))

            elif opt in ["-I", "--set-default-source"]:
                match = set_default_pattern.match(optarg)
                if match:
                    try:
                        source = int(match.group(1))
                    except ValueError:
                        source = match.group(1)
                    config.operations.append((
                        6,
                        source
                    ))

            elif opt in ["-p", "--print"]:
                print_info()
                return 0

            elif opt in ["-P", "--print-select"]:
                match = print_select_pattern.match(optarg)
                if match:
                    print_info(
                        sinks=int(match.group(1)),
                        sources=int(match.group(2)),
                        modules=int(match.group(3))
                    )
                else:
                    raise Exception(f"Error parsing option '{opt} {optarg}': invalid format")
                return 0

            elif opt in ["-h", "--help"]:
                print(helptext)
                return 0

    except getopt.GetoptError as ERR:
        print(helptext)
        raise ERR

    if config.discord:
        # discord info
        discord_out, discord_in = get_discord_io()

        # null sinks
        NS1_module, NS1, NS1_monitor = make_null_sink("NullSink1", printflag=True)
        NS2_module, NS2, NS2_monitor = make_null_sink("NullSink2", printflag=True)

        # loopbacks
        NS1_to_main_out = make_loopback(NS1_monitor, config.main_out, printflag=True)
        NS1_to_NS2 = make_loopback(NS1_monitor, NS2, printflag=True)
        main_in_to_NS2 = make_loopback(config.main_in, NS2, printflag=True)

        # assign discord_* to appropriate sinks/sources
        move_sink_input(discord_out, config.main_out, printflag=True)
        move_source_output(discord_in, NS2_monitor, printflag=True)

        # set defaults so that you won't have to care about when new
        # applications want to use sinks/sources
        set_default_sink(NS1, printflag=True)
        set_default_source(NS2_monitor, printflag=True)

    for operation in config.operations:
        if operation[0] == 0: # make sink
            sink = make_null_sink(operation[1], printflag=True)
            if operation[2]:
                set_default_sink(sink[1].name, printflag=True)
            if operation[3]:
                set_default_source(sink[2].name, printflag=True)

        elif operation[0] == 1: # make loopback
            source = get_source(operation[1])
            sink = get_sink(operation[2])
            make_loopback(source, sink, printflag=True)

        elif operation[0] == 2: # move sink input
            sink_input = get_sink_input(operation[1])
            sink = get_sink(operation[2])
            move_sink_input(sink_input, sink, printflag=True)

        elif operation[0] == 3: # move source output
            source_output = get_source_output(operation[1])
            source = get_source(operation[2])
            move_source_output(source_output, source, printflag=True)

        elif operation[0] == 4: # kill module
            kill_module(operation[1], printflag=True)

        elif operation[0] == 5: # set default sink
            sink = get_sink(operation[1])
            set_default_sink(sink)

        elif operation[0] == 6: # set default source
            source = get_source(operation[1])
            set_default_source(source)

    if config.discord and config.discord_wait:
        print("")
        o = input("Press ENTER to kill the Discord setup... ")
        print("")
        kill_module(
            main_in_to_NS2.index,
            NS1_to_NS2.index,
            NS1_to_main_out.index,
            NS2_module.index,
            NS1_module.index,
            printflag=True
        )
        set_default_sink(config.main_out)
        set_default_source(config.main_in)
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))

