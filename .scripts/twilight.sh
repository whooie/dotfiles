#!/bin/bash

# C5.A4.G4-F4-A4.E4.F4-F4-E4.Db4.D4-E4-F4.D4.F4-F4-G4.A#4.A4-G4-F4-G4
# |-----------------------|----------------|---------------|------------------|
# |                       |                |               |                  |
# |-----------------------|----------------|---------------|------------------|
# |   4   o`              |                |               |                  |
# |---------x---x----_--x-|---x------x---x-|-------x---x---|----x---x---------|
# |   4       o`   _/ o   |                |               |     #o` No       |
# |-G-------------o-------|----------------|-----_---------|-o`---------o---o-|
# |                 o     |     o          |   _/o       o |              o   |
# |-----------------------|-o`----o`-------|--/o-----------|------------------|
#                                     bo`   No       o`                        
#
# o` = eigth note; o = quarter note; x = quarter rest; b = flat; # = sharp; N = natural

C5=523.2511
As4=466.1638
A4=440.0000
G4=391.9954
F4=349.2282
E4=329.6276
D4=293.6648
Db4=277.1826

#C5=1046.5022
#As4=932.3276
#A4=880.0000
#G4=783.9908
#F4=698.4564
#E4=659.2552
#D4=587.3296
#Db4=554.3652

eigth=180
quarter=$[2*eigth]
half=$[4*eigth]

twilight(){
    beep -f ${C5}  -l $eigth \
        -D $eigth
    beep -f ${A4}  -l $eigth \
        -D $eigth
    beep -f ${G4}  -l $eigth
    beep -f ${F4}  -l $eigth
    beep -f ${A4}  -l $eigth \
        -D $eigth

    beep -f ${E4}  -l $eigth \
        -D $eigth
    beep -f ${F4}  -l $quarter
    beep -f ${E4}  -l $eigth \
        -D $eigth
    beep -f ${Db4} -l $eigth \
        -D $eigth

    beep -f ${D4}  -l $eigth
    beep -f ${E4}  -l $eigth
    beep -f ${F4}  -l $eigth \
        -D $eigth
    beep -f ${D4}  -l $eigth \
        -D $eigth
    beep -f ${F4}  -l $quarter

    beep -f ${G4}  -l $eigth \
        -D $eigth
    beep -f ${As4} -l $eigth \
        -D $eigth
    beep -f ${A4}  -l $eigth
    beep -f ${G4}  -l $eigth
    beep -f ${F4}  -l $eigth
    beep -f ${G4}  -l $eigth
}
end(){
    beep -f ${F4} -l $half
}

twilight
twilight
end
