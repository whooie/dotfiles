#!/bin/bash

case $1 in
    ""|-h)
        echo -e "Usage: \e[1mrmDR_Store.sh\e[0m \e[4mstarting location\e[0m"
        echo -e "       \e[1mrmDS_Store.sh\e[0m [ -h ]"
        exit 1
        ;;
    *)
        find "$1" -name ".DS_Store" -print0 | xargs -0 rm -rf
        find "$1" -name "._*" -print0 | xargs -0 rm -rf
        find "$1" -name ".Spotlight*" -print0 | xargs -0 rm -rf
        find "$1" -name ".Trashes" -print0 | xargs -0 rm -rf
        find "$1" -name ".fseventsd" -print0 | xargs -0 rm -rf
        ;;
esac
