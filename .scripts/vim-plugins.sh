#!/bin/bash

INSTALL_DIR="/home/whooie/.vim/pack/whooie/start"
mkdir -p "${INSTALL_DIR}"

install() {
    test -d "${2}" && {
        echo "${2} exists, skipping..."
    } || {
        echo ">> clone ${1} -> ${2}"
        git clone "${1}" "${2}"
        vim -u NONE -c "helptags ${2}/doc" -c q
    }
}

update() {
    olddir="$(pwd)"
    test -d "${1}" && {
        echo ">> updating ${1}"
        cd "${1}"
        git pull
        cd "${olddir}"
    } || {
        echo "error occurred when updating ${1}, skipping..."
    }
}

repos=(
    "git@github:tpope/vim-sleuth.git"
    "git@github:chaoren/vim-wordmotion.git"
    "git@github:tomtom/tcomment_vim.git"
    "git@github:tpope/vim-surround.git"
    "git@github:rust-lang/rust.vim.git"
    "git@github:mg979/vim-visual-multi.git"
    "git@github:preservim/nerdtree.git"
    "git@github:preservim/tagbar.git"
    "git@github:luizribeiro/vim-cooklang.git"
    "git@github:zah/nim.vim.git"
    "git@github:preservim/vim-markdown.git"
    "git@github:voldikss/vim-mma.git"
    "git@github:neovimhaskell/haskell-vim.git"
    "git@github:wlangstroth/vim-racket.git"
    "git@github:bling/vim-bufferline.git"
)

locs=(
    "${INSTALL_DIR}/vim-sleuth"
    "${INSTALL_DIR}/vim-wordmotion"
    "${INSTALL_DIR}/tcomment_vim"
    "${INSTALL_DIR}/vim-surround"
    "${INSTALL_DIR}/rust.vim"
    "${INSTALL_DIR}/vim-visual-multi"
    "${INSTALL_DIR}/nerdtree"
    "${INSTALL_DIR}/tagbar"
    "${INSTALL_DIR}/vim-cooklang"
    "${INSTALL_DIR}/nim.vim"
    "${INSTALL_DIR}/vim-markdown"
    "${INSTALL_DIR}/vim-mma"
    "${INSTALL_DIR}/haskell-vim"
    "${INSTALL_DIR}/vim-racket"
    "${INSTALL_DIR}/vim-bufferline"
)

if [ "${1}" == "update" ]; then
    for k in ${!locs[@]}; do
        update "${locs[$k]}"
    done
else
    for k in ${!repos[@]}; do
        install "${repos[$k]}" "${locs[$k]}"
    done
fi

