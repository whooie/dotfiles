" Vim color file
" Maintainer:	David Schweikert <david@schweikert.ch>
" Last Change:	2014 Mar 19

set background=dark

hi clear

let g:colors_name = "cosmos"

if exists("syntax_on")
  syntax reset
endif

" Normal should come first
hi Normal       guifg=#d8d8d8   guibg=#101012
hi Cursor       guifg=bg        guibg=fg
hi lCursor      guifg=NONE      guibg=NONE

hi CursorLine   cterm=NONE      ctermbg=NONE    guifg=NONE      guibg=NONE
hi clear CursorLineNr
hi CursorLineNr ctermfg=234     ctermbg=253     guifg=#1c1c1c   guibg=#dadada
hi LineNr       ctermfg=245     ctermbg=NONE    guifg=#8a8a8a   guibg=NONE
hi TablineFill  ctermfg=NONE    ctermbg=NONE    term=bold       cterm=bold
hi TabLine      ctermfg=NONE    ctermbg=NONE    term=bold       cterm=bold
hi TabLineSel   ctermfg=White   ctermbg=NONE
hi VertSplit    ctermfg=NONE    ctermbg=NONE    term=bold       cterm=NONE
hi StatusLine   cterm=NONE      ctermfg=NONE    ctermbg=NONE    guibg=NONE

" Note: we never set 'term' because the defaults for B&W terminals are OK
hi DiffAdd      ctermbg=LightBlue       guibg=LightBlue
hi DiffChange   ctermbg=LightMagenta    guibg=LightMagenta
hi DiffDelete   ctermfg=Blue	        ctermbg=LightCyan   gui=bold        guifg=Blue guibg=LightCyan
hi DiffText     ctermbg=Red	            cterm=bold          gui=bold        guibg=Red
hi Directory    ctermfg=LightGray	    guifg=Blue
hi ErrorMsg     ctermfg=White	        ctermbg=DarkRed     guibg=Red	    guifg=White
hi FoldColumn   ctermfg=Black	        ctermbg=Grey        guibg=Grey	    guifg=DarkBlue
hi Folded       ctermbg=Gray	        ctermfg=Black       guibg=LightGrey guifg=DarkBlue
hi IncSearch    cterm=reverse	        gui=reverse
hi ModeMsg      cterm=bold	            gui=bold
hi MoreMsg      ctermfg=DarkGreen       gui=bold            guifg=SeaGreen
hi NonText      ctermfg=DarkGray	    gui=bold            guifg=DarkGray      guibg=#101012
hi Pmenu        guibg=LightBlue
hi PmenuSel     ctermfg=White	        ctermbg=DarkBlue    guifg=White     guibg=DarkBlue
hi Question     ctermfg=DarkGreen       gui=bold            guifg=SeaGreen
if &background == "light"
    hi Search   ctermfg=NONE            ctermbg=Yellow      guibg=Yellow    guifg=NONE
else
    hi Search   ctermfg=Black	        ctermbg=Yellow      guibg=Yellow    guifg=Black
endif
hi SpecialKey   ctermfg=DarkGray        guifg=DarkGray
hi StatusLine   cterm=bold	            ctermbg=blue        ctermfg=yellow  guibg=gold  guifg=blue
hi StatusLineNC	cterm=bold	            ctermbg=blue        ctermfg=black   guibg=gold  guifg=blue
hi Title        ctermfg=DarkMagenta     gui=bold            guifg=Magenta
hi VertSplit    cterm=reverse	        gui=reverse
hi Visual       ctermbg=NONE	        cterm=reverse       gui=reverse     guifg=Grey  guibg=fg
hi VisualNOS    cterm=underline,bold    gui=underline,bold
hi WarningMsg   ctermfg=DarkRed	        guifg=Red
hi WildMenu     ctermfg=Black	        ctermbg=Yellow      guibg=Yellow    guifg=Black
hi ColorColumn  ctermbg=Black

" syntax highlighting
hi Comment      cterm=NONE ctermfg=Gray         gui=NONE    guifg=Gray
hi Constant     cterm=NONE ctermfg=Blue         gui=NONE    guifg=green3
hi Identifier   cterm=NONE ctermfg=DarkCyan     gui=NONE    guifg=cyan4
hi PreProc      cterm=NONE ctermfg=Blue         gui=NONE    guifg=magenta3
hi Special      cterm=NONE ctermfg=Cyan         gui=NONE    guifg=deeppink
hi Statement    cterm=NONE ctermfg=Red	        gui=bold    guifg=blue
hi Type	        cterm=NONE ctermfg=White     gui=none    guifg=blue
hi String       cterm=NONE ctermfg=Yellow
hi Conditional  cterm=NONE ctermfg=Red
hi Builtin      cterm=NONE ctermfg=Cyan
hi Structure    cterm=NONE ctermfg=Gray
hi Operator     cterm=None ctermfg=Red

" markdown
hi mkdCode          cterm=None ctermfg=White    gui=None guifg=White
hi mkdCodeDelimiter cterm=None ctermfg=DarkCyan gui=None guifg=DarkCyan
hi mkdCodeStart     cterm=None ctermfg=Cyan     gui=None guifg=Cyan
hi mkdCodeEnd       cterm=None ctermfg=Cyan     gui=None guifg=Cyan
hi mkdBlockquote    cterm=None ctermfg=DarkYellow gui=None guifg=DarkYellow
hi mkdListItem      cterm=None ctermfg=Red      gui=None guifg=Red

" Java
hi javaExternal     cterm=NONE  ctermfg=Red
hi javaError        cterm=bold  ctermfg=DarkYellow
hi javaConditional  cterm=NONE  ctermfg=Red
hi javaRepeat       cterm=NONE  ctermfg=Red
hi javaBoolean      cterm=NONE  ctermfg=Magenta
hi javaConstant     cterm=NONE  ctermfg=Magenta
hi javaTypedef      cterm=none  ctermfg=DarkBlue
hi javaOperator     cterm=NONE  ctermfg=Red
hi javaType         cterm=none  ctermfg=Cyan
hi javaStatement    cterm=NONE  ctermfg=Red
hi javaStorageClass cterm=NONE  ctermfg=Red
hi javaExceptions   cterm=NONE  ctermfg=Red
hi javaAssert       cterm=NONE  ctermfg=Red
hi javaMethodDecl   cterm=NONE  ctermfg=Red
hi javaClassDecl    cterm=NONE  ctermfg=White
hi javaBranch       cterm=NONE  ctermfg=Red
hi javaScopeDecl    cterm=NONE  ctermfg=Red
hi javaFuncDef      cterm=NONE  ctermfg=Blue
hi javaVarArg       cterm=NONE  ctermfg=Cyan

" matlab
hi matlabFunction   cterm=NONE  ctermfg=Cyan
hi matlabImaginary  cterm=NONE  ctermfg=Magenta

" changelog
hi  changelogText   ctermfg=White

" NERDTree
hi NERDTreeDir      ctermfg=Gray        ctermbg=NONE    cterm=Bold
hi NERDTreeUp       ctermfg=DarkGray    ctermbg=NONE    cterm=Bold
hi NERDTreeOpenable ctermfg=Yellow      ctermbg=NONE    cterm=Bold
hi NERDTreeClosable ctermfg=DarkYellow  ctermbg=NONE    cterm=Bold
hi NERDTreeFile     ctermfg=Magenta     ctermbg=NONE    cterm=Bold

" leap.nvim
hi LeapMatch ctermbg=White ctermfg=Black cterm=Bold
hi LeapLabelPrimary ctermbg=White ctermfg=Black cterm=Bold
hi LeapLabelSecondary ctermbg=Cyan ctermfg=Black cterm=Bold


" vim: sw=2
