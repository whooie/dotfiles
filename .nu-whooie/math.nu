# Create a complex number represented as a two-item record
def complex [
    re: number # Real part
    im: number # Imaginary part
] {
    { re: $re, im: $im }
}

# Super-command and constructor for complex numbers and related operations
def c [
    re: number # Real part
    im: number # Imaginary part
] {
    { re: $re, im: $im }
}

# Performs addition for a complex-valued input
def "c +" [ z: record ] {
    let w = $in
    if ($z | describe) in ["int", "float"] {
        { re: ($w.re + $z), im: ($w.im) }
    } else {
        { re: ($w.re + $z.re), im: ($w.im + $z.im) }
    }
}

# Performs subtraction for a complex-valued input
def "c -" [ z: record ] {
    let w = $in
    if ($z | describe) in ["int", "float"] {
        { re: ($w.re - $z), im: ($w.im) }
    } else {
        { re: ($w.re - $z.re), im: ($w.im - $z.im) }
    }
}

# Performs multiplication for a complex-valued input
def "c *" [ z: record ] {
    let w = $in
    if ($z | describe) in ["int", "float"] {
        { re: ($z * $w.re), im: ($z * $w.im) }
    } else {
        {
            re: ($w.re * $z.re - $w.im * $z.im),
            im: ($w.re * $z.im + $w.im * $z.re)
        }
    }
}

# Performs division for a complex-valued input
def "c /" [ z: record ] {
    let w = $in
    if ($z | describe) in ["int", "float"] {
        { re: ($w.re / $z), im: ($w.im / $z) }
    } else {
        let N = (($z | c norm) ** 2)
        {
            re: ((($w.re * $z.re) + ($w.im * $z.im)) / $N),
            im: ((($w.im * $z.re) - ($w.re * $z.im)) / $N)
        }
    }
}

# Computes the norm of a complex-valued input
def "c norm" [] {
    let w = $in
    (($w.re ** 2) + ($w.im ** 2)) | math sqrt
}

# Alias for `c norm`
def "c abs" [] { c norm }

# Computes the conjugate value of a complex-valued input
def "c conj" [] {
    let w = $in
    { re: ($w.re), im: (-1 * $w.im) }
}

# Alias for `math avg`
def "math mean" [] { $in | math avg }

# Computes the covariance matrix for a series of N-dimensional data points
def "math covariance" [
    --reduce-dimension (-r) # For D=1 and D=2-dimensional data, return the result as a scalar
] {
    let data = $in
    if ($data | length) == 0 {
        error make { msg: "encountered empty dataset" }
    }
    if not ($data | all { |x| $x | describe | str starts-with "list" }) {
        error make { msg: "input stream type must be list" }
    }
    let Nvars = ($data.0 | length)
    if not ($data | all { |x| ($x | length) == $Nvars }) {
        error make { msg: "all data points must be of equal dimension" }
    }
    if not (
        $data
        | all { |x|
            $x | all { |xk| ($xk | describe) in ["int", "float"] }
        }
    ) {
        error make { msg: "encountered non-numerical value" }
    }

    let covar = (
        0..($Nvars - 1)
        | each { |i|
            0..($Nvars - 1)
            | each { |j|
                let X = ($data | each { |V| $V | get $i })
                let Y = ($data | each { |V| $V | get $j })
                let XY = ($X | zip $Y | each { |xy| $xy.0 * $xy.1 })
                let E_XY = ($XY | math avg)
                let E_X = ($X | math avg)
                let E_Y = ($Y | math avg)
                $E_XY - ($E_X * $E_Y)
            }
        }
    )
    if $reduce_dimension and $Nvars == 1 {
        $covar | get 0 | get 0
    } else if $reduce_dimension and $Nvars == 2 {
        $covar | get 0 | get 1
    } else {
        $covar
    }
}

# Computes the Pearson correlation matrix for a series of N-dimensional data points
def "math correlation" [
    --reduce-dimension (-r) # For D=1 and D=2-dimensional data, return the result as a scalar
] {
    let data = $in
    if ($data | length) == 0 {
        error make { msg: "encountered empty dataset" }
    }
    if not ($data | all { |x| $x | describe | str starts-with "list" }) {
        error make { msg: "input stream type must be list" }
    }
    let Nvars = ($data.0 | length)
    if not ($data | all { |x| ($x | length) == $Nvars }) {
        error make { msg: "all data points must be of equal dimension" }
    }
    if not (
        $data
        | all { |x|
            $x | all { |xk| ($xk | describe) in ["int", "float"] }
        }
    ) {
        error make { msg: "encountered non-numerical value" }
    }

    let corr = (
        0..($Nvars - 1)
        | each { |i|
            0..($Nvars - 1)
            | each { |j|
                let X = ($data | each { |V| $V | get $i })
                let Y = ($data | each { |V| $V | get $j })
                let covar = ($X | zip $Y | math covariance -r)
                let sigma_X = ($X | math stddev)
                let sigma_Y = ($Y | math stddev)
                $covar / ($sigma_X * $sigma_Y)
            }
        }
    )
    if $reduce_dimension and $Nvars == 1 {
        $corr | get 0 | get 0
    } else if $reduce_dimension and $Nvars == 2 {
        $corr | get 0 | get 1
    } else {
        $corr
    }
}

