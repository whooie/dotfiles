#!/bin/bash

print-help(){
    echo -e "\e[1mUsage:\e[0m \e[1mblurbg\e[0m \e[4mstart\e[0m | \e[4mstop\e[0m | \e[4mon\e[0m | \e[4moff\e[0m"
    echo -e "       \e[1mblurbg\e[0m [ -i | --info ]"
    echo -e "       \e[1mblurbg\e[0m [ -h ]"
}

case $1 in
    "")
        print-help
        exit 1
        ;;
    -h|--help)
        print-help
        ;;
    -i|--info)
	pid=$(cat /home/whooie/.control/blurbg/pid | tr -d "\n")
	if [[ $(ps -p $pid | tail -n 1 | awk '{print $1}') != "PID" ]]; then
            if [[ $(cat /home/whooie/.control/blurbg/blur_control) == "1" ]]; then
                echo "running:on"
                exit 0
            else
                echo "running:off"
                exit 0
            fi
        else
            echo "not running"
            exit 0
        fi
        ;;
    start)
	echo 1 > /home/whooie/.control/blurbg/blur_control
        bash /home/whooie/.scripts/blur_background.sh &
	echo $! > /home/whooie/.control/blurbg/pid
        ;;
    on)
        echo 1 > /home/whooie/.control/blurbg/blur_control
        ;;
    off)
        echo 0 > /home/whooie/.control/blurbg/blur_control
        ;;
    stop)
        echo kill > /home/whooie/.control/blurbg/blur_control
        ;;
esac
