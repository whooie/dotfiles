#!/bin/bash

print-help(){
    n="\e[0m"
    b="\e[1m"
    u="\e[4m"

    echo -e "Usage: timer [ -q label ] timespec"
    echo -e "       timer [ -h ]"
}

timespec=""

while [[ ${#@} -gt 0 ]]; do
    if [[ "${1:0:1}" == "-" ]]; then
        opt="${1:1}"
        for ((i = 0; i < ${#opt}; i++)); do
            case ${opt:$i:1} in
                h)
                    print-help
                    exit 0
                    ;;
                l)
                    shift 1
                    label="$1"
                    ;;
                *)
                    echo "Invalid option ${opt:$i:1}."
                    print-help
                    exit 1
                    ;;
            esac
        done
        shift 1
    else
        timespec+="$1"
        shift 1
    fi
done

if [[ $(amixer -c 0 sget Beep | tail -n 1 | awk '{print $6}') == "[off]" ]]; then
	beep="amixer -c 0 -q sset Beep mute"
else
	beep=""
fi

if [[ $(amixer -c 0 sget Master | tail -n 1 | awk '{print $6}') == "[off]" ]]; then
	master="amixer -c 0 -q sset Master mute"
else
	master=""
fi

if [[ $(amixer -c 0 sget Speaker | tail -n 1 | awk '{print $7}') == "[off]" ]]; then
	speaker="amixer -c 0 -q sset Speaker mute"
else
	speaker=""
fi

amixer -c 0 -q sset Master unmute
amixer -c 0 -q sset Speaker unmute
amixer -c 0 -q sset Beep unmute
beep -f 350 -l 100
$beep
$speaker
$master
sleep "$timespec"
amixer -c 0 -q sset Master unmute
amixer -c 0 -q sset Speaker unmute
amixer -c 0 -q sset Beep unmute
notify-send -t 2000 "timer" "$label"
beep -f 400 -l 100 -d 50 -r 2
$beep
$speaker
$master
