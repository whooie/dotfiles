source "/home/whooie/.config/nushell/env.nu"

# def create_starship_prompt [] {
#     starship prompt --cmd-duration $env.CMD_DURATION_MS $'--status=($env.LAST_EXIT_CODE)'
# }

$env.PROMPT_INDICATOR = { || $"(ansi reset)" }
$env.PROMPT_INDICATOR_VI_INSERT = { || $"(ansi reset)\r(ansi white_bold)>(ansi reset) " }
$env.PROMPT_INDICATOR_VI_NORMAL = { || $"(ansi reset)\r(ansi white_bold)|(ansi reset) " }
$env.PROMPT_MULTILINE_INDICATOR = { || $"(ansi reset)(ansi white_dimmed):::(ansi reset) " }

$env.PROMPT_COMMAND = { || create_starship_prompt }

