set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source /home/whooie/.vimrc

" let g:indent_blankline_char_blankline='┊'

set cursorline

set guicursor=n-v-c-i:block

set foldexpr=nvim_treesitter#foldexpr()

set title

let g:lisp_rainbow = 1

"snippets
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetDirectories=[$HOME."/.config/nvim/UltiSnips", "UltiSnips"]
" let g:UltiSnipsSnippetDirectories=["UltiSnips", $HOME.'/.vim/UltiSnips']
" let g:UltiSnipsSnippetsDir="/home/whooie/.vim/UltiSnips"
let g:tex_flavor="latex"

" set gdefault

" let g:indent_blankline_char_list = ['|', ':']
" let g:indent_blankline_char_list_blankline = ['|', ':']

" let g:pyindent_disable_parentheses_indenting=v:false
let g:pyindent_closed_paren_align_last_line=v:false
" let g:pyindent_searchpair_timeout=150
let g:pyindent_continue=shiftwidth()
" let g:pyindent_open_paren=shiftwidth()
" let g:pyindent_nested_paren=shiftwidth()

lua <<EOF

require("nvim-treesitter.configs").setup({
    highlight = { enable = true },
    additional_vim_regex_highlighting = true,
    indent = { enable = true }
})

require("indent_blankline").setup({
    show_current_context = true,
    show_current_context_start = true,
})

require("leap").set_default_keymaps()
EOF

