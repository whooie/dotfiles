#!/bin/bash

input="$@"
answer="$(python -c "import sys; sys.dont_write_bytecode = True; import numpy as np; import math as m; import random as r; import whooie.phys as phys; import whooie.foobar as foobar; print(f'{$input}')")"
action=$(echo -e "Clear\nCopy\nClose" | dmenu -fn "Misc Tamsyn" -nb "#1a1a1c" -nf "#a0a0a0" -sb "#32394e" -sf "#ffffff" -x 8 -y 8 -w 1904 -h 24 -p "= $answer")
case $action in
    "Clear")
        $0
        ;;
    "Copy")
        echo $answer | tr -d '\n' | xclip
        ;;
    "Close")
        ;;
    *)
        $0 "$answer $action"
        ;;
esac
