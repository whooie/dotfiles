#!/bin/bash

if [[ $1 == "" ]] || [[ $1 == "-h" ]]; then
    echo -e "Usage: \e[1mbackup.sh\e[0m \e[4mmountpoint\e[0m"
    echo -e "       \e[1mbackup.sh\e[0m [ -h ]"
    exit 1
fi

sudo rsync -aAXvP --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} / $1
