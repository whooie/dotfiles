import sys
import getopt
import subprocess as sp
import typing as t
import re
from math import prod

def run(command: t.List[str], **kwargs) -> sp.CompletedProcess:
    X = sp.run(command, **kwargs)
    if X.returncode > 0:
        raise Exception(f"Subprocess exited with error code > 0:\n  command: {command}")
    return X

def get_outputs_info() -> t.Dict[str, t.Dict[str, t.Union[bool, t.Dict[str, t.Dict[str, bool]]]]]:
    output_pat = re.compile(r"([a-zA-Z0-9\-]+) (connected|disconnected) (.*)")
    output_mode_pat = re.compile(r"^   ([0-9]+x[0-9]+)[ ]+([0-9.]+)([*]*)([+]*)(.*)")
    X = run(["xrandr", "--current"], stdout=sp.PIPE).stdout.decode("utf-8").split("\n")

    outputs = dict()
    output_name = None
    for x in X:
        if match := output_pat.match(x):
            output_name = match.group(1)
            outputs[output_name] = dict()
            outputs[output_name]["connected"] = (match.group(2) == "connected")
            outputs[output_name]["primary"] = (match.group(3)[:7] == "primary")
            outputs[output_name]["modes"] = dict()
        if (match := output_mode_pat.match(x)) and (output_name is not None):
            outputs[output_name]["modes"][match.group(1)] = {
                "active": match.group(3) == "*",
                "preferred": match.group(4) == "+"
            }
    return outputs

def output_is_connected(X: dict, output: str) -> bool:
    return X[output]["connected"]

def output_is_primary(X: dict, output: str) -> bool:
    return X[output]["primary"]

def output_has_mode(X: dict, output: str, mode: str) -> bool:
    return mode in X[output]["modes"].keys()

def output_mode_is_active(X: dict, output: str, mode: str) -> bool:
    return X[output]["modes"][mode]["active"]

def output_mode_is_preferred(X: dict, output: str, mode: str) -> bool:
    return X[output]["modes"][mode]["preferred"]

def get_active_modes(X: dict) -> t.Dict[str, str]:
    res = dict()
    for output in X.keys():
        for mode in X[output]["modes"].keys():
            if X[output]["modes"][mode]["active"]:
                res[output] = mode
    return res

def get_preferred_modes(X: dict) -> t.Dict[str, str]:
    res = dict()
    for output in X.keys():
        for mode in X[output]["modes"].keys():
            if X[output]["modes"][mode]["preferred"]:
                res[output] = mode
    return res

def main(argv) -> None:
    shortopts = ""
    longopts = [
    ]
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
    except getopt.GetoptError as ERR:
        raise ERR
    if len(args) < 2:
        raise Exception("requires 2 args: <output> <mode>")
    for opt, optarg in opts:
        pass
    X = get_outputs_info()
    if output_has_mode(X, args[0], args[1]):
        print(args[1])
        return 0
    else:
        max_res = max(
            X[args[0]]["modes"].keys(),
            key=lambda x: prod([int(d) for d in x.split("x")])
        )
        print(max_res)
        return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))

