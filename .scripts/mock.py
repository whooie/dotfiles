#!/usr/bin/python

import getopt
import sys
import random

opts, args = getopt.gnu_getopt(sys.argv[1:], '')

if len(args) == 0:
    sys.exit(1)
else:
    convert = {
        str.upper: [(str.upper, 0.2), (str.lower, 0.8)],
        str.lower: [(str.upper, 0.8), (str.upper, 0.2)]
    }
    last = str.upper
    res = ""
    for arg in args:
        for char in arg:
            if char == ' ':
                res += ' '
                continue
            r0 = random.random()
            r = 0
            for opt in convert[last]:
                if r0 >= r and r0 < r+opt[1]:
                    conversion = opt[0]
                    break
                else:
                    r += opt[1]
            res += conversion(char)
            last = conversion
        res += ' '
    print(res[:-1])
    sys.exit(0)
