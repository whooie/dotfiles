#!/bin/bash

path="/etc/openvpn/mullvad"

print-help(){
    echo -e "Frontend for openvpn."
    echo -e "Usage: vpn <config_name>"
    echo -e "       vpn -l"
    echo -e "       vpn [ -h ]"
}

case $1 in
	-h)
        print-help
        exit 0
	    ;;
    -l)
        for file in $path/mullvad_*.conf; do
            basename "$file" | sed -r 's/mullvad_([a-zA-Z0-9_\-]*)\.conf/\1/'
        done
        exit 0
        ;;
    "")
        print-help
        exit 1
        ;;
    *)
        filename="$1"
        if [[ ! -e "$path/mullvad_$filename.conf" ]]; then
            echo "Invalid config 'mullvad_$filename.conf'. See -l for more info."
            exit 1
        else
            cd $path
            sudo openvpn --config "mullvad_$filename.conf"
            exit 0
        fi
        ;;
esac
