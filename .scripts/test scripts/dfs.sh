#!/bin/bash

dfs(){
    echo "${2:-}${1##*/}"
    for item in "$1"/*; do
        if [[ -d "$item" ]] && [[ ! -z $(ls "$item") ]]; then
            dfs "$item" "${2:-}  "
        else
            echo "${2:-}  ${item##*/}"
        fi
    done
}

dfs $1 ""
