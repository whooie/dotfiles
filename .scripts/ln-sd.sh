#!/bin/bash

if [[ $1 == "" ]] || [[ $2 == "" ]]; then
    echo "Input error"
    exit 1
fi

if [[ ${1:0:1} == "/" ]] || [[ ${1:0:1} == '~' ]]; then
    source_file="$1"
else
    source_file="$(pwd)/$1"
fi
if [[ ${2:0:1} == "/" ]] || [[ ${1:0:1} == '~' ]]; then
    target_file="${2%/}/${1##*/}"
else
    target_file="$(pwd)/${2%/}/${1##*/}"
fi

ln -s "$source_file" "$target_file"
