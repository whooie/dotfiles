#!/bin/bash

#search_workspace_tree(){
#    {
#        echo "$1" | $JQ '.focused' | grep --quiet "true"
#    } && {
#        echo $(echo "$1" \
#            | $JQ '{f: .focused, n: .name, r:.rect}
#            | select(.f == true)
#            | .r.x, .r.y, .r.width, .r.height'
#        )
#    } || {
#        nodes=$(echo "$1" | $JQ '.nodes[]')
#        echo $(search_workspace_tree "$nodes")
#    }
#}

I3MSG=$(command -v i3-msg) || exit 1
JQ=$(command -v jq -M) || exit 2
BC=$(command -v bc) || exit 3

gap=14

output=$($I3MSG -t get_workspaces \
    | $JQ '.[]
    | {f: .focused, out: .output}
    | select(.f == true)
    | .out'
)

readarray workspace <<<"$($I3MSG -t get_workspaces \
    | $JQ '.[]
    | {f: .focused, n: .name, r: .rect}
    | select(.f == true)
    | .n, .r.x, .r.y, .r.width, .r.height'
)"
workspace_x=$[${workspace[1]} + 0]
workspace_y=$[${workspace[2]} + 0]
workspace_w=$[${workspace[3]} + 0]
workspace_h=$[${workspace[4]} + 0]

#workspace_tree=$($I3MSG -t get_tree \
#    | $JQ '.nodes[] 
#    | select(.name == '"$output"').nodes[]
#    | select(.type == "con").nodes[]
#    | select(.name == '"${workspace[0]}"').floating_nodes[]'
#)
#
#readarray container <<<"$(search_workspace_tree "$workspace_tree" | tr ' ' '\n')"
readarray container <<<"$($I3MSG -t get_tree \
    | $JQ '.nodes[]
    | select(.name == '"$output"').nodes[]
    | select(.type == "con").nodes[]
    | select(.name == '"${workspace[0]}"').floating_nodes[].nodes[]
    | {f: .focused, r: .rect}
    | select(.f == true)
    | .r.x, .r.y, .r.width, .r.height'
)"
container_x=$[${container[0]} + 0]
container_y=$[${container[1]} + 0]
container_w=$[${container[2]} + 0]
container_h=$[${container[3]} + 0]

bar_h=$($I3MSG -t get_tree \
    | $JQ '.nodes[]
    | select(.name == '"$output"').nodes[]
    | select(.name == "bottomdock")
    | .rect.height'
)

if [[ "$1" == "left" ]]; then
    #new_x=$($BC <<<"$workspace_x + $gap")
    new_x=$[$workspace_x + $gap]
elif [[ "$1" == "center" ]]; then
    #new_x=$($BC <<<"$workspace_x + ($workspace_w - $container_w)/2")
    new_x=$[$workspace_x + ($workspace_w - $container_w)/2]
elif [[ "$1" == "right" ]]; then
    #new_x=$($BC <<<"$workspace_x + ($workspace_w - $container_w - $gap)")
    new_x=$[$workspace_x + ($workspace_w - $container_w - $gap)]
else
    new_x=$($BC <<<"scale=0; ($workspace_x + (${1:-0} * $workspace_w))/1")
fi

if [[ "$2" == "top" ]]; then
    #new_y=$($BC <<<"$workspace_y + $gap")
    new_y=$[$workspace_y + $gap]
elif [[ "$2" == "center" ]]; then
    #new_y=$($BC <<<"$workspace_y + ($workspace_h - $container_h)/2")
    new_y=$[$workspace_y + ($workspace_h - $container_h)/2]
elif [[ "$2" == "bottom" ]]; then
    #new_y=$($BC <<<"$workspace_y + ($workspace_h - $container_h - $gap)")
    new_y=$[$workspace_y + ($workspace_h - $container_h - $gap)]
else
    new_y=$($BC <<<"scale=0; ($workspace_y + (${2:-0} * ($workspace_h + $bar_h)))/1")
fi

$I3MSG -t command move position $new_x $new_y
