"""
Handy miscellaneous functions.
"""

import re
import itertools
import math
import numpy as np

def timestr_to_seconds(timestr):
    multipliers = [1, 60, 3600, 86400]

    elements = [float(e) for e in timestr.split(":")]
    elements.reverse()
    elements = elements[:4]
    t = 0
    for i in range(len(elements)):
        t = t + elements[i]*multipliers[i]
    return t

def time_diff_s(timestr1, timestr2):
    return timestr_to_seconds(timestr2) - timestr_to_seconds(timestr1)

def pad_str(s, l, c=' ', j='l'):
    length = len(s) if l == None else l
    if length < len(str(s)):
        if j in ['r', 'right']:
            return str(s)[len(str(s))-length:]
        else:
            return str(s)[:l]
    elif j in ['c', 'center']:
        return ((length-len(str(s)))//2)*c + str(s) + int((length-len(str(s)))/2+0.5)*c
    elif j in ['r', 'right']:
        return (length-len(str(s)))*c + str(s)
    else:
        return str(s) + (length-len(str(s)))*c

def time_diff(timestr1, timestr2):
    multipliers = [86400, 3600, 60, 1]
    lengths = [None, 2, 2, 6]
    
    diff = time_diff_s(timestr1, timestr2)
    s = ""
    for i in range(4):
        if i != 3:
            e = diff//multipliers[i]
            diff = diff - e*multipliers[i]
            s = s + pad_str(f"{e:.0f}", lengths[i], '0', 'r') + ":"
        else:
            e = diff
            s = s + pad_str(f"{e:.3f}", lengths[i], '0', 'r')
    return s

def piano_keynum(key):
    if type(key).__name__ == "int" or type(key).__name__ == "float":
        return key
    elif type(key).__name__ == "str":
        pattern = re.compile("([A-G])([#bn]?)([\-]?)([0-9]+)")
        match = pattern.match(key)
        octave_key_nums = {
                "C": 1,
                "D": 3,
                "E": 5,
                "F": 6,
                "G": 8,
                "A": 10,
                "B": 12
            }
        sharp_flat = {
                "#": 1,
                "b": -1,
                "n": 0,
                "": 0
            }
        if match:
            return 12*int(match.group(3)+match.group(4)) \
                    + octave_key_nums[match.group(1)] \
                    + sharp_flat[match.group(2)] \
                    - 9
        else:
            return None
    else:
        return None

def piano_keyname(key, accidental="#"):
    if type(key).__name__ == "str":
        pattern = re.compile("([A-G])([#bn]?)([\-]?)([0-9]+)")
        match = pattern.match(key)
        return key if match else None
    elif type(key).__name__ == "int" or type(key).__name__ == "float":
        octave_key_names = {
                1: "C",
                3: "D",
                5: "E",
                6: "F",
                8: "G",
                10: "A",
                12: "B"
            }
        if accidental == "#":
            acc = 1
        elif accidental == "b":
            acc = -1
        else:
            return None
        octave_num = (key-1 + 9)//12
        octave_key_num = (key + 9)%12 if (key + 9)%12 != 0 else 12
        for k in octave_key_names.keys():
            if octave_key_num - k == 0:
                return octave_key_names[octave_key_num] + str(octave_num)
            elif octave_key_num - k == acc:
                return octave_key_names[octave_key_num-acc] + accidental + str(octave_num)
        return None
    else:
        return None

def piano_freq(key, a4=440):
    keynum = piano_keynum(key)
    return a4*2**((keynum-49)/12)

def concat_multiline_str(str1, str2):
    lines1 = str1.split('\n')
    lines2 = str2.split('\n')
    outstr = ""
    while len(lines1) > 0 or len(lines2) > 0:
        try:
            line1 = lines1.pop(0)
        except:
            line1 = ""
        try:
            line2 = lines2.pop(0)
        except:
            line2 = ""
        outstr = outstr + line1 + line2 + '\n'
    return outstr

def color_hex(message, hex_color):
    r, g, b = [int(hex_color[i:i+2], 16) for i in range(1, len(hex_color), 2)]
    return f"\x1b[38;2;{r};{g};{b}m{message}\x1b[0m"

def print_hex(message, hex_color):
    print(color_hex(message, hex_color))

def binomial(n, k):
    if k < 0 or k > n:
        return 0
    if k == 0 or k == n:
        return 1
    k = min(k, n - k)
    c = 1
    for i in range(k):
        c = c * (n - i) / (i + 1)
    return c

def gcd(a, b, p=False):
    q = b // a
    r = b % a
    if p:
        print(f"{b} = {q} * {a} + {r}")
    if r == 0:
        return a
    else:
        return gcd(r, a, p)

def ordermod(a, n):
    if gcd(a, n) != 1:
        return None
    k = 1
    while True:
        if pow(a, k, mod=n) == 1:
            return k
        else:
            k += 1

def totient(n):
    phi = 0
    if n == 0:
        return 0
    for i in range(1, abs(n)):
        if gcd(i, n) == 1:
            phi += 1
    return phi

def is_prime(n):
    for d in range(2, n):
        if n % d == 0:
            return (False, d)
    return (True, 1)

def listmod(z, m):
    return list(map(lambda x: x % m, z))

def cont_frac(alpha, maxiter=20):
    an = [int(alpha)]
    alpha_last = alpha
    for k in range(maxiter - 1):
        try:
            alpha_last = 1 / (alpha_last - int(alpha_last))
            an.append(int(alpha_last))
        except OverflowError:
            break
        except ZeroDivisionError:
            break
    return an

def Cn(an, n=None):
    __p, _p, p = 1, an[0], an[0]
    __q, _q, q = 0, 1, 1
    for ai in an[1:len(an) if n == None else n+1]:
        p = ai*_p + __p
        q = ai*_q + __q
        __p, _p = _p, p
        __q, _q = _q, q
    return p, q

def Cn_(an):
    if len(an) == 1:
        return an[0]
    else:
        return an[0] + 1/Cn_(an[1:])

def prime_factors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors

def order_prime_factors(n):
    factors = prime_factors(n)
    orders = dict()
    for factor in factors:
        if factor in orders:
            orders[factor] += 1
        else:
            orders[factor] = 1
    return list(zip(orders.keys(), orders.values()))

def dirichlet_prod(n, f, g):
    prime_orders = order_prime_factors(n)
    primes, orders = list(zip(*prime_orders))
    combos = list(itertools.product(*[tuple(range(n+1)) for n in orders]))
    res = 0
    for combo in combos:
        d = math.prod([p**e for p, e in prime_orders])
        res += f(d)*g(n//d)
    return res

def num_divisors(n):
    if abs(n) <= 1:
        return 1
    primes, orders = list(zip(*order_prime_factors(n)))
    combos = list(itertools.product(*[tuple(range(n+1)) for n in orders]))
    return len(combos)

def sieve(N):
    nums = list(range(2, N + 1))
    primes = list()
    while len(nums) > 0:
        n = nums.pop(0)
        primes.append(n)
        for k in nums:
            if k % n == 0:
                nums.remove(k)
    return primes

def xor_swap(a, b):
    a = a|b
    b = a|b
    a = a|b

def fibonacci_binet(n):
    phi = (1 + math.sqrt(5))/2
    return ((phi + 0j)**n - (-1 / phi + 0j)**n) / math.sqrt(5)

def chebyshevspace(xmin, xmax, N):
    k = np.linspace(1, N, N)
    return (
        (xmax + xmin) / 2
        + (xmax - xmin) * np.cos((2 * k - 1) / (2 * N) * np.pi) / 2
    )

