#!/bin/bash

name="$1"
if [[ $name == "sudo" ]]; then
    name="$2"
fi
test "${name:-0}" == "0" || echo -e "$(date)\n"
test "${name:-0}" == "0" || time "$@"
test "${name:-0}" == "0" || echo -e "\n$(date)"
notify-send -t 2000 "${name:-ndone}" "process has ended"
if [[ $(amixer -qc 0 sget Beep | tail -n 1 | awk '{print $6}') == "[on]" ]]; then
    beep_finish="amixer -c 0 sset Beep unmute"
fi
amixer -qc 0 sset Beep mute
echo -ne "\a"
$beep_finish
