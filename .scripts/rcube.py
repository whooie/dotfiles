#!/usr/bin/python

import whooie.rubix
import getopt
import sys
import getpass
from timeit import default_timer as timer

config = type("Config", (), {
    "interactive": False,
    "preview": True
})()

helptext = """
Simple script to generate Rubik's cube scrambles and time solves.

Usage: rcube [ -i ] [ -h ]
Options:
    -i, --interactive
        Run in interactive mode. Accessible commands are:
            <empty> : Generate another scramble.
            t       : Start a timer (press ENTER to stop).
            q       : Quit.
            ?       : List commands.
    -P, --no-preview
        Don't print a preview of the scrambled cube in interactive mode.
    -h, --help
        Print this text and exit.
"""

shortopts = "iPh"
longopts = [
    "interactive",
    "no-preview",
    "help"
]
try:
    opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
except getopt.GetoptError as ERR:
    raise ERR
for opt, optarg in opts:
    if opt in ["-i", "--interactive"]:
        config.interactive = True
    if opt in ["-P", "--no-preview"]:
        config.preview = False
    elif opt in ["-h", "--help"]:
        print(helptext)
        sys.exit(0)

if config.interactive:
    print("")
    while True:
        cube = whooie.rubix.Cube(printmoves=config.preview, truecolor=True)
        cube.randomize(32)
        o = input(">> ")
        if o in ["t", "timer", "time"]:
            t0 = timer()
            o = input("\033[1A\rPress ENTER to stop timer: ")
            print(f"\033[1A\rSolved in {timer() - t0:.3f}s          ")
            o = input("Press ENTER to continue: ")
        elif o in ["?", "h", "help"]:
            print("""
COMMANDS
<empty>         : Generate another scramble
t, timer, time  : Start a timer (press ENTER to stop)
q, quit, exit   : Quit
?, h, help      : List commands


















            """[1:-1])
        elif o in ["q", "quit", "exit"]:
            sys.exit(0)
else:
    cube = whooie.rubix.Cube(printmoves=True, truecolor=True)
    cube.randomize(35)
