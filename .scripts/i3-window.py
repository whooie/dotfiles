import getopt
import sys
import json
import subprocess
import re
#import i3ipc
#i3 = i3ipc.Connection()
#i3.get_tree().find_focused().workspace()

def get_json_output(command):
    return json.loads(subprocess.getoutput(command))

def json_select(json_list, prop_val_test):
    return list(filter(prop_val_test, json_list))

def main(argv):
    no_border = False
    execute_command = False
    shortopts = "be"
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts)
    except getopt.GetoptError as e:
        print(f"{e}")
        return 1
    for opt, optarg in opts:
        if opt in ["-b"]:
            no_border = True
        elif opt in ["-e"]:
            execute_command = True

    if len(args) < 3:
        print("not enough arguments")
        return 1

    gap_size = 14

    i3_workspaces = get_json_output('i3-msg -t get_workspaces')
    i3_tree = get_json_output('i3-msg -t get_tree')

    workspace = json_select(i3_workspaces, lambda x: x["focused"] == True)
    ws_name = workspace[0]["name"]
    ws_output = workspace[0]["output"]
    ws_rect = type('', (), workspace[0]["rect"])()

    output_tree = json_select(i3_tree["nodes"], lambda x: x["name"] == ws_output)[0]
    workspace_tree = json_select(output_tree["nodes"], lambda x: x["type"] == "con")[0]
    floating_trees = json_select(workspace_tree["nodes"], lambda x: x["name"] == ws_name)[0]["floating_nodes"]
    floating_containers = json_select([single_parent["nodes"][0] for single_parent in floating_trees],
            lambda x: x["focused"] == True)
    if len(floating_containers) == 0:
        print("no focused floating containers")
        return 1
    con_rect = type('', (), floating_containers[0]["rect"])()

    bar_height = json_select(output_tree["nodes"], lambda x: x["name"] == "bottomdock")[0]["rect"]["height"]

    border_size = floating_containers[0]["current_border_width"] if no_border else 0
    
    if args[0] == "position":
        if args[1] == "left":
            new_x = round(ws_rect.x + gap_size - border_size)
        elif args[1] == "center":
            new_x = round(ws_rect.x + (ws_rect.width - con_rect.width)/2)
        elif args[1] == "right":
            new_x = round(ws_rect.x + (ws_rect.width - con_rect.width - gap_size) + border_size)
        else:
            try:
                args[1] = float(args[1])
                if args[1] < 0:
                    raise Exception
            except:
                print("invalid x argument")
                return 1

            if args[1] <= 1:
                new_x = round(ws_rect.x + args[1]*ws_rect.width - border_size)
            else:
                new_x = round(ws_rect.x + args[1] - border_size)

        if args[2] == "top":
            new_y = round(ws_rect.y + gap_size - border_size)
        elif args[2] == "center":
            new_y = round(ws_rect.y + (ws_rect.height - con_rect.height)/2)
        elif args[2] == "bottom":
            new_y = round(ws_rect.y + (ws_rect.height - con_rect.height - gap_size) + border_size)
        else:
            try:
                args[2] = float(args[2])
                if args[2] < 0:
                    raise Exception
            except:
                print("invalid y argument")
                return 1

            if args[2] <= 1:
                new_y = round(ws_rect.y + args[2]*(ws_rect.height + bar_height) - border_size)
            else:
                new_y = round(ws_rect.y + args[2] - border_size)
        
        print(new_x, new_y)
        if execute_command: subprocess.call(["i3-msg", "move", "position", str(new_x), str(new_y)])
        return 0

    elif args[0] == "size":
        ratio_match = re.compile('([0-9.]+)\:([0-9.]+)').match(args[1])
        if ratio_match:
            try:
                new_h = int(args[2])
            except:
                print("invalid height argument")
                return 1

            new_w = round(new_h*float(ratio_match.group(1))/float(ratio_match.group(2)))

            new_w = new_w + 2*border_size
            new_h = new_h + 2*border_size
        
        else:
            try:
                (args[1], args[2]) = (float(args[1]), float(args[2]))
                if args[1] < 0 or args[2] < 0:
                    raise Exception
            except:
                print("invalid arguments")
                return 1

            if args[1] <= 1:
                new_w = round(args[1]*ws_rect.width + 2*border_size)
            else:
                new_w = round(args[1] + 2*border_size)

            if args[2] <= 1:
                new_h = round(args[2]*(ws_rect.width+bar_height) + 2*border_size)
            else:
                new_h = round(args[2] + 2*border_size)

        print(new_w, new_h)
        if execute_command: subprocess.call(["i3-msg", "resize", "set", str(new_w), str(new_h)])
        return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
