#!/bin/bash

pad_left=" "
pad_right="$(printf ' %.0s' {1..${#1}})"

for i in {1..40}; do
    num=$[$RANDOM % ${1:-6} + 1]
    echo -en "$pad_left[$num]$pad_right\r"
    sleep 0.05
done

echo -en "\n"

test "$num" == "1" && (echo -n "$pad_left"; echo "BANG!") || (echo -n "$pad_left"; echo "Lucky boy...")
