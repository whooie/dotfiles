#!/bin/bash

#source_name=$(i3-msg -t get_workspaces | sed 's/"rect":{//g' | tr "{" "\n" | grep "\"focused\":true" | awk 'BEGIN{FS=","}{print $2}' | awk 'BEGIN{FS="\""}{print $4}')

source_name=$(i3-msg -t get_workspaces | jq -M '.[]
    | {f: .focused, n: .name}
    | select(.f == true)
    | .n' | tr -d '"')

#case "$1" in
#    10)
#        target_name="10:♪"
#        ;;
#    11)
#        target_name="11:#"
#        ;;
#    12)
#        target_name="12:!"
#        ;;
#    13)
#        target_name="13:$"
#        ;;
#    *)
#        target_name="$1"
#        ;;
#esac

if [[ $(i3-msg -t get_workspaces) = *'"name":'\"$1\"* ]]; then
    i3-msg "rename workspace $1 to _"
    i3-msg "rename workspace to $1"
    i3-msg "rename workspace _ to $source_name"
else
    i3-msg "rename workspace to $1"
fi
