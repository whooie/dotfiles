#!/bin/bash

(tmux ls | awk '{print $1}' | grep -q "$1:") && echo "session exists" && exit 1
main_width=137

# note: greek alphabet unicodes start from U+03b1 (lower case) and U+0391 (upper case)

case $1 in
    # can also use
    # tmux send-keys -t "session:window.pane" "command"
    table)
        thedir='/home/whooie'
        sessid='table'
        tmux new-session -s "$sessid" -d
        tmux rename-window "HOME"
        tmux set-window-option -t "$sessid:HOME" main-pane-width "$main_width"

        ## rust
        #winid="rust"
        #tmux new-window -t "$sessid" -d -n "$winid" -c "$thedir/00/tests/rust test/testing/src" "vim main.rs; nu; bash -ic dev"
        #tmux split-window -h -t "$sessid:$winid" -c "$thedir/00/tests/rust test/testing" "nu; bash -ic dev"
        #tmux select-layout -t "$sessid:$winid" even-horizontal
        #tmux select-pane -t "$sessid:$winid.0"

        ## i3-window
        #winid="i3-window"
        #tmux new-window -t "$sessid" -d -n "$winid" -c "$thedir/.scripts" "vim i3-window.py; bash -ic dev"
        #tmux split-window -h -t "$sessid:$winid" -c "$thedir/.scripts" "nu; bash -ic dev"
        #tmux select-layout -t "$sessid:$winid" even-horizontal
        #tmux select-pane -t "$sessid:$winid.0"

        # pyo3
        winid="pyo3"
        windir="$thedir/00/tests/pyo3/pyo3_test"
        tmux new-window -t "$sessid" -n "$winid" -c "$windir/src" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux split-window -h -t "$sessid:$winid" -c "$windir" "bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$windir" "bash -ic dev"
        tmux select-layout -t "$sessid:$winid" main-vertical
        tmux select-pane -t "$sessid:$winid.0"
        tmux resize-pane -t "$sessid:$winid.0" -x $main_width
        tmux resize-pane -t "$sessid:$winid.1" -D 10

        # libSr-rust
        winid="srsolve"
        windir="$thedir/Documents/Rice/00research/02-killian/06-2019/python/srsolve"
        tmux new-window -t "$sessid" -n "$winid" -c "$windir/libSr" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$windir/libSr_rs" "bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$windir" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" main-vertical
        tmux resize-pane -t "$sessid:$winid.1" -D 10
        tmux select-pane -t "$sessid:$winid.0"

        # epr-dnd
        winid="epr-dnd"
        windir="$thedir/Documents/epr-dnd"
        tmux new-window -t "$sessid" -n "$winid" -c "$windir/notes/src" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$windir/notes/src" "vim SUMMARY.md; bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$windir/notes/src" "bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$windir/notes" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" main-vertical
        tmux select-pane -t "$sessid:$winid.0"

        # writer's bloch
        winid="bloch"
        windir="$thedir/Documents/writers-bloch"
        tmux new-window -t "$sessid" -n "$winid" -c "$windir/src" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$windir/src" "vim SUMMARY.md; bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$windir/src" "bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$windir" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" main-vertical
        tmux select-pane -t "$sessid:$winid.0"

        tmux select-window -t "$sessid:pyo3"
        ;;

    notes)
        thedir='/home/whooie/Documents/Rice/00notes'
        sessid='notes'
        tmux new-session -s "$sessid" -d -c "$thedir"
        tmux rename-window "HOME"
        tmux set-window-option -t "$sessid:HOME" main-pane-width "$main_width"

        # main
        winid="alpha"
        tmux new-window -t "$sessid" -d -n "$winid" -c "$thedir"
        tmux split-window -h -t "$sessid:$winid" -c "$thedir"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" even-horizontal

        # subdoc
        winid="beta"
        tmux new-window -t "$sessid" -d -n "$winid" -c "$thedir"
        tmux split-window -h -t "$sessid:$winid" -c "$thedir"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" even-horizontal

        tmux select-window -t "$sessid:alpha"
        ;;

    coursework)
        thedir='/home/whooie/Documents/UIUC/00courses'
        sessid='coursework'
        tmux new-session -s "$sessid" -d -c "$thedir" "bash -ic dev"
        tmux rename-window "HOME"
        tmux set-window-option -t "$sessid:HOME" main-pane-width "$main_width"

        # course 1
        winid="phys513"
        tmux new-window -t "$sessid" -n "$winid" -c "$thedir/phys513" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$thedir/phys513" "bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "/home/whooie/Downloads/00hw" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" main-vertical
        tmux resize-pane -t "$sessid:$winid.1" -D 10
        tmux select-pane -t "$sessid:$winid.0"
        
        # course 2
        winid="phys498"
        tmux new-window -t "$sessid" -n "$winid" -c "$thedir/phys498" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$thedir/phys498" "bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$thedir/phys498" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" main-vertical
        tmux resize-pane -t "$sessid:$winid.1" -D 10
        tmux select-pane -t "$sessid:$winid.0"
        
        tmux select-window -t "$sessid:HOME"
        ;;

    research)
        thedir='/home/whooie/Documents/UIUC'
        sessid='research'
        tmux new-session -s "$sessid" -d -c "$thedir" "bash -ic dev"
        tmux rename-window "HOME"
        tmux set-window-option -t "$sessid:HOME" main-pane-width "$main_width"

        # docs
        winid="docs"
        tmux new-window -t "$sessid" -d -n "$winid" -c "$thedir" "bash"
        tmux split-window -h -t "$sessid:$winid" -c "$thedir/00readings" "bash"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" even-horizontal

        # lab notes
        winid="lab-notes"
        windir="$thedir/00lab-notes"
        tmux new-window -t "$sessid" -n "$winid" -c "$windir/notes/src" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$windir/notes/src" "vim SUMMARY.md; bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$windir/notes/src" "bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$windir/notes" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" main-vertical
        tmux select-pane -t "$sessid:$winid.0"

        # entangleware control
        winid="ew-control"
        windir="$thedir/01ew-control"
        tmux new-window -t "$sessid" -n "$winid" -c "$windir" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$windir" "bash -ic dev"
        tmux split-window -v -t "$sessid:$winid" -c "$windir" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" main-vertical
        tmux resize-pane -t "$sessid:$winid.1" -D 10
        tmux select-pane -t "$sessid:$winid.0"

        # free
        winid="alpha"
        tmux new-window -t "$sessid" -d -n "$winid" -c "$thedir" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$thedir" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" even-horizontal

        # free
        winid="beta"
        tmux new-window -t "$sessid" -d -n "$winid" -c "$thedir" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$thedir" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" even-horizontal

        # free
        winid="gamma"
        tmux new-window -t "$sessid" -d -n "$winid" -c "$thedir" "bash -ic dev"
        tmux split-window -h -t "$sessid:$winid" -c "$thedir" "bash -ic dev"
        tmux set-window-option -t "$sessid:$winid" main-pane-width "$main_width"
        tmux select-layout -t "$sessid:$winid" even-horizontal

        tmux select-window -t "$sessid:lab-notes"
        ;;

    *)
        echo "session is not pre-programmed"
        ;;
esac
tmux -2 attach -t "$sessid"

