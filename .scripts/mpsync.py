#!/usr/bin/python

import sys
import os
import os.path as path
import shutil
import getopt
import re
import time
config = type("Config", (), dict(
    status=False,
    skips=False,
    color=False
))()

home = "/home/whooie"
home_music = path.join(home, "Music")
home_playlists = path.join(home, ".config/mpd/playlists")

player_home = "/media/music"
player_music = path.join(player_home, "Music")
player_playlists = path.join(player_home, "Playlists")
player_prefix = "/<microSD1>/Music"

playlist_fileext = ".m3u"
playlist_merges = ["Earworms", "Lyricals", "Soundtracks"]

main_playlists = [
    "Background",
    # "Boop",
    "Classic Rock",
    # "Earworms 1",
    # "Earworms 2",
    "Instrumentals",
    "Math_Prog",
    # "Lyricals 1",
    # "Lyricals 2",
    "Running Mix",
    "Salient",
    "Supersalient",
    "Saline",
    "Soundtracks 1",
    "Soundtracks 2",
    "Soundtracks 3",
    "Yorushika",
    "Yosugi",
]

# blacklists
blacklist_artists = [
    "7!!",
    "38 Special",
    "Aaron Copland",
    "AC-DC",
    "Aerosmith",
    "Alex S",
    "Alisa Takigawa",
    "Aoi Eir",
    "Arai Akino",
    "Asami Imai",
    "Ayano Mashiro",
    "Beatles, The",
    "BlackGryph0n",
    "Black Keys, The",
    "Bob Seger",
    "Bryan Adams",
    "ClariS",
    "Curtis Schweitzer",
    "Daniel Ingram",
    "David Rolfe",
    "Dismemberment Plan, The",
    "Eagles, The",
    "ENA",
    "Eric Johnson",
    "Eurobeat Brony",
    "Foghat",
    "Fukuhara Miho",
    "Galileo Galilei",
    "Game Freak & Shota Kageyama",
    "Green Day",
    "Hanekawa Tsubasa (Horie Yui)",
    "Hayami Saori & Touyama Nao",
    "Huey Lewis & The News",
    "Iwasaki Taisei",
    "John Williams",
    "Kalafina",
    "Kana-Boon",
    "Kanbaru Suruga (Sawashiro Miyuki)",
    "Konomi Suzuki",
    "Kyle Athayde Dance Party",
    "Nana Kitade",
    "Niel Zaza",
    "Nintendo",
    "Ozzy Osbourne",
    "Phil Collins",
    "PJ Lequerica",
    "Protomen, The",
    "Queen",
    "REO Speedwagon",
    "Rob Thomas",
    "Russell Velazquez",
    "Sangatsu no Phantasia",
    "Satoru Kosaki",
    "Sayuri",
    "Scenarioart",
    "Sengoku Nadeko (Hanazawa Kana)",
    "Senjougahara Hitagi (Saitou Chiwa)",
    "Shigatsu wa Kimi no Uso",
    "Sizzlebird"
    "Smashing Pumpkins",
    "Streetlight Manifesto",
    "Styx",
    "Survivor",
    "Tara Strong",
    "Tomatsu Haruka",
    "ZZ Top"
]
blacklist_albums = [
    "1984",
    "50 Most Essential Pieces of Classical Music, The",
    "Ame no Umi",
    "Aqua Terrarium",
    "Axis Bold As Love",
    "Big Dark Love",
    "Dear Answer",
    "FLCL Progressive-Alternative Complete Box Set - Disk 2",
    "FLCL Progressive-Alternative Complete Box Set - Disk 3",
    "Gamecube Controller Whitenoise",
    "Gekkan Shoujo Nozaki-kun Vol.3 Special CD Complete Soundtrack",
    "Its Easier To Be Somebody Else",
    "polyomino",
    "Poutine Split",
    "Sound Checks",
    "Sweet Track",
    "Trinity",
    "Ultimate Collection, The",
    "Walk On",
    "We Need Medicine"
]
exclude_patterns = [
    re.compile(r".*- Arienai.*"),
    re.compile(r".*\.jpg"),
    re.compile(r".*\.png"),
    re.compile(r".*\.pdf"),
    re.compile(r".*\.cue"),
    re.compile(r".*\.log"),
    re.compile(r".*\.txt"),
    re.compile(r".*\.rar"),
    re.compile(r".*\.tif"),
    re.compile(r".*\.dsf"),
    re.compile(r".*- Radio.*"),
    re.compile(r"[0-9]{2} ROR2.*\([0-9]{,2}-[0-9]{,2}-[0-9]{,2}\)\.flac"),
    re.compile(r"blacklist/*"),
]

blacklist_artists.sort()
blacklist_albums.sort()
blacklist = set(blacklist_artists + blacklist_albums)

help_text = f"""
Usage:  mpsync [ -spc ] [ -pmRibh ]"

Operations:
    -p, --sync-playlists
        Sync playlists to player playlist directory.
    -m, --sync-music
        Sync music to player music directory.
    -R, --remove-blacklist
        Remove blacklisted items from the player music directory, if they exist.
    -b, --print-blacklist
        Print blacklist items.
    -i, --last-sync
        Print last sync date/time.
    -h, --help
        Print this info and exit.

Options:
    -S, --print-status
        If not otherwise hidden, print the status of the examinations of music
        files and directories.
    -P, --print-skipped
        Print skipped files alongside copied files, both with copy/skip status.
    -C, --color-output
        Print copies/skipped files with color.

Home Dir = {home}
Home Music Dir = {home_music}
Home Playlist Dir = {home_playlists}
Player Dir = {player_home}
Player Music = {player_music}
Player Playlist Dir = {player_playlists}
Player Prefix = {player_prefix}
Playlist File Extension = {playlist_fileext}
Playlist Merges = {playlist_merges}
"""

def exclude_test(s):
    for pattern in exclude_patterns:
        if pattern.match(s) is not None:
            return True
    return False

def dfscp(sourcedir, targetdir, offset, status, skips, color):
    c = "\033[90m"
    b = "\033[1m"
    n = "\033[0m"
    t = path.split(targetdir)[1]
    dir_exclude = lambda x: skips*(color*c + offset+x+"/ : "+(color*b+"EXCLUDE") + color*n + "\n")
    dir_exists = lambda x: color*c + offset+x+"/" + (status or skips)*(" : "+(color*b+"EXISTS")) + color*n + "\n"
    dir_create = lambda x: offset+x+"/" + (status or skips)*(" : "+(color*b+"CREATE")) + color*n + "\n"
    file_exclude = lambda x: skips*(color*c + offset+"  "+x+" : "+(color*b+"EXCLUDE") + color*n + "\n")
    file_exists = lambda x: skips*(color*c + offset+"  "+x+" : "+(color*b+"EXISTS") + color*n + "\n")
    file_copy = lambda x: offset+"  "+x+ + (status or skips)*(" : "+(color*b+"COPY")) + color*n + "\n"

    if (t in blacklist) or (exclude_test(t)):
        print(dir_exclude(t), end="")
        return
    elif path.exists(targetdir):
        print(dir_exists(t), end="")
    else:
        print(dir_create(t), end="")
        os.mkdir(targetdir)

    source_contents = os.listdir(sourcedir)
    source_contents.sort()
    for item in source_contents:
        S = path.join(sourcedir, item)
        T = path.join(targetdir, item)
        if path.isdir(S) and len(os.listdir(S)) > 0:
            dfscp(S, T, offset+"  ", status, skips, color)
        elif path.isfile(T):
            if path.getctime(S) > path.getctime(T):
                if item not in blacklist and not exclude_test(item):
                    print(file_copy(item), end="")
                    shutil.copy2(S, T)
                else:
                    print(file_exclude(item), end="")
            else:
                print(file_exists(item), end="")
        else:
            if item not in blacklist and not exclude_test(item):
                print(file_copy(item), end="")
                shutil.copy2(S, T)
            else:
                print(file_exclude(item), end="")
    return

def sync_music(sourcedir, targetdir, status, skips, color):
    if not path.isdir(targetdir):
        print(":: mkdir "+targetdir)
        os.mkdir(targetdir)
    print(":: Copying recursively from "+sourcedir+" to "+targetdir)
    dfscp(sourcedir, targetdir, "", status, skips, color)
    outfile = open(path.join(targetdir, "last_sync"), 'w')
    outfile.write("Music last synced: "+timedate())
    outfile.close()
    return

def copy_playlist(sourcefilename, targetfilename, player_prefix, win_drive=None):
    infile = open(sourcefilename, 'r')
    playlist = infile.readlines()
    infile.close()
    for i in range(len(playlist)):
        playlist[i] = path.join(player_prefix, playlist[i])
        if win_drive is not None:
            playlist[i] = win_drive+(len(win_drive)>0)*":"+playlist[i].replace("/", "\\")
    outfile = open(targetfilename, 'w')
    outfile.writelines(playlist)
    outfile.close()
    return

def cat_files(targetfilename, *sourcefilenames):
    lines = list()
    for sourcefilename in sourcefilenames:
        infile = open(sourcefilename, 'r')
        lines += infile.readlines()
        infile.close()
    outfile = open(targetfilename, 'w')
    outfile.writelines(lines)
    outfile.close()
    return

def sync_playlists(sourcedir, targetdir, player_prefix, playlist_fileext, merges):
    if not path.isdir(targetdir):
        print(":: mkdir "+targetdir)
        os.mkdir(targetdir)
    print(":: Copying *"+playlist_fileext+" playlists from "+sourcedir+" to "+targetdir)
    playlists = list(filter(lambda x: path.splitext(x)[1] == playlist_fileext, os.listdir(sourcedir)))
    playlists.sort()
    for playlist in playlists:
        S = path.join(sourcedir, playlist)
        T = path.join(targetdir, playlist)
        print("  "+playlist)
        copy_playlist(S, T, player_prefix)
    print(":: Performing playlist mergers:")
    playlists = os.listdir(targetdir)
    playlists.sort()
    merge_list = [[p for p in playlists if merge + " " in p] for merge in merges]
    for i in range(len(merges)):
        print(" ", *[p+" " for p in merge_list[i]], "--> ", merges[i]+playlist_fileext)
        merge_list[i] = [path.join(targetdir, p) for p in merge_list[i]]
        cat_files(path.join(targetdir, merges[i]+playlist_fileext), *merge_list[i])
    outfile = open(path.join(targetdir, "last_sync"), 'w')
    outfile.write("Playlists last synced: "+timedate())
    outfile.close()
    return

def copy_structured(sourcedir, sourcefile, targetdir):
    dirs = sourcefile.replace(sourcedir, "").split("/")[:-1]
    while "" in dirs:
        dirs.remove("")
    T = targetdir
    for t in dirs:
        T = path.join(T, t)
        if not path.isdir(T):
            os.mkdir(T)
    shutil.copy2(sourcefile, T)
    return

def pad_int(n, N):
    return (len(str(N))-len(str(n)))*"0"+str(n)

def sync_by_playlists(source_musicdir, source_playlistdir,
        target_musicdir, target_playlistdir,
        player_prefix, playlists, playlist_fileext, merges, win_drive=None,
        symlinks=False):
    if not path.isdir(target_musicdir):
        print(":: mkdir "+target_musicdir)
        os.mkdir(target_musicdir)
    if not path.isdir(target_playlistdir):
        print(":: mkdir "+target_playlistdir)
        os.mkdir(target_playlistdir)
    playlists.sort()
    print(":: Copying playlists from "+source_playlistdir+" to "+target_playlistdir)
    for playlist in playlists:
        print("  "+playlist+":")
        if not symlinks:
            S = path.join(source_playlistdir, playlist+playlist_fileext)
            T = path.join(target_playlistdir, playlist+playlist_fileext)
            copy_playlist(S, T, player_prefix, win_drive)
        else:
            S = path.join(source_playlistdir, playlist+playlist_fileext)
            T = path.join(target_playlistdir, playlist)
            if not path.isdir(T):
                print("  :: mkdir "+T)
                os.mkdir(T)
        infile = open(S, 'r')
        filenames = [x.replace("\n", "") for x in infile.readlines()]
        infile.close()
        k = 1
        for filename in filenames:
            s = path.join(source_musicdir, filename)
            t = path.join(target_musicdir, filename)
            if path.isfile(t):
                pass
                # if path.getctime(s) > path.getctime(t):
                #     print("    "+filename)
                #     copy_structured(source_musicdir, s, target_musicdir)
            else:
                print("    "+filename)
                copy_structured(source_musicdir, s, target_musicdir)
            # if not path.isfile(t) or path.getctime(s) > path.getctime(t):
            #     print("    "+filename)
            #     copy_structured(source_musicdir, s, target_musicdir)
            if symlinks:
                _t = path.join(T, pad_int(k, len(filenames))+" "+filename.split("/")[-1])
                if not path.islink(_t):
                    os.symlink(t, _t)
                k += 1
    print(":: Performing playlist mergers:")
    _playlists = os.listdir(target_playlistdir)
    _playlists.sort()
    merge_list = [[p for p in _playlists if merge + " " in p] for merge in merges]
    for i in range(len(merges)):
        if not symlinks:
            print(" ", *[p+" " for p in merge_list[i]], "--> ", merges[i]+playlist_fileext)
            merge_list[i] = [path.join(target_playlistdir, p) for p in merge_list[i]]
            cat_files(path.join(target_playlistdir, merges[i]+playlist_fileext), *merge_list[i])
        else:
            print(" ", *[p+" " for p in merge_list[i]], "--> ", merges[i])
            T = path.join(target_playlistdir, merges[i])
            if not path.isdir(T):
                print("  mkdir "+T)
                os.mkdir(T)
            allfiles = list()
            for p in merge_list[i]:
                _p = path.join(target_playlistdir, p)
                newfiles = [path.join(_p, x) for x in os.listdir(_p)]
                newfiles.sort()
                allfiles += newfiles
            k = 1
            for s in allfiles:
                t = path.join(T, pad_int(k, len(allfiles))+" "+s.split("/")[-1])
                if not path.islink(t):
                    os.symlink(s, t)
                k += 1
    return

def print_blacklist(color):
    b = "\033[1m"
    n = "\033[0m"
    print(color*b + "The following have been blacklisted:" + color*n)
    print(color*b + "Artists:" + color*n)
    for artist in blacklist_artists:
        print("  "+artist)
    print(color*b + "Albums:" + color*n)
    for album in blacklist_albums:
        print("  "+album)
    print(color*b + "General Patterns:" + color*n)
    for pattern in exclude_patterns:
        print("  "+pattern.pattern)
    return

def get_last_sync(player_playlists, player_music):
    infilename = path.join(player_playlists, "last_sync")
    if path.isfile(infilename):
        infile = open(infilename, 'r')
        print(infile.read(), end="")
        infile.close()
    else:
        print(infilename+" does not exist")
    infilename = path.join(player_music, "last_sync")
    if path.isfile(infilename):
        infile = open(infilename, 'r')
        print(infile.read(), end="")
        infile.close()
    else:
        print(infilename+" does not exist")
    return
    
def remove_blacklist(targetdir, blacklist_artists, blacklist_albums):
    for artist in blacklist_artists:
        A = path.join(targetdir, artist)
        if path.isdir(A):
            shutil.rmtree(A, ignore_errors=True)
            print("Remove "+A)
        for album in blacklist_albums:
            B = path.join(A, album)
            if path.isdir(B):
                shutil.rmtree(B, ignore_errors=True)
                print("Remove "+B)
    return

def timedate():
    T = time.localtime()
    return time.strftime("%a %Y.%m.%d %H:%M:%S") \
            + f" {time.tzname[T.tm_isdst]}\n"

def main(argv):
    shortopts = "pmlRbiSPCh"
    longopts = [
        "sync-playlists",
        "sync-music",
        "sync-by-playlists",
        "remove-blacklist",
        "print-blacklist",
        "last-sync",
        "print-status",
        "print-skipped",
        "color-output",
        "help"
    ]
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
    except getopt.GetoptError as ERR:
        print(help_text)
        raise ERR
    for opt, optarg in opts:
        if opt in ["-p", "--sync-playlists"]:
            sync_playlists(
                home_playlists,
                player_playlists,
                player_prefix,
                playlist_fileext,
                playlist_merges
            )
        elif opt in ["-m", "--sync-music"]:
            sync_music(
                home_music,
                player_music,
                config.status,
                config.skips,
                config.color
            )
        elif opt in ["-l", "--sync-by-playlists"]:
            sync_by_playlists(
                home_music,
                home_playlists,
                player_music,
                player_playlists,
                "/storage/75E3-15E6/Music",
                main_playlists,
                playlist_fileext,
                playlist_merges,
                None,
                False
            )
        elif opt in ["-R", "--remove-blacklist"]:
            remove_blacklist(
                player_music,
                blacklist_artists,
                blacklist_albums
            )
        elif opt in ["-b", "--print-blacklist"]:
            print_blacklist(config.color)
        elif opt in ["-i", "--last-sync"]:
            get_last_sync(
                player_playlists,
                player_music
            )
        elif opt in ["-S", "--status"]:
            config.status = True
        elif opt in ["-P", "--print-skipped"]:
            config.skips = True
        elif opt in ["-C", "--color-output"]:
            config.color = True
        elif opt in ["-h", "--help"]:
            print(help_text)
            return 0
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
