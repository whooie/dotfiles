#!/bin/bash

display_offset=$(i3-msg -t get_workspaces | sed 's/"rect":{//g' | tr "{" "\n" | grep "\"focused\":true" | awk 'BEGIN{FS=","}{print $5}' | awk 'BEGIN{FS=":"}{print $2}')
echo $display_offset
