"""
Virtual Rubik's cubes.
"""

import copy
import random
import re

def concat_multiline_str(str1, str2):
    lines1 = str1.split('\n')
    lines2 = str2.split('\n')
    outstr = ""
    while len(lines1) > 0 or len(lines2) > 0:
        try:
            line1 = lines1.pop(0)
        except:
            line1 = ""
        try:
            line2 = lines2.pop(0)
        except:
            line2 = ""
        outstr = outstr + line1 + line2 + '\n'
    return outstr[:-1]

def concat_multiline_strs(*strs):
    outstr = ''
    for string in strs:
        outstr = concat_multiline_str(outstr, string)
    return outstr

class Face:
    def __init__(self, face=None, truecolor=False):
        """
        Constructor for Face objects.

        INPUTS:
            face - 3x3 nested lists of strings, each of which must be one of
                    'B'/'b' (blue)
                    'G'/'g' (green)
                    'O'/'o' (orange)
                    'R'/'r' (red)
                    'W'/'w' (white)
                    'Y'/'y' (yellow)
                  -OR-
                a single character in the above set.
            truecolor - Print using 24-bit colors.
        """
        bad_face_err = "Invalid face supplied"
        if type(face).__name__ == "NoneType":
            self.face = [['W', 'W', 'W'], ['W', 'W', 'W'], ['W', 'W', 'W']]
        elif type(face).__name__ == "list" and len(face) == 3:
            for row in face:
                if type(row).__name__ == "list" and len(row) == 3:
                    for color in row:
                        if color not in ['B','b','G','g','O','o','R','r','Y','y','W','w']:
                            raise Exception(bad_face_err)
                else:
                    raise Exception(bad_face_err)
            self.face = [[color.upper() for color in row] for row in face]
        elif face in ['B','b','G','g','O','o','R','r','Y','y','W','w']:
            self.face = [[face.upper() for j in range(3)] for i in range(3)]
        else:
            raise Exception(bad_face_err)
        self.truecolor(truecolor)

    def truecolor(self, c=True):
        if c:
            self.colors = {
                    'B': "\x1b[1;7;38;2;32;64;208m",
                    'G': "\x1b[1;7;38;2;0;144;0m",
                    'O': "\x1b[1;7;38;2;255;96;32m",
                    'R': "\x1b[1;7;38;2;208;0;0m",
                    'W': "\x1b[1;7;38;2;255;255;255m",
                    'Y': "\x1b[1;7;38;2;255;255;0m",
                    'n': "\x1b[0m"
                }
        else:
            self.colors = {
                    'B': "\033[1;7;94m",
                    'G': "\033[1;7;92m",
                    'O': "\033[1;7;95m",
                    #'O': "\033[1;7;33m",
                    'R': "\033[1;7;91m",
                    'W': "\033[1;7;97m",
                    'Y': "\033[1;7;93m",
                    'n': "\033[0m"
                }

    def __getitem__(self, pos):
        bad_index_err = "Invalid indices supplied: must be two ints in range [0,2]"
        if type(pos).__name__ == "tuple" and len(pos) == 2:
            i,j = pos
            if not (type(i).__name__ == type(j).__name__ == "int"):
                raise Exception(bad_index_err)
            return self.face[i][j]
        else:
            raise Exception(bad_index_err)

    def __setitem__(self, pos, val):
        bad_index_err = "Invalid indices supplied: must be two ints in range [0,2]"
        if type(pos).__name__ == "tuple" and len(pos) == 2:
            i,j = pos
            if not (type(i).__name__ == type(j).__name__ == "int"):
                raise Exception(bad_index_err)
            if val in ['B','b','G','g','O','o','R','r','Y','y','W','w']:
                self.face[i][j] = val
            else:
                raise Exception(bad_index_err)
        else:
            raise Exception(bad_index_err)

    def _color_cubee(self, c):
        if c in ['B', 'G', 'O', 'R', 'W', 'Y']:
            return self.colors[c]+"     "+self.colors['n'] \
                    + self.colors[c]+"  "+c+"  "+self.colors['n'] \
                    + self.colors[c]+"     "+self.colors['n']
        else:
            raise Exception("Invalid face color; not really sure how it got here")

    def __str__(self):        
        output = ".-----------.\n"
        for i in range(len(self.face)):
            rowstr = "|"
            for cubee in self.face[i]:
                rowstr = rowstr + self.colors[cubee] + " " + cubee + " " + self.colors['n'] + "|"
            output = output + rowstr + "\n"
            output = output + ("|---+---+---|\n" if i != 2 else "'-----------'")
        return output

    def rotate(self, ccw=False):
        rotated = list(zip(*reversed(self.face))) if not ccw else list(zip(*(self.face)))[::-1]
        rotated = [list(row) for row in rotated]
        self.face = copy.deepcopy(rotated)

class Cube:
    def __init__(self, faces=None, moves='', printmoves=False, truecolor=False):
        """
        Constructor for Cube objects.

        INPUTS:
            faces - dict mapping 'F','B','R','L','U','D' to Face objects.
            moves - str of space-delineated moves (e.g. 'R2 L U'') to apply
                after the initial creation of the cube.
            printmoves - print the cube after every call of the `move` method.
            truecolor - print using 24-bit colors.
        """
        bad_faces_err = "Invalid cube supplied"
        if type(faces).__name__ == "NoneType":
            self.FS = Face('B', truecolor)
            self.BS = Face('G', truecolor)
            self.RS = Face('R', truecolor)
            self.LS = Face('O', truecolor)
            self.US = Face('Y', truecolor)
            self.DS = Face('W', truecolor)
        elif type(faces).__name__ == "dict":
            facelabels = ['F', 'B', 'R', 'L', 'U', 'D']
            if set(facelabels).issubset(set(faces.keys())):
                for facelabel in facelabels:
                    if type(faces[facelabel]).__name__ != "Face":
                        raise Exception(bad_faces_err)
                self.FS = copy.deepcopy(faces['F'])
                self.BS = copy.deepcopy(faces['B'])
                self.RS = copy.deepcopy(faces['R'])
                self.LS = copy.deepcopy(faces['L'])
                self.US = copy.deepcopy(faces['U'])
                self.DS = copy.deepcopy(faces['D'])
        else:
            raise Exception(bad_faces_err)
        self.do_print = False
        self.move(moves)
        self.do_print = printmoves
        self.tc = truecolor
        self.truecolor(truecolor)

    def reset(self):
        self.FS = Face('B', self.tc)
        self.BS = Face('G', self.tc)
        self.RS = Face('R', self.tc)
        self.LS = Face('O', self.tc)
        self.US = Face('Y', self.tc)
        self.DS = Face('W', self.tc)
        if self.do_print: print(self)

    def truecolor(self, c=True):
        self.tc = c
        for face in [self.FS, self.BS, self.RS, self.LS, self.US, self.DS]:
            face.truecolor(c)

    def printmoves(self, p=True):
        self.do_print = p

    #def F(self, p=False):
    #    self.FS.rotate(p)
    #    old = [self.RS, self.DS, self.LS, self.US]
    #    new = copy.deepcopy(old)
    #    di = 2*p - 1
    #    for i in range(4):
    #        for k in range(3):
    #            new[i][k,0] = old[(i+di)%4][2*(not p), 2*p+(-2*p+1)*k]
    #    [self.RS, self.DS, self.LS, self.US] = new

    def F(self):
        self.FS.rotate(ccw=False)
        [r_new, l_new, u_new, d_new] = copy.deepcopy([self.RS, self.LS, self.US, self.DS])
        for k in range(3):
            r_new[k,0] = self.US[2,k]
            l_new[k,2] = self.DS[0,k]
            u_new[2,k] = self.LS[2-k,2]
            d_new[0,k] = self.RS[2-k,0]
        [self.RS, self.LS, self.US, self.DS] = [r_new, l_new, u_new, d_new]
        if self.do_print: print(self)

    def Fp(self):
        self.FS.rotate(ccw=True)
        [r_new, l_new, u_new, d_new] = copy.deepcopy([self.RS, self.LS, self.US, self.DS])
        for k in range(3):
            r_new[k,0] = self.DS[0,2-k]
            l_new[k,2] = self.US[2,2-k]
            u_new[2,k] = self.RS[k,0]
            d_new[0,k] = self.LS[k,2]
        [self.RS, self.LS, self.US, self.DS] = [r_new, l_new, u_new, d_new]
        if self.do_print: print(self)

    def F2(self):
        printstate = self.do_print
        self.do_print = False
        self.F(); self.F();
        self.do_print = printstate
        if self.do_print: print(self)

    def B(self):
        self.BS.rotate(ccw=False)
        [r_new, l_new, u_new, d_new] = copy.deepcopy([self.RS, self.LS, self.US, self.DS])
        for k in range(3):
            r_new[k,2] = self.DS[2,2-k]
            l_new[k,0] = self.US[0,2-k]
            u_new[0,k] = self.RS[k,2]
            d_new[2,k] = self.LS[k,0]
        [self.RS, self.LS, self.US, self.DS] = [r_new, l_new, u_new, d_new]
        if self.do_print: print(self)

    def Bp(self):
        self.BS.rotate(ccw=True)
        [r_new, l_new, u_new, d_new] = copy.deepcopy([self.RS, self.LS, self.US, self.DS])
        for k in range(3):
            r_new[k,2] = self.US[0,k]
            l_new[k,0] = self.DS[2,k]
            u_new[0,k] = self.LS[2-k,0]
            d_new[2,k] = self.RS[2-k,2]
        [self.RS, self.LS, self.US, self.DS] = [r_new, l_new, u_new, d_new]
        if self.do_print: print(self)

    def B2(self):
        printstate = self.do_print
        self.do_print = False
        self.B(); self.B();
        self.do_print = printstate
        if self.do_print: print(self)

    def L(self):
        self.LS.rotate(ccw=False)
        [f_new, b_new, u_new, d_new] = copy.deepcopy([self.FS, self.BS, self.US, self.DS])
        for k in range(3):
            f_new[k,0] = self.US[k,0]
            b_new[k,2] = self.DS[2-k,0]
            u_new[k,0] = self.BS[2-k,2]
            d_new[k,0] = self.FS[k,0]
        [self.FS, self.BS, self.US, self.DS] = [f_new, b_new, u_new, d_new]
        if self.do_print: print(self)

    def Lp(self):
        self.LS.rotate(ccw=True)
        [f_new, b_new, u_new, d_new] = copy.deepcopy([self.FS, self.BS, self.US, self.DS])
        for k in range(3):
            f_new[k,0] = self.DS[k,0]
            b_new[k,2] = self.US[2-k,0]
            u_new[k,0] = self.FS[k,0]
            d_new[k,0] = self.BS[2-k,2]
        [self.FS, self.BS, self.US, self.DS] = [f_new, b_new, u_new, d_new]
        if self.do_print: print(self)

    def L2(self):
        printstate = self.do_print
        self.do_print = False
        self.L(); self.L();
        self.do_print = printstate
        if self.do_print: print(self)

    def R(self):
        self.RS.rotate(ccw=False)
        [f_new, b_new, u_new, d_new] = copy.deepcopy([self.FS, self.BS, self.US, self.DS])
        for k in range(3):
            f_new[k,2] = self.DS[k,2]
            b_new[k,0] = self.US[2-k,2]
            u_new[k,2] = self.FS[k,2]
            d_new[k,2] = self.BS[2-k,0]
        [self.FS, self.BS, self.US, self.DS] = [f_new, b_new, u_new, d_new]
        if self.do_print: print(self)
    
    def Rp(self):
        self.RS.rotate(ccw=True)
        [f_new, b_new, u_new, d_new] = copy.deepcopy([self.FS, self.BS, self.US, self.DS])
        for k in range(3):
            f_new[k,2] = self.US[k,2]
            b_new[k,0] = self.DS[2-k,2]
            u_new[k,2] = self.BS[2-k,0]
            d_new[k,2] = self.FS[k,2]
        [self.FS, self.BS, self.US, self.DS] = [f_new, b_new, u_new, d_new]
        if self.do_print: print(self)
    
    def R2(self):
        printstate = self.do_print
        self.do_print = False
        self.R(); self.R();
        self.do_print = printstate
        if self.do_print: print(self)

    def U(self):
        self.US.rotate(ccw=False)
        [f_new, b_new, r_new, l_new] = copy.deepcopy([self.FS, self.BS, self.RS, self.LS])
        for k in range(3):
            f_new[0,k] = self.RS[0,k]
            b_new[0,k] = self.LS[0,k]
            r_new[0,k] = self.BS[0,k]
            l_new[0,k] = self.FS[0,k]
        [self.FS, self.BS, self.RS, self.LS] = [f_new, b_new, r_new, l_new]
        if self.do_print: print(self)

    def Up(self):
        self.US.rotate(ccw=True)
        [f_new, b_new, r_new, l_new] = copy.deepcopy([self.FS, self.BS, self.RS, self.LS])
        for k in range(3):
            f_new[0,k] = self.LS[0,k]
            b_new[0,k] = self.RS[0,k]
            r_new[0,k] = self.FS[0,k]
            l_new[0,k] = self.BS[0,k]
        [self.FS, self.BS, self.RS, self.LS] = [f_new, b_new, r_new, l_new]
        if self.do_print: print(self)

    def U2(self):
        printstate = self.do_print
        self.do_print = False
        self.U(); self.U();
        self.do_print = printstate
        if self.do_print: print(self)

    def D(self):
        self.DS.rotate(ccw=False)
        [f_new, b_new, r_new, l_new] = copy.deepcopy([self.FS, self.BS, self.RS, self.LS])
        for k in range(3):
            f_new[2,k] = self.LS[2,k]
            b_new[2,k] = self.RS[2,k]
            r_new[2,k] = self.FS[2,k]
            l_new[2,k] = self.BS[2,k]
        [self.FS, self.BS, self.RS, self.LS] = [f_new, b_new, r_new, l_new]
        if self.do_print: print(self)

    def Dp(self):
        self.DS.rotate(ccw=True)
        [f_new, b_new, r_new, l_new] = copy.deepcopy([self.FS, self.BS, self.RS, self.LS])
        for k in range(3):
            f_new[2,k] = self.RS[2,k]
            b_new[2,k] = self.LS[2,k]
            r_new[2,k] = self.BS[2,k]
            l_new[2,k] = self.FS[2,k]
        [self.FS, self.BS, self.RS, self.LS] = [f_new, b_new, r_new, l_new]
        if self.do_print: print(self)

    def D2(self):
        printstate = self.do_print
        self.do_print = False
        self.D(); self.D();
        self.do_print = printstate
        if self.do_print: print(self)

    def M(self):
        [f_new, b_new, u_new, d_new] = copy.deepcopy([self.FS, self.BS, self.US, self.DS])
        for k in range(3):
            f_new[k,1] = self.US[k,1]
            b_new[k,1] = self.DS[2-k,1]
            u_new[k,1] = self.BS[2-k,1]
            d_new[k,1] = self.FS[k,1]
        [self.FS, self.BS, self.US, self.DS] = [f_new, b_new, u_new, d_new]
        if self.do_print: print(self)

    def Mp(self):
        [f_new, b_new, u_new, d_new] = copy.deepcopy([self.FS, self.BS, self.US, self.DS])
        for k in range(3):
            f_new[k,1] = self.DS[k,1]
            b_new[k,1] = self.US[2-k,1]
            u_new[k,1] = self.FS[k,1]
            d_new[k,1] = self.BS[2-k,1]
        [self.FS, self.BS, self.US, self.DS] = [f_new, b_new, u_new, d_new]
        if self.do_print: print(self)

    def M2(self):
        printstate = self.do_print
        self.do_print = False
        self.M(); self.M();
        self.do_print = printstate
        if self.do_print: print(self)

    def E(self):
        [f_new, b_new, r_new, l_new] = copy.deepcopy([self.FS, self.BS, self.RS, self.LS])
        for k in range(3):
            f_new[1,k] = self.LS[1,k]
            b_new[1,k] = self.RS[1,k]
            r_new[1,k] = self.FS[1,k]
            l_new[1,k] = self.BS[1,k]
        [self.FS, self.BS, self.RS, self.LS] = [f_new, b_new, r_new, l_new]
        if self.do_print: print(self)

    def Ep(self):
        [f_new, b_new, r_new, l_new] = copy.deepcopy([self.FS, self.BS, self.RS, self.LS])
        for k in range(3):
            f_new[1,k] = self.RS[1,k]
            b_new[1,k] = self.LS[1,k]
            r_new[1,k] = self.BS[1,k]
            l_new[1,k] = self.FS[1,k]
        [self.FS, self.BS, self.RS, self.LS] = [f_new, b_new, r_new, l_new]
        if self.do_print: print(self)

    def E2(self):
        printstate = self.do_print
        self.do_print = False
        self.E(); self.E();
        self.do_print = printstate
        if self.do_print: print(self)

    def S(self):
        [r_new, l_new, u_new, d_new] = copy.deepcopy([self.RS, self.LS, self.US, self.DS])
        for k in range(3):
            r_new[k,1] = self.US[1,k]
            l_new[k,1] = self.DS[1,k]
            u_new[1,k] = self.LS[2-k,1]
            d_new[1,k] = self.RS[2-k,1]
        [self.RS, self.LS, self.US, self.DS] = [r_new, l_new, u_new, d_new]
        if self.do_print: print(self)

    def Sp(self):
        [r_new, l_new, u_new, d_new] = copy.deepcopy([self.RS, self.LS, self.US, self.DS])
        for k in range(3):
            r_new[k,1] = self.DS[1,2-k]
            l_new[k,1] = self.US[1,2-k]
            u_new[1,k] = self.RS[k,1]
            d_new[1,k] = self.LS[k,1]
        [self.RS, self.LS, self.US, self.DS] = [r_new, l_new, u_new, d_new]
        if self.do_print: print(self)

    def S2(self):
        printstate = self.do_print
        self.do_print = False
        self.S(); self.S();
        self.do_print = printstate
        if self.do_print: print(self)

    def f(self):
        printstate = self.do_print
        self.do_print = False
        self.F(); self.S();
        self.do_print = printstate
        if self.do_print: print(self)

    def fp(self):
        printstate = self.do_print
        self.do_print = False
        self.Fp(); self.Sp();
        self.do_print = printstate
        if self.do_print: print(self)

    def f2(self):
        printstate = self.do_print
        self.do_print = False
        self.F2(); self.S2();
        self.do_print = printstate
        if self.do_print: print(self)

    def b(self):
        printstate = self.do_print
        self.do_print = False
        self.B(); self.Sp();
        self.do_print = printstate
        if self.do_print: print(self)

    def bp(self):
        printstate = self.do_print
        self.do_print = False
        self.Bp(); self.S();
        self.do_print = printstate
        if self.do_print: print(self)

    def b2(self):
        printstate = self.do_print
        self.do_print = False
        self.B2(); self.S2();
        self.do_print = printstate
        if self.do_print: print(self)

    def r(self):
        printstate = self.do_print
        self.do_print = False
        self.R(); self.Mp();
        self.do_print = printstate
        if self.do_print: print(self)

    def rp(self):
        printstate = self.do_print
        self.do_print = False
        self.Rp(); self.M();
        self.do_print = printstate
        if self.do_print: print(self)

    def r2(self):
        printstate = self.do_print
        self.do_print = False
        self.R2(); self.M2();
        self.do_print = printstate
        if self.do_print: print(self)

    def l(self):
        printstate = self.do_print
        self.do_print = False
        self.L(); self.M();
        self.do_print = printstate
        if self.do_print: print(self)

    def lp(self):
        printstate = self.do_print
        self.do_print = False
        self.Lp(); self.Mp();
        self.do_print = printstate
        if self.do_print: print(self)

    def l2(self):
        printstate = self.do_print
        self.do_print = False
        self.L2(); self.L2();
        self.do_print = printstate
        if self.do_print: print(self)

    def u(self):
        printstate = self.do_print
        self.do_print = False
        self.U(); self.Ep();
        self.do_print = printstate
        if self.do_print: print(self)

    def up(self):
        printstate = self.do_print
        self.do_print = False
        self.Up(); self.E();
        self.do_print = printstate
        if self.do_print: print(self)

    def u2(self):
        printstate = self.do_print
        self.do_print = False
        self.U2(); self.U2();
        self.do_print = printstate
        if self.do_print: print(self)

    def d(self):
        printstate = self.do_print
        self.do_print = False
        self.D(); self.E();
        self.do_print = printstate
        if self.do_print: print(self)

    def dp(self):
        printstate = self.do_print
        self.do_print = False
        self.Dp(); self.Ep();
        self.do_print = printstate
        if self.do_print: print(self)

    def d2(self):
        printstate = self.do_print
        self.do_print = False
        self.D2(); self.E2();
        self.do_print = printstate
        if self.do_print: print(self)

    def x(self):
        printstate = self.do_print
        self.do_print = False
        self.R(); self.Mp(); self.Lp();
        self.do_print = printstate
        if self.do_print: print(self)

    def xp(self):
        printstate = self.do_print
        self.do_print = False
        self.Rp(); self.M(); self.L();
        self.do_print = printstate
        if self.do_print: print(self)

    def x2(self):
        printstate = self.do_print
        self.do_print = False
        self.R2(); self.M2(); self.L2();
        self.do_print = printstate
        if self.do_print: print(self)

    def y(self):
        printstate = self.do_print
        self.do_print = False
        self.U(); self.Ep(); self.Dp();
        self.do_print = printstate
        if self.do_print: print(self)

    def yp(self):
        printstate = self.do_print
        self.do_print = False
        self.Up(); self.E(); self.D();
        self.do_print = printstate
        if self.do_print: print(self)

    def y2(self):
        printstate = self.do_print
        self.do_print = False
        self.U2(); self.E2(); self.D2();
        self.do_print = printstate
        if self.do_print: print(self)

    def z(self):
        printstate = self.do_print
        self.do_print = False
        self.F(); self.S(); self.Bp();
        self.do_print = printstate
        if self.do_print: print(self)

    def zp(self):
        printstate = self.do_print
        self.do_print = False
        self.Fp(); self.Sp(); self.B();
        self.do_print = printstate
        if self.do_print: print(self)

    def z2(self):
        printstate = self.do_print
        self.do_print = False
        self.F2(); self.S2(); self.B2();
        self.do_print = printstate
        if self.do_print: print(self)

    def move(self, movestr):
        bad_move_err = "Invalid move supplied"
        move_map = {
                "F": self.F, "F'": self.Fp, "F2": self.F2,
                "B": self.B, "B'": self.Bp, "B2": self.B2,
                "R": self.R, "R'": self.Rp, "R2": self.R2,
                "L": self.L, "L'": self.Lp, "L2": self.L2,
                "U": self.U, "U'": self.Up, "U2": self.U2,
                "D": self.D, "D'": self.Dp, "D2": self.D2,
                "f": self.f, "f'": self.fp, "f2": self.f2,
                "b": self.b, "b'": self.bp, "b2": self.b2,
                "r": self.r, "r'": self.rp, "r2": self.r2,
                "l": self.l, "l'": self.lp, "l2": self.l2,
                "u": self.u, "u'": self.up, "u2": self.u2,
                "d": self.d, "d'": self.dp, "d2": self.d2,
                "M": self.M, "M'": self.Mp, "M2": self.M2,
                "E": self.E, "E'": self.Ep, "E2": self.E2,
                "S": self.S, "S'": self.Sp, "S2": self.S2,
                "x": self.x, "x'": self.xp, "x2": self.x2,
                "y": self.y, "y'": self.yp, "y2": self.y2,
                "z": self.z, "z'": self.zp, "z2": self.z2
            }
        pattern = re.compile("[FBRLUDfbrludMESxyz][2']?")
        movelist = movestr.split(' ')
        methodlist = list()
        for move in movelist:
            match = pattern.match(move)
            if move == '':
                continue
            elif match:
                methodlist.append(move_map[match.group(0)])
            else:
                raise Exception(bad_move_err)
        printstate = self.do_print
        self.do_print = False
        for method in methodlist:
            method()
        self.do_print = printstate
        if self.do_print: print(self)

    def randomize(self, n=40):
        moves = random_moves(n)
        print(moves)
        self.move(moves)

    def __str__(self):
        topstr = concat_multiline_str((7*(14*' '+'\n'))[:-1], str(self.US)) + '\n'
        middlestr = concat_multiline_strs(str(self.LS), (7*" \n")[:-1], str(self.FS), (7*" \n")[:-1], str(self.RS), (7*" \n")[:-1], str(self.BS)) + '\n'
        bottomstr = concat_multiline_str((7*(14*' '+'\n'))[:-1], str(self.DS))
        return topstr + middlestr + bottomstr

def random_moves(n=40):
    moves = [
        ["F", "R", "U", "D" ,"L", "B", ],
        ["F'","R'","U'","D'","L'","B'",],
        ["F2","R2","U2","D2","L2","B2",]
    ]
    outstr = ""
    i = 0
    k0 = -1
    k1 = -1
    k = -1
    while i < n:
        movekind = random.choice(moves[:((n-i)>1)+2])
        while k == k1 or (k == k0 and k + k1 == 5):
            k = random.randint(0, 5)
        k0 = k1
        k1 = k
        move = movekind[k]
        i += 2 if '2' in move else 1
        #i += 1
        outstr = outstr + move + " "
    return outstr[:-1]

sune = "R U R' U R U2 R' U"
lsune = "L' U' L U' L' U2 L U'"
Jperm = "R U R' F' R U R' U' R' F R2 U' R' U'"
lJperm = "L' U' L F L' U' L U L F' L2 U L U"
