fun! promptline#themes#cosmos#get()
  return {
        \'a'    : [234, 253],
        \'b'    : [253, 237],
        \'c'    : [253, 235],
        \'x'    : [234, 27],
        \'y'    : [234, 214],
        \'z'    : [234, 63],
        \'warn' : [253, 124]}
endfun
