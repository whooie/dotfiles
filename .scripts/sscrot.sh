#!/bin/bash

filename=$(scrot -s '%Y-%m-%d-%H%M%S.png' -e 'echo $f')
read -ra dims <<<"$(convert $filename -format '%w %h' info:)"
convert "$(pwd)/$filename" -crop $[${dims[0]} - 1]x$[${dims[1]} - 1]+1+1 "$(pwd)/$filename"
mv "$(pwd)/$filename" "$HOME/Screenshots"
