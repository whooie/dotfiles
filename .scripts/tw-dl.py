#!/usr/bin/python

import getopt
import sys
import subprocess as sp
import os
import os.path as path
import re

def curl_write(url: str, fname: str) -> None:
    X = sp.run(["curl", "-s", url], stdout=sp.PIPE)
    if X.returncode:
        raise Exception(f"curl '{url}' > '{fname}'")
    outfile = open(fname, 'wb')
    outfile.write(X.stdout)
    outfile.close()
    return None

def main(argv):
    shortopts = ""
    longopts = [
    ]
    try:
        opts, args = getopt.gnu_getopt(argv[1:], shortopts, longopts)
        for opt, optarg in opts:
            pass
    except Exception as ERR:
        raise ERR

    PWD = os.getcwd()
    arg_pat = re.compile(r'^([0-9a-zA-Z_\-]+)\:(.+)')
    url_pat = re.compile(r'https\:\/\/pbs\.twimg\.com\/media\/([0-9a-zA-Z_\-]+)\?format=([a-zA-Z]+)(\&[0-9a-zA-Z=]*)*')

    for arg in args:
        arg_match = arg_pat.match(arg)
        if arg_match is None:
            raise Exception(f"invalid arg: {arg}")
        if arg_match.group(1) not in ["http", "https"]:
            T = path.join(PWD, arg_match.group(1))
            if not path.isdir(T):
                print(f":: mkdir {T}")
                os.mkdir(T)
            arg_items = arg_match.group(2).split(",")
        else:
            T = PWD
            arg_items = arg.split(",")

        for arg_item in arg_items:
            url_match = url_pat.match(arg_item)
            if url_match is None:
                raise Exception(f"invalid arg item: {arg_item}")
            t = path.join(T, url_match.group(1)+"."+url_match.group(2))
            print(f"Download -> {t}")
            curl_write(arg_item.split("&")[0]+"&name=4096x4096", t)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
