#!/bin/bash

print-help(){
    echo -e "Usage: \e[1mmounthfs\e[0m \e[4mdevice\e[0m \e[4mmountpoint\e[0m"
    echo -e "       \e[1mmounthfs\e[0m [ -h ]"
}

case $1 in
    "")
        print-help
        exit 1
        ;;
    -h)
        print-help
        exit 0
        ;;
esac

sudo mount -f hfsplus -o force,rw $1 $2
