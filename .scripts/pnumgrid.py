#!/usr/bin/python

import random as r
import math as m
import getopt
import sys; sys.dont_write_bytecode = True

helptext = """
Displays an array of random numbers following a given format.

Usage: pnumgrid.py [ options... ]
Options:
    -s, --spacing <str>
        Set the spacing between adjacent columns in number of characters. This
        space includes plus and minus signs, if present. The default value is 5.
    -w, --whole-digits <int>
        Set the number whole-number digits that each number should have. The
        default value is 5.
    -d, --decimal-digits <int>
        Set the number of digits to display after the decimal. The default value
        is 5.
    -m, --rows <int>
        Set the number of rows of numbers to display. The default value is 25.
    -n, --columns <int>
        Set the number of columns of numbers to display. The default value is 5.
    -P, --positive-only
        Display only positive numbers. Takes precedence over `--negative-only`.
    -N, --negative-only
        Display only negative numbers.
    -p, --plus-sign
        Display a plus sign before positive numbers.
    -h, --help
        Print this text.
"""

def generate_number(whole_digits, decimal_digits, p, n):
    """
    Generate a number which fulfills the user-specified conditions.
    """
    if p:
        a = ''
    elif n:
        a = '-'
    else:
        a = r.choice(['', '-'])

    numstr = "." if decimal_digits > 0 else ''
    i = 0
    while i < whole_digits:
        n = str(int(10*r.random()))
        if i == whole_digits-1 and n == '0':
            continue
        numstr = n+numstr
        i = i+1
    for i in range(decimal_digits):
        numstr = numstr + str(int(10*r.random()))

    return float(a+numstr)

def main(argv):
    shortopts = 's:w:d:m:n:PNph'
    longopts = [
            "spacing=",
            "whole-digits=",
            "decimal-digits=",
            "rows=",
            "columns=",
            "positive-only",
            "negative-only"
            "plus-sign",
            "help"
        ]
    try:
        opts, args = getopt.gnu_getopt(argv[1:], shortopts, longopts)
    except getopt.GetoptError as e:
        print(f"getopt: {e}")
        print(helptext)
        return 1

    spacing = 5
    whole_digits = 5
    decimal_digits = 5
    rows = 25
    cols = 5
    pos_only = False
    neg_only = False
    plus = ''
    try:
        for opt, optarg in opts:
            if opt in {"-s", "--spacing"}:
                spacing = int(optarg)
            elif opt in {"-w", "--whole-digits"}:
                whole_digits = int(optarg)
            elif opt in {"-d", "--decimal-digits"}:
                decimal_digits = int(optarg)
            elif opt in {"-m", "--rows"}:
                rows = int(optarg)
            elif opt in {"-n", "--columns"}:
                cols = int(optarg)
            elif opt in {"-P", "--positive-only"}:
                pos_only = True
            elif opt in {"-N", "--negative-only"}:
                neg_only = True
            elif opt in {"-p", "--plus-sign"}:
                plus = '+'
            elif opt in {"-h", "--help"}:
                print(helptext)
                sys.exit(0)
    except Exception as e:
        print(f"parse opts,args: {e}")
        print(helptext)
        return 1

    format_str = f"{plus}{spacing+whole_digits+decimal_digits+1}.{decimal_digits}f"
    for i in range(rows):
        for j in range(cols):
            num = generate_number(whole_digits, decimal_digits, pos_only, neg_only)
            print(("{:"+format_str+"}").format(num), end='')
        print('')

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))

