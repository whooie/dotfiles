#!/usr/bin/python

import getopt
import subprocess
import sys

try:
    opts, args = getopt.gnu_getopt(sys.argv[1:], "", [])
except Exception as ERR:
    raise ERR
url_base = "https://dschep.github.io/imgur-album-downloader/#/"
code = args[0].split("/")[-1]
subprocess.run(["qutebrowser", ":open "+url_base+code])

