discord \
    --use-gl=Desktop
    --ignore-gpu-blocklist \
    --disable-features=UseOzonePlatform \
    --enable-features=VaapiVideoDecoder,VaapiVideoEncoder \
    --enable-gpu-rasterization \
    --enable-zero-copy \
    --no-sandbox
