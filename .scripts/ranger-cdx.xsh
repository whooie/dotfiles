#!/bin/xonsh

if len($ARGS) == 1:
    args = $(pwd).strip()
else:
    args = ""
    for arg in $ARGS:
        args = args + " " + arg

tempfile = $(mktemp -t tmp.XXXXXX).strip()
ranger --choosedir @(tempfile) @(args)
cd @$(cat @(tempfile))
rm -f @(tempfile)
