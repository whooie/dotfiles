module phys {
    export def pi [] { 3.141592653589793 }

    # planck constant [kg m^2 s^-1]
    export def h [] { 6.626070040e-34 }         # +/- 0 (exact)

    # reduced planck constant [kg m^2 s^-1]
    export def hbar [] { (h) / 2.0 / (pi) }     # +/- 0 (exact)

    # speed of light in vacuum [m s^-1]
    export def c [] { 2.99792458e+8 }           # +/- 0 (exact)

    # Avogadro's number
    export def NA [] { 6.022140857e+23 }        # +/- 0 (exact)

    # Boltzmann's constant [J K^-1]
    export def kB [] { 1.38064852e-23 }         # +/- 0 (exact)

    # electric permittivity in vacuum [F m^-1]
    export def e0 [] { 8.8541878128e-12 }       # +/- 0.0000000013e-12

    # magnetic permeability in vacuum [N A^-2]
    export def u0 [] { 1.25663706212e-6 }       # +/- 0.00000000019e-6

    # Newtonian gravitational constant [m^3 kg^-1 s^-2]
    export def G [] { 6.67430e-11 }             # +/- 0.00015e-11

    # gravitational acceleration near Earth's surface [m s^-2]
    export def g [] { 9.80665 }                 # +/- 0 (exact)

    # elementary charge [C]
    export def e [] { 1.602176634e-19 }         # +/- 0 (exact)

    # electron mass [kg]
    export def me [] { 9.1093837015e-31 }       # +/- 0.0000000028e-31

    # proton mass [kg]
    export def mp [] { 1.67262192369e-27 }      # +/- 0.00000000051e-27

    # neutron mass [kg]
    export def mn [] { 1.67492749804e-27 }      # +/- 0.00000000095e-27

    # unified atomic mass unit [kg]
    export def mu [] { 1.66053906660e-27 }      # +/- 0.0000000005e-27

    # Rydberg constant [m^-1]
    export def Rinf [] { 10973731.568160 }      # +/- 0.000021

    # fine structure constant
    export def alpha [] { 7.2973525693e-3 }     # +/- 0.0000000011e-3

    # molar gas constant
    export def R [] { 8.314462618 }             # +/- 0 (exact)

    # Stefan-Boltzmann constant
    export def SB [] {                          # +/- idk
        ((pi) ** 2 * (kB) ** 4) / (60.0 * (hbar) ** 3 * (c) ** 2)
    }

    # Bohr radius [m]
    export def a0 [] { 5.29177210903e-11 }      # +/- 0.00000000080e-11

    # Bohr magneton [J T^-1]
    export def uB [] { 9.2740100783e-24 }       # +/- 0.0000000028e-24

    # nuclear magneton [J T^-1]
    export def uN [] { 5.050783699e-27 }        # +/- 0.000000031e-27

    # Hartree energy [J] = 2*Rinf*h*c
    export def Eh [] { 4.3597447222071e-18 }    # +/- 0.0000000000085e-18

    # unified atomic mass unit [kg] = 1/NA/1000
    export def amu [] { 1.66053906660e-27 }     # +/- 0.00000000050e-27

    # # Super command for helpful quantum-related functions
    # export def q [] { }
    #
    # # Compute the coefficient for an angular momentum ladder raising operator
    # export def "q raise" [J: number, m: number] {
    #     if ($m >= (0.0 - $J)) and ($m < $J) {
    #         (($J - $m) * ($J + $m + 1)) | math sqrt
    #     } else {
    #         0.0
    #     }
    # }
    #
    # # Compute the coefficient for an angular momentum ladder lowering operator
    # export def "q lower" [J: number, m: number] {
    #     if ($m > (0.0 -  $J)) and ($m <= $J) {
    #         (($J + $m) * ($J - $m + 1)) | math sqrt
    #     } else {
    #         0.0
    #     }
    # }
}
