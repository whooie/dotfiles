#!/bin/bash

case $1 in
	-f)
		curl -s v2.wttr.in/$2
		;;
	*)
		curl -s v2.wttr.in/$1 | tail -n 4
		;;
esac
echo ""
