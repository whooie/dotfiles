#!/bin/bash

files=("*- Arienai*" "*- Radio*" "*.png" "*.jpg")
filetest(){ # 1:match 2:patterns
    local match=$1
    shift
    local -a patterns=("$@")
    for pattern in "${patterns[@]}"; do
        if [[ "$match" == $pattern ]]; then
            return 1
        fi
    done
    return 0
}

if filetest "boop" "${files[@]}"; then echo "hello"; else echo "goodbye"; fi
if filetest "boop.png" "${files[@]}"; then echo "hello"; else echo "goodbye"; fi
if filetest "01 - Arienai Makuaigeki" "${files[@]}"; then echo "hello"; else echo "goodbye"; fi
