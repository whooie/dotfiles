" Vim syntax file
" Language:         Document outline planning
" First author:     Will Huie

if exists('b:current_syntax')
    finish
endif
let s:keepcpo= &cpo
set cpo&vim
scriptencoding utf-8

syn match outlineLineRange "\v\# [0-9\-]+ \#"
syn region outlineCommentText start="\v\/\*" end="\v\*\/" contains=outlineLineRange
syn match outlineQuickComment "\v\/\/.*" contains=outlineLineRange
syn match outlineTitle "\v^\=.*$"
syn match outlineSectionNumber "\v^[ \t]*([0-9.]+|Appendix [a-zA-Z]\:) "
syn match outlineSectionTitle "\v^[ \t]*([0-9.]+|Appendix [a-zA-Z]\:) [A-Z].*$" contains=outlineSectionNumber
syn match outlineBullet "\v^[ \t]*(\-|\*)"
syn match outlineCitation "\v\[.*\]"
syn match outlineInsert "\v^[ \t]*\(\(.*\)\)[ \t]*$"

hi outlineLineRange cterm=NONE ctermfg=Blue ctermbg=NONE
hi outlineCommentText cterm=NONE ctermfg=Yellow ctermbg=NONE
hi outlineQuickComment cterm=NONE ctermfg=Yellow ctermbg=NONE
hi outlineTitle cterm=Bold ctermfg=White ctermbg=NONE
hi outlineSectionNumber cterm=Bold ctermfg=Cyan ctermbg=NONE
hi outlineSectionTitle cterm=Bold ctermfg=White ctermbg=NONE
hi outlineBullet cterm=NONE ctermfg=Red ctermbg=NONE
hi outlineCitation cterm=NONE ctermfg=Cyan ctermbg=NONE
hi outlineInsert cterm=NONE ctermfg=Gray ctermbg=NONE
