#!/bin/bash

mode="undefined"
capture_type="undefined"
args="undefined"
input="undefined"
input_type="undefined"
is_help="false"
output="undefined"

print-help() {
    echo -e "Frontend for arecord and ffmpeg."
    echo -e "Usage: \e[1mrecord\e[0m \e[4mcapture mode\e[0m \e[4minput source\e[0m \e[4mfilename\e[0m"
    echo -e "       \e[1mrecord\e[0m [ -h ]"
    echo -e "\e[1mArguments\e[0m (mode, source, and filename must all be defined):"
    echo -e "  \e[4mcapture mode\e[0m            Defines what kind of capture the script will be doing."
    echo -e "                          Acceptable modes:"
    echo -e "                            a, audio    : record audio"
    echo -e "                            v, video    : record video"
    echo -e "  \e[4minput source\e[0m            Defines where the script will take its input from."
    echo -e "                          Acceptable audio sources:"
    echo -e "                            l, loopback : record internal system audio (must have the proper .asoundrc configuration)"
    echo -e "                            u, usb      : record audio from a usb input source"
    echo -e "                            mm, mic     : record audio from the computer's internal microphone"
    echo -e "                          Acceptable video sources:"
    echo -e "                            1, output1  : record video from the leftmost screen (1920x1080)"
    echo -e "                            2, output2  : record video from the rightmost screen (1920x1080)"
    echo -e "                            a, all      : record video from all screens (3840x1080)"
    echo -e "                            w, webcam   : record video taken from the computer's webcam (1280x720)"
    echo -e "  \e[4mfilename\e[0m               Defines the name and filetype of the output (must include file extension)"
    echo -e "  -h,--help                         Displays this text\n"
}   

case $1 in
    -h|--help)
        print-help
        exit 0
        ;;
    "")
        print-help
        exit 1
        ;;
    a|audio)
        mode="arecord"
        capture_type="audio"
        ;;
    v|video)
        mode="ffmpeg"
        capture_type="video"
        ;;
    *)
        echo "Invalid capture mode"
        print-help
        exit 1
        ;;
esac
case $2 in
    l|loopback)
        args="-D hw:0,1 -r 48000 -c 2 -f S32_LE"
        input="loopback"
        input_type="audio"
        ;;
    u|usb)
        args="-D hw:2,0 -r 48000 -c 1 -f S16_LE"
        input="usb"
        input_type="audio"
        ;;
    m|mic)
        args="-D hw:1,0 -r 48000 -c 2 -f S32_LE"
        input="mic"
        input_type="audio"
        ;;
    1|output1)
        args="-video_size 1920x1080 -framerate 30 -f x11grab -i :0.0+0,0"
        input="screen 1"
        input_type="video"
        ;;
    2|output2)
        args="-video_size 1920x1080 -framerate 30 -f x11grab -i :0.0+1920,0"
        input="screen 2"
        input_type="video"
        ;;
    a|all)
        args="-video_size 3840x1080 -framerate 30 -f x11grab -i :0.0+0,0"
        input="all screens"
        input_type="video"
        ;;
    w|webcam)
        args="-f v4l2 -s 1280x720 -framerate 30 -i /dev/video0"
        input="webcam"
        input_type="video"
        ;;
    *)
        echo "Invalid input source"
        exit 1
        ;;
esac

output="$3"

#case $1 in
#    -h|--help|"")
#        is_help="true"
#        ;;
#    *)
#        while getopts "m:i:f:" opt; do
#            case $opt in
#                f)
#                    output="$OPTARG"
#                    ;;
#                m)
#                    case $OPTARG in
#                        a|audio)
#                            mode="arecord"
#                            capture_type="audio"
#                            ;;
#                        v|video)
#                            mode="ffmpeg"
#                            capture_type="video"
#                            ;;
#                        *)
#                            echo "Invalid argument $OPTARG for option -$opt"
#                    esac
#                    ;;
#                i)
#                    case $OPTARG in
#                        l|loopback)
#                            args="-D hw:0,1 -r 48000 -c 2 -f S32_LE"
#                            input="loopback"
#                            input_type="audio"
#                            ;;
#                        u|usb)
#                            args="-D hw:2,0 -r 48000 -c 1 -f S16_LE"
#                            input="usb"
#                            input_type="audio"
#                            ;;
#                        m|mic)
#                            args="-D hw:1,0 -r 48000 -c 2 -f S32_LE"
#                            input="mic"
#                            input_type="audio"
#                            ;;
#                        1|output1)
#                            args="-video_size 1920x1080 -framerate 60 -f x11grab -i :0.0+0,0"
#                            input="screen 1"
#                            input_type="video"
#                            ;;
#                        2|output2)
#                            args="-video_size 1920x1080 -framerate 60 -f x11grab -i :0.0+1920,0"
#                            input="screen 2"
#                            input_type="video"
#                            ;;
#                        a|all)
#                            args="-video_size 3840x1080 -framerate 60 -f x11grab -i :0.0+0,0"
#                            input="all screens"
#                            input_type="video"
#                            ;;
#                        w|webcam)
#                            args="-f v4l2 -s 1280x720 -framerate 60 -i /dev/video0"
#                            input="webcam"
#                            input_type="video"
#                            ;;
#                        *)
#                            echo "Invalid argument $OPTARG for option -$opt"
#                            ;;
#                    esac
#                    ;;
#            esac
#        done
#        ;;
#esac
#
#if [[ $is_help == "true" ]]; then
#    echo -e "\e[1mOptions\e[0m (mode, source, and filename must all be defined):"
#    echo -e "  -m(ode) <mode>          Defines what kind of capture the script will be doing."
#    echo -e "                          Acceptable modes:"
#    echo -e "                            a(udio)    : record audio"
#    echo -e "                            v(ideo)    : record video"
#    echo -e "  -i(nput) <source>       Defines where the script will take its input from."
#    echo -e "                          Acceptable audio sources:"
#    echo -e "                            l(oopback) : record internal system audio (must have the proper .asoundrc configuration)"
#    echo -e "                            u(sb)      : record audio from a usb input source"
#    echo -e "                            m(ic)      : record audio from the computer's internal microphone"
#    echo -e "                          Acceptable video sources:"
#    echo -e "                            (output)1  : record the video feed to the leftmost screen (1920x1080)"
#    echo -e "                            (output)2  : record the video feed to the rightmost screen (1920x1080)"
#    echo -e "                            a(ll)      : record the video feed to all screens (3840x1080)"
#    echo -e "                            w(ebcam)   : record the video taken from the computer's webcam (1280x720)"
#    echo -e "  -f(ilename) <file.ext>  Defines the name and filetype of the output (must include file extension)"
#    echo -e "  -h,--help                         Displays this text"
#    echo -e "Usage: \e[1mrecord.sh\e[0m -m \e[4mmode\e[0m -i \e[4msource\e[0m -f \[4mfilename\e[0m"
#    echo -e "       \e[1mrecord.sh\e[0m [ -h ]"
#    echo -e "       \e[1mrecord.sh\e[0m"
#    exit 1
#fi

if [[ $mode == "undefined" ]] || [[ $args == "undefined" ]] || [[ $output == "undefined" ]]; then
    echo "Options -m, -i, and -f must all be defined"
    exit 1
fi

if [[ $capture_type != $input_type ]]; then
    echo "Capture mode and input type do not match"
    exit 1
fi

echo "===== $input ====="
$mode $args $output
