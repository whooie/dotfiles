#!/bin/bash

#blur_name=$(cat /home/whooie/.fehbg | tail -n 1 | awk -v FS="'" '{print $2}' | awk -v FS="." '{print $1" (blur)."$2}')
#trap "walls=(/home/whooie/.config/wallpaper_blur/*); feh --bg-fill --no-xinerama ${walls[1]}" KILL

while [[ $(cat /home/whooie/.control/blurbg/blur_control) != "kill" ]]; do
    if [[ $(cat /home/whooie/.control/blurbg/blur_control) == "1" ]]; then
        if [[ $(xtitle) != "" ]]; then
            if [[ -e /home/whooie/.config/wallpaper_blur/blur.png ]]; then
                feh --bg-fill --no-xinerama /home/whooie/.control/blurbg/blur.png
            elif [[ -e /home/whooie/.config/wallpaper_blur/blur.jpg ]]; then
                feh --bg-fill --no-xinerama /home/whooie/.control/blurbg/blur.jpg
            else
                bash /home/whooie/.fehbg
            fi
        else
            if [[ -e /home/whooie/.config/wallpaper_blur/no_blur.png ]]; then
                feh --bg-fill --no-xinerama /home/whooie/.control/blurbg/no_blur.png
            elif [[ -e /home/whooie/.config/wallpaper_blur/no_blur.jpg ]]; then
                feh --bg-fill --no-xinerama /home/whooie/.control/blurbg/no_blur.jpg
            else
                bash /home/whooie/.fehbg
            fi
        fi
    else
        walls=(/home/whooie/.control/blurbg/*)
        feh --bg-fill --no-xinerama ${walls[1]}
    fi
    #sleep 1
done
walls=(/home/whooie/.control/blurbg/*)
feh --bg-fill --no-xinerama ${walls[2]}
echo 1 > /home/whooie/.control/blurbg/blur_control
