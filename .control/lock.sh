#!/bin/bash

xset -dpms
xset s off

case $1 in
    ""|-h|--help)
        echo -e "Usage: \e[1mlock.sh\e[0m -a|-A|-x|-X"
        echo -e "       \e[1mlock.sh\e[0m [ -h ]"
        exit 1
        ;;
esac

panel_x=30
panel_y=30
panel_w=256
panel_h=132
    
readarray workspace <<<"$(i3-msg -t get_workspaces \
    | jq -M '.[]
    | {f: .focused, out: .output, r: .rect}
    | select(.f == true)
    | .out, .r.x, .r.y, .r.width, .r.height'
)"
workspace_x=$[${workspace[1]} + 0]
workspace_y=$[${workspace[2]} + 0]
workspace_w=$[${workspace[3]} + 0]
workspace_h=$[${workspace[4]} + 0]

bar_h=$(i3-msg -t get_tree \
    | jq -M '.nodes[]
    | select(.name == '"${workspace[0]}"').nodes[]
    | select(.name == "bottomdock")
    | .rect.height'
)

xpos_corner=$[$workspace_x + $panel_x]
ypos_corner=$[$workspace_y + $workspace_h + $bar_h - $panel_h - $panel_y]
xpos_center=$[$workspace_x + ($workspace_w - $panel_w)/2]
ypos_center=$[$workspace_y + ($workspace_h + $bar_h - $panel_h)/2]

get_bg(){
    case $1 in
        wall)
            wall="$(tail -n 1 ${HOME}/.fehbg | sed "s/[^']*'//" | sed "s/'//")"
            # wall="$(cat ~/.fehbg | tail -n 1 | sed "s/feh //g" | sed "s/--bg-fill //g" | sed "s/--no-xinerama //g" | sed "s/--no-fehbg //g" | tr -d "'")"
            wall=${wall:0:-1}
            ;;
        scrot)
            scrot -z ~/screen.png
            wall=~/screen.png
            ;;
    esac
    case $2 in
        pixelate)
            convert "$wall" -scale 12.5% -sharpen 0.65x0.65 -scale 800% ~/screen.png
            ;;
        blur)
            if [[ $1 == "wall" ]]; then
                convert ~/.control/blurbg/blur.* ~/screen.png
            else
                convert "$wall" -blur 5x5 ~/screen.png
            fi
            ;;
        none)
            if [[ $1 == "wall" ]]; then
                cp "$wall" ~/screen.png
            fi
            ;;
    esac
}

compose_bg(){
    case $1 in
        corner)
            case $2 in
                black)
                    composite -compose atop -geometry 336x132+$xpos_corner+$ypos_corner ~/.config/lock/lockblack.png ~/screen.png ~/screen.png
                    ;;
                regular)
                    composite -compose atop -geometry 336x132+$xpos_corner+$ypos_corner ~/.config/lock/lockregular.png ~/screen.png ~/screen.png
                    ;;
            esac
            ;;
        center)
            composite -compose atop -geometry 256x132+$xpos_center+$ypos_center ~/.config/lock/lockicon.png ~/screen.png ~/screen.png
            ;;
        none)
            ;;
    esac
}

lock_bg(){
    case $1 in
        corner)
            case $2 in
                clock)
                    i3lock --radius 30 --indicator --ind-pos="$workspace_x+300:y+h-98" \
                        --color=00000088 \
                        --inside-color=15151800 --ring-color=f4f4f4f0 --line-color=e8e8e800 --keyhl-color=181818a0 \
                        --ringver-color=f4f4f4ff --insidever-color=30303000 \
                        --ringwrong-color=f4f4f4f0 --insidewrong-color=404040a8 \
                        --verif-text=" " --wrong-text=" " \
                        --verif-color=00000000 --wrong-color=00000000 \
                        --clock --time-pos="ix-150:iy" \
                        --time-color=f4f4f4f0 --time-size=40 --time-str="%H:%M:%S" \
                        --date-color=f4f4f4f0 --date-size=20 --date-str="%a %Y.%m.%d" \
                        --{time,date,layout,verif,wrong,greeter}-font=mononoki \
                        -i ~/screen.png
                    ;;
                noclock)
                    composite -compose atop -geometry 336x132+$xpos_corner+$ypos_corner ~/.config/lock/locktext.png ~/screen.png ~/screen.png
                    i3lock --radius 30 --indicator --ind-pos="$workspace_x+305:y+h-98" \
                        --color=00000088 \
                        --inside-color=15151800 --ring-color=f4f4f4f0 --line-color=e8e8e800 --keyhl-color=181818a0 \
                        --ringver-color=f4f4f4ff --insidever-color=30303000 \
                        --ringwrong-color=f4f4f4f0 --insidewrong-color=404040a8 \
                        --verif-text=" " --wrong-text=" " \
                        --verif-color=00000000 --wrong-color=00000000 \
                        --{time,date,layout,verif,wrong,greeter}-font=mononoki \
                        -i ~/screen.png
                    ;;
            esac
            ;;
        center)
            i3lock --radius 65 --indicator --ind-pos="$workspace_x+w/2:y+h/2" \
                --color=00000088 \
                --inside-color=1a1a1a00 --ring-color=f4f4f4ff --line-color=e8e8e800 --keyhl-color=181818a0 \
                --ringver-color=f4f4f4ff --insidever-color=30303008 \
                --ringwrong-color=f4f4f4f0 --insidewrong-color=404040a8 \
                --verif-text="" --wrong-text="" \
                --verif-color=00000000 --wrong-color=00000000 \
                --{time,date,layout,verif,wrong,greeter}-font=mononoki \
                -i ~/screen.png
            ;;
    esac
}

lock_i3(){
    echo -n "get_bg"
    time get_bg scrot pixelate
    echo -n "compose_bg"
    time compose_bg corner black
    echo -n "lock_bg"
    time lock_bg corner clock
    if [[ $1 == "-s" ]]; then
        systemctl suspend
    fi
    rm ~/screen.png
}

lock_slock(){
    if [[ $1 == "-s" ]]; then
        systemctl suspend | slock
    else
        slock
    fi
}

while getopts "aAgkxXh" opt; do
    case $opt in
        a)
            lock_i3
            ;;
        A)
            lock_i3 -s
            ;;
        x)
            lock_slock
            ;;
        X)
            lock_slock -s
            ;;
    esac
done


xset -dpms
xset s off

exit 0
