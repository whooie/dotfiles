#!/bin/bash

case $1 in
    ""|-h|--help)
        echo -e "Usage: \e[1mscan_wifi.sh\e[0m -p|--auto-parse"
        echo -e "       \e[1mscan_wifi.sh\e[0m -v|--verbose"
        echo -e "       \e[1mscan_wifi.sh\e[0m [ -h|--help ]"
        ;;
    -p|--auto-parse)
        sudo netctl stop-all
        sudo ip link set wlp4s0 up
        sudo iw dev wlp4s0 scan | grep -e "SSID" -e "WPA" -e "WEP"
        sudo ip link set wlp4s0 down
        ;;
    -v|--verbose)
        sudo netctl stop-all
        sudo ip link set wlp4s0 up
        sudo iw dev wlp4s0 scan
        sudo ip link set wlp4s0 down
        ;;
    *)
        echo "Invalid option. See 'scan_wifi.sh -h' for more info."
esac
