#!/bin/bash

minutes=0
seconds=0
case ${1:-s} in
    s)
        clear
        #echo -en "\r"
        tput cup 0 0
        echo -n "0:00" | toilet -tf ${2:-smbraille}
        while true; do
            seconds=$[seconds+1]
            if [[ $seconds -eq 60 ]]; then
                minutes=$[minutes+1]
                seconds="0"
            fi
            output="$minutes:"
            if [[ $seconds -lt 10 ]]; then
                output+="0$seconds"
            else
                output+="$seconds"
            fi
            sleep 0.985s
            #clear
            #echo -en "\r"
            tput cup 0 0
            echo -n "$output" | toilet -tf ${2:-smbraille}
        done
        ;;
    l)
        clear
        #echo -en "\r"
        tput cup 0 0
        echo -n "0 minutes, 0 seconds" | toilet -tf ${2:-smbraille}
        while true; do
            seconds=$[seconds+1]
            if [[ $seconds -eq 60 ]]; then
                minutes=$[minutes+1]
                seconds="0"
            fi
            if [[ $minutes -eq 1 ]]; then
                output="$minutes minute, "
            else
                output="$minutes minutes, "
            fi
            if [[ $seconds -eq 1 ]]; then
                output+="$seconds second  "
            else
                output+="$seconds seconds"
            fi
            sleep 0.985s
            #clear
            #echo -en "\r"
            tput cup 0 0
            echo -n "$output" | toilet -tf ${2:-smbraille}
        done
        ;;
    *)
        echo "Invalid option. If you're trying to specify a font, you have to first specify s or l."
        echo "  s :: ~m:ss"
        echo "  l :: ~m minute(s), ~s second(s)"
        exit 1
        ;;
esac
