# Nushell Environment Config File

def create_left_prompt [] {
    let R = (ansi reset)
    let pwd = $env.PWD
    let dir = if ($pwd == "/home/whooie") {
        "~"
    } else if ($pwd == "/") {
        "/"
    } else {
        [' ', ($pwd | path basename), ' '] | str join
    }
    [$'($R).:[', $dir, '] '] | str join
}

def create_starship_prompt [] {
    starship prompt --cmd-duration $env.CMD_DURATION_MS $'--status=($env.LAST_EXIT_CODE)'
}

# def create_right_prompt [] {
#     let datetime = [
#         (date now | format date '%Y.%m.%d')
#         (date now | format date '%H:%M:%S')
#     ]
#     let predir = ($env.PWD | split row '/' | last 3 | first 2 | str join '/')
#     [$predir] | append $datetime | str join '  '
# }

def create_right_prompt [] {
    let datetime = [
        (date now | format date '%Y.%m.%d')
        (date now | format date '%H:%M:%S')
    ]
    $datetime | str join '  '
}

# Use nushell functions to define your right and left prompt
$env.PROMPT_COMMAND = { || create_left_prompt }
$env.PROMPT_COMMAND_RIGHT = { || create_right_prompt }

# The prompt indicators are environmental variables that represent
# the state of the prompt
# $env.PROMPT_INDICATOR = { "〉" }
# $env.PROMPT_INDICATOR_VI_INSERT = { "〉" }
# $env.PROMPT_INDICATOR_VI_NORMAL = { "〉" }
$env.PROMPT_INDICATOR = { || $"(ansi reset)> " }
$env.PROMPT_INDICATOR_VI_INSERT = { || $"(ansi reset)> " }
$env.PROMPT_INDICATOR_VI_NORMAL = { || $"(ansi reset)| " }
$env.PROMPT_MULTILINE_INDICATOR = { || $"(ansi reset)(ansi white_dimmed):::(ansi reset) " }

alias dev = load-env {
    PROMPT_INDICATOR: { || $"(ansi reset)" },
    PROMPT_INDICATOR_VI_INSERT: { || $"(ansi reset)\r(ansi white_bold)>(ansi reset) " },
    PROMPT_INDICATOR_VI_NORMAL: { || $"(ansi reset)\r(ansi white_bold)|(ansi reset) " },
    PROMPT_MULTILINE_INDICATOR: { || $"(ansi reset)(ansi white_dimmed):::(ansi reset) " },
    PROMPT_COMMAND: { || create_starship_prompt },
}

alias undev = load-env {
    PROMPT_INDICATOR: { || $"(ansi reset)>" },
    PROMPT_INDICATOR_VI_INSERT: { || $"(ansi reset)> " },
    PROMPT_INDICATOR_VI_NORMAL: { || $"(ansi reset)| " },
    PROMPT_MULTILINE_INDICATOR: { || $"(ansi reset)(ansi white_dimmed):::(ansi reset) " },
    PROMPT_COMMAND: { || create_left_prompt },
}

# Specifies how environment variables are:
# - converted from a string to a value on Nushell startup (from_string)
# - converted from a value back to a string when running external commands (to_string)
# Note: The conversions happen *after* config.nu is loaded
$env.ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) }
    to_string: { |v| $v | path expand | str join (char esep) }
  }
  "Path": {
    from_string: { |s| $s | split row (char esep) }
    to_string: { |v| $v | path expand | str join (char esep) }
  }
}

# Directories to search for scripts when calling source or use
#
# By default, <nushell-config-dir>/scripts is added
$env.NU_LIB_DIRS = [
    ($nu.config-path | path dirname | path join 'scripts')
]

# Directories to search for plugin binaries when calling register
#
# By default, <nushell-config-dir>/plugins is added
$env.NU_PLUGIN_DIRS = [
    ($nu.config-path | path dirname | path join 'plugins')
]

# To add entries to PATH (on Windows you might use Path), you can use the following pattern:
# $env.PATH = ($env.PATH | split row (char esep) | prepend '/some/path')

$env.PATH = ($env.PATH
    | append [
        "/home/whooie/.nix-profile/bin"
        "/home/whooie/.local/bin"
        "/home/whooie/.pyenv/shims"
        "/home/whooie/.pyenv/bin"
        "/home/whooie/.cargo/bin"
        "/home/whooie/.opam/5.1.0/bin"
        "/home/whooie/.opam/5.1.0/lib"
        "/home/whooie/.nu-whooie"
    ]
)

$env.LS_COLORS = "rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=01;36:ow=01;36:st=37;44:ex=01;33:*.tar=00:*.tgz=00:*.arc=00:*.arj=00:*.taz=00:*.lha=00:*.lz4=00:*.lzh=00:*.lzma=00:*.tlz=00:*.txz=00:*.tzo=00:*.t7z=00:*.zip=00:*.z=00:*.Z=00:*.dz=00:*.gz=00:*.lrz=00:*.lz=00:*.lzo=00:*.xz=00:*.bz2=00:*.bz=00:*.tbz=00:*.tbz2=00:*.tz=00:*.deb=00:*.rpm=00:*.jar=00:*.war=00:*.ear=00:*.sar=00:*.rar=00:*.alz=00:*.ace=00:*.zoo=00:*.cpio=00:*.7z=00:*.rz=00:*.cab=00:*.jpg=00:*.jpeg=00:*.gif=00:*.bmp=00:*.pbm=00:*.pgm=00:*.ppm=00:*.tga=00:*.xbm=00:*.xpm=00:*.tif=00:*.tiff=00:*.png=00:*.svg=00:*.svgz=00:*.mng=00:*.pcx=00:*.mov=00:*.mpg=00:*.mpeg=00:*.m2v=00:*.mkv=00:*.webm=00:*.ogm=00:*.mp4=00:*.m4v=00:*.mp4v=00:*.vob=00:*.qt=00:*.nuv=00:*.wmv=00:*.asf=00:*.rm=00:*.rmvb=00:*.flc=00:*.avi=00:*.fli=00:*.flv=00:*.gl=00:*.dl=00:*.xcf=00:*.xwd=00:*.yuv=00:*.cgm=00:*.emf=00:*.ogv=00:*.ogx=00:*.aac=00:*.au=00:*.flac=00:*.m4a=00:*.mid=00:*.midi=00:*.mka=00:*.mp3=00:*.mpc=00:*.ogg=00:*.ra=00:*.wav=00:*.oga=00:*.opus=00:*.spx=00:*.xspf=00:"
$env.BROWSER = "qutebrowser"
$env.EDITOR = "vim"
$env.PYTHONSTARTUP = "/home/whooie/.pythonrc"
$env.TERM = "rxvt-unicode"
$env.BAT_THEME = "1337"
$env.BAT_STYLE = "header,header-filename,grid,numbers,changes"

$env.OPAM_SWITCH_PREFIX = '/home/whooie/.opam/5.1.0'
$env.CAML_LD_LIBRARY_PATH = '/home/whooie/.opam/5.1.0/lib/stublibs:/home/whooie/.opam/5.1.0/lib/ocaml/stublibs:/home/whooie/.opam/5.1.0/lib/ocaml'
$env.OCAML_TOPLEVEL_PATH = '/home/whooie/.opam/5.1.0/lib/toplevel'

$env.NIX_PROFILES = '/nix/var/nix/profiles/default /home/whooie/.nix-profile'
$env.XDG_DATA_DIRS = '/usr/local/share:/usr/share:/home/whooie/.nix-profile/share:/nix/var/nix/profiles/default/share'
$env.NIX_SSL_CERT_FILE = '/etc/ssl/certs/ca-certificates.crt'

$env.MANPATH = '/home/whooie/.nix-profile/share/man:/home/whooie/.opam/5.1.0/man'

def _ranger-cd [ dir: string = "." ] {
    let init_dir = $env.PWD
    let temp_file = (mktemp -t "ranger_cd.XXXXXXXXXX" | str join | str trim)
    run-external "ranger" "--choosedir" $"($temp_file)" "--" $"($dir)"
    let temp_file_lines = (open --raw $temp_file | lines)
    rm -f $temp_file
    if ($temp_file_lines | length) > 0 {
        $temp_file_lines | get 0 | str join
    } else {
        $init_dir
    }
}

alias ranger-cd = cd (_ranger-cd)

def _yazi-cd [ dir: string = "." ] {
    let init_dir = $env.PWD
    let temp_file = (mktemp -t "yazi_cd.XXXXXXXXXX" | str join | str trim)
    run-external "yazi" "--cwd-file" $"($temp_file)" "--" $"($dir)"
    let temp_file_lines = (open --raw $temp_file | lines)
    rm -f $temp_file
    if ($temp_file_lines | length) > 0 {
        $temp_file_lines | get 0 | str join
    } else {
        $init_dir
    }
}

alias yazi-cd = cd (_yazi-cd)

alias clearall = do { clear; run-external "printf" "'\\e[3J'" }

def CL [ action: closure ] {
    clearall
    do $action
}

def netctl-s [ profile: string ] {
    sudo netctl stop-all
    if $profile == "l" {
        sudo netctl start limax
    } else if $profile == "i" {
        sudo netctl start ilnet
    } else {
        sudo netctl start $profile
    }
}

def sshfs-f [ ...args: any ] {
    $args | sshfs -F /home/whooie/.ssh/config -o reconnect -o allow_other $in
}

def scp-f [ ...args: any ] {
    $args | scp -F /home/whooie/.ssh/config $in
}

def tmux-a [ ...args: any ] {
    $args | tmux -2 attach -t $in
}

def tmux-k [ ...args: any ] {
    $args | tmux kill-session -t $in
}

def mpv-sf [ ...args: any ] {
    $args | mpv-s -f "22" $in
}

def mpv-sync [
    --address (-a): string = "syncplay.pl:8999"
    --name (-n): string = "whooie"
    --server (-s): string = "srcdfa"
    --password (-p): string = ""
    --gui (-g)
    video: string = ""
] {
    if ($gui) {
        syncplay -a $"($address)" --player-path $"((which mpv).path.0)" --name $"($name)" -r $"($server)" --password $"($password)" $video
    } else {
        syncplay --no-gui -a $"($address)" --player-path $"((which mpv).path.0)" --name $"($name)" -r $"($server)" --password $"($password)" $video
    }
}

def rclone-coveylab [ ...args: any ] {
    $args | rclone --vfs-cache-mode full mount coveylab: --dir-cache-time 2m30s $in
}

def timer [
    --sound (-s): path = `/home/whooie/Music/covet/technicolor (2020)/01 good morning.flac`
    T: duration,
] {
    let secs = ($T / 1sec)
    let subsecs = ($secs mod 1)
    let wholesecs = ($secs - $subsecs)
    let times = if $subsecs > 0.0 {
        ($wholesecs)..0
        | each { |t| $"($t)sec" | into duration }
        | prepend $T
    } else {
        ($wholesecs)..0
        | each { |t| $"($t)sec" | into duration }
    }
    let waits = if $subsecs > 0.0 {
        ($wholesecs)..1
        | each { |_| 1sec }
        | prepend ($"($subsecs)sec" | into duration)
        | append 0sec
    } else {
        ($wholesecs)..1
        | each { |_| 1sec }
        | append 0sec
    }

    $times
    | zip $waits
    | each { |tw|
        clear
        figlet -c $"($tw.0)"
        sleep $tw.1
    }
    mpv $sound
}

def wls [ script: path ] {
    wolfram -script $script
}

def pf [ ishname: string ] {
    ps | where name =~ $ishname
}

def mmv [
    --verbose (-v)
    --interactive (-i)
    destination: path
] {
    let sources = $in
    let action = (
        if $verbose {
            if $interactive {
                { |x| mv -v -i $x $destination }
            } else {
                { |x| mv -v $x $destination }
            }
        } else {
            if $interactive {
                { |x| mv -i $x $destination; null }
            } else {
                { |x| mv $x $destination; null }
            }
        }
    )
    $sources | each $action | flatten
}

def ccp [
    --recursive (-r)
    --verbose (-v)
    --interactive (-i)
    --no-symlink (-n)
    destination: path
] {
    let sources = $in
    let action = (
        if $recursive {
            if $verbose {
                if $interactive {
                    if $no_symlink {
                        { |x| cp -r -v -i -n $x $destination }
                    } else {
                        { |x| cp -r -v -i $x $destination }
                    }
                } else {
                    if $no_symlink {
                        { |x| cp -r -v -n $x $destination }
                    } else {
                        { |x| cp -r -v $x $destination }
                    }
                }
            } else {
                if $interactive {
                    if $no_symlink {
                        { |x| cp -r -i -n $x $destination; null }
                    } else {
                        { |x| cp -r -i $x $destination; null }
                    }
                } else {
                    if $no_symlink {
                        { |x| cp -r -n $x $destination; null }
                    } else {
                        { |x| cp -r $x $destination; null }
                    }
                }
            }
        } else {
            if $verbose {
                if $interactive {
                    if $no_symlink {
                        { |x| cp -v -i -n $x $destination }
                    } else { 
                        { |x| cp -v -i $x $destination }
                    }
                } else {
                    if $no_symlink {
                        { |x| cp -v -n $x $destination }
                    } else {
                        { |x| cp -v $x $destination }
                    }
                }
            } else {
                if $interactive {
                    if $no_symlink {
                        { |x| cp -i -n $x $destination; null  }
                    } else {
                        { |x| cp -i $x $destination; null }
                    }
                } else {
                    if $no_symlink {
                        { |x| cp -n $x $destination; null }
                    } else {
                        { |x| cp $x $destination; null }
                    }
                }
            }
        }
    )
    $sources | each $action | flatten
}

def pyrmexplorer [] {
    cd /home/whooie/Stuff/pyrmexplorer
    python -m rmexplorer
}

def "cargo doc open" [
    --open-with (-o): string = "surf" # Use this to open the html file.
    --doc (-d): string = "" # Open this documentation. One of: {alloc, lang, cargo, core, edition, embedded, nomicon, macros, reference, example, rustc, rustdoc, std, test, unstable} or empty. Defaults to crate documentation if unspecified.
    --background (-b) # Fork to background in a Bash sub-shell.
] {
    let path = (
        match $doc {
            "alloc" => (rustup doc --path --alloc),
            "lang" => (rustup doc --path --book),
            "cargo" => (rustup doc --path --cargo),
            "core" => (rustup doc --path --core),
            "edition" => (rustup doc --path --edition-guide),
            "embedded" => (rustup doc --path --embedded-book),
            "nomicon" => (rustup doc --path --nomicon),
            "macros" => (rustup doc --path --proc_macro),
            "reference" => (rustup doc --path --reference),
            "example" => (rustup doc --path --rust-by-example),
            "rustc" => (rustup doc --path --rustc),
            "rustdoc" => (rustup doc --path --rustdoc),
            "std" => (rustup doc --path --std),
            "test" => (rustup doc --path --test),
            "unstable" => (rustup doc --path --unstable-book),
            "" => $"target/doc/(open Cargo.toml | get lib | get name)/index.html",
            _ => { error make { msg: "invalid value for option `--doc`" } },
        }
    )
    if $background {
        bash -c $"($open_with) '($path)' &"
    } else {
        run-external $open_with $"($path)"
    }
}

alias "cargo doc openq" = cargo doc open --open-with qutebrowser

def "cargo script" [
    --quiet (-q)
    --new (-n)
    file: path
] {
    if $new {
        [
            '//! ```cargo'
            '//! [package]'
            '//! edition = "2021"'
            '//!'
            '//! [dependencies]'
            '//! ```'
            ''
            'fn main() {'
            '    todo!()'
            '}'
        ] | str join "\n" | save $file
    } else {
        if $quiet {
            cargo +nightly -Zscript -q $file
        } else {
            cargo +nightly -Zscript $file
        }
    }
}

def "cargo doc katex" [
    --quiet (-q)
    --open
    --package (-p): string
    --verbose (-p)
    --workspace
    --exclude: string
    --all
    --color: string
    --frozen
    --no-deps
    --document-private-items
    --locked
    --jobs (-j): int
    --offline
    --config: string
    --keep-going
    --lib
    --unstable-flags (-Z): string
    --bin: string
    --bins
    --example: string
    --examples
    --release (-r)
    --profile: string
    --features (-F): string
    --all-features
    --no-default-features
    --target: string
    --target-dir: path
    --manifest-path: path
    --message-format: string
    --ignore-rust-version
    --unit-graph
    --timings: string
] {
    let options = (
        [
            (if $quiet { "--quiet" } else { "" })
            (if $open { "--open" } else { "" })
            (if $package != null { ["--package", $package] } else { [""] })
            (if $verbose { "--verbose" } else { "" })
            (if $workspace { "--workspace" } else { "" })
            (if $exclude != null { ["--exclude", $exclude] } else { [""] })
            (if $all { "--all" } else { "" })
            (if $color != null { ["--color", $color] } else { [""] })
            (if $frozen { "--frozen" } else { "" })
            (if $no_deps { "--no-deps" } else { "" })
            (if $document_private_items { "--document-private-items" } else { "" })
            (if $locked { "--locked" } else { "" })
            (if $jobs != null { ["--jobs", $"($jobs)"] } else { [""] })
            (if $offline { "--offline" } else { "" })
            (if $config != null { ["--config", $config] } else { [""] })
            (if $keep_going { "--keep-going" } else { "" })
            (if $lib { "--lib" } else { "" })
            (if $unstable_flags != null { ["-Z", $unstable_flags] } else { [""] })
            (if $bin != null { ["--bin", $bin] } else { [""] })
            (if $bins { "--bins" } else { "" })
            (if $example != null { "--bins" } else { "" })
            (if $release { "--release" } else { "" })
            (if $profile != null { ["--profile", $profile] } else { [""] })
            (if $features != null { ["--features", $features] } else { [""] })
            (if $all_features { "--all-features" } else { "" })
            (if $no_default_features { "--no-default-features" } else { "" })
            (if $target != null { ["--target", $target] } else { [""]})
            (if $target_dir != null { ["--target-dir", $target_dir] } else { [""] })
            (if $manifest_path != null { ["--manifest-path", $manifest_path] } else { [""] })
            (if $message_format != null { ["--message-format", $message_format] } else { [""] })
            (if $ignore_rust_version { "--ignore-rust-version" } else { "" })
            (if $unit_graph { "--unit-graph" } else { "" })
            (if $timings != null { ["--timings", $timings] } else { [""] })
        ]
        | where ($it != "" and $it != [""])
        | flatten
    )
    $options | cargo doc $in
    (
        with-env
            [
                RUSTDOCFLAGS
                "--html-in-header /home/whooie/Documents/templates/rust/rustdoc-katex.html"
            ]
            {
                $options | cargo doc --no-deps $in
            }
    )
}

def "cargo clipdoc" [
    --quiet (-q)
    --package (-p): string
    --verbose (-p)
    --workspace
    --exclude: string
    --all
    --color: string
    --frozen
    --no-deps
    --document-private-items
    --locked
    --jobs (-j): int
    --offline
    --config: string
    --keep-going
    --lib
    --unstable-flags (-Z): string
    --bin: string
    --bins
    --example: string
    --examples
    --release (-r)
    --profile: string
    --features (-F): string
    --all-features
    --no-default-features
    --target: string
    --target-dir: path
    --manifest-path: path
    --message-format: string
    --ignore-rust-version
    --unit-graph
    --timings: string
] {
    let options = (
        [
            (if $quiet { "--quiet" } else { "" })
            (if $package != null { ["--package", $package] } else { [""] })
            (if $verbose { "--verbose" } else { "" })
            (if $workspace { "--workspace" } else { "" })
            (if $exclude != null { ["--exclude", $exclude] } else { [""] })
            (if $all { "--all" } else { "" })
            (if $color != null { ["--color", $color] } else { [""] })
            (if $frozen { "--frozen" } else { "" })
            (if $no_deps { "--no-deps" } else { "" })
            (if $document_private_items { "--document-private-items" } else { "" })
            (if $locked { "--locked" } else { "" })
            (if $jobs != null { ["--jobs", $"($jobs)"] } else { [""] })
            (if $offline { "--offline" } else { "" })
            (if $config != null { ["--config", $config] } else { [""] })
            (if $keep_going { "--keep-going" } else { "" })
            (if $lib { "--lib" } else { "" })
            (if $unstable_flags != null { ["-Z", $unstable_flags] } else { [""] })
            (if $bin != null { ["--bin", $bin] } else { [""] })
            (if $bins { "--bins" } else { "" })
            (if $example != null { "--bins" } else { "" })
            (if $release { "--release" } else { "" })
            (if $profile != null { ["--profile", $profile] } else { [""] })
            (if $features != null { ["--features", $features] } else { [""] })
            (if $all_features { "--all-features" } else { "" })
            (if $no_default_features { "--no-default-features" } else { "" })
            (if $target != null { ["--target", $target] } else { [""]})
            (if $target_dir != null { ["--target-dir", $target_dir] } else { [""] })
            (if $manifest_path != null { ["--manifest-path", $manifest_path] } else { [""] })
            (if $message_format != null { ["--message-format", $message_format] } else { [""] })
            (if $ignore_rust_version { "--ignore-rust-version" } else { "" })
            (if $unit_graph { "--unit-graph" } else { "" })
            (if $timings != null { ["--timings", $timings] } else { [""] })
        ]
        | where ($it != "" and $it != [""])
        | flatten
    )
    $options | cargo clippy $in
    $options | cargo doc $in
}

def "dune doc" [
    --action-stderr-on-success: string
    --action-stdout-on-success: string
    --build-info
    --display-separate-messages
    --passive-watch-mode
    --watch (-w)
] {
  let options = (
    [
      (if $action_stderr_on_success != null {
        ["--action-stderr-on-success", $action_stderr_on_success]
      } else {
        [""]
      })
      (if $action_stdout_on_success != null {
        ["--action-stdout-on-success", $action_stdout_on_success]
      } else {
        [""]
      })
      (if $build_info { "--build-info" } else { "" })
      (if $display_separate_messages { "--display-separate-messages" } else { "" })
      (if $passive_watch_mode { "--passive-watch-mode" } else { "" })
      (if $watch { "--watch" } else { "" })
    ]
    | where ($it != "" and $it != [""])
    | flatten
  )
  $options | dune build $in @doc
}

def "dune doc open" [
    --open-with (-o): string = "surf" # Use this to open the html file.
    --doc (-d): string = "" # Open this documentation. One of: {std, doc manual} or empty. Defaults to crate documentation if unspecified.
    --background (-b) # Fork to background in a Bash sub-shell.
] {
    let path = (
        match $doc {
            "std" => ("https://v2.ocaml.org/api/index.html"),
            "doc" => ("https://v2.ocaml.org/manual/ocamldoc.html"),
            "manual" => ("https://v2.ocaml.org/manual/index.html"),
            "" => ("_build/default/_doc/_html/index.html"),
            _ => { error make { msg: "invalid value for option `--doc`" } },
        }
    )
    if $background {
        bash -c $"($open_with) '($path)' &"
    } else {
        run-external $open_with $"($path)"
    }
}

alias "dune doc openq" = dune doc open --open-with qutebrowser

def "dune buildoc" [
    --action-stderr-on-success: string
    --action-stdout-on-success: string
    --build-info
    --display-separate-messages
    --passive-watch-mode
    --watch (-w)
] {
  let options = (
    [
      (if $action_stderr_on_success != null {
        ["--action-stderr-on-success", $action_stderr_on_success]
      } else {
        [""]
      })
      (if $action_stdout_on_success != null {
        ["--action-stdout-on-success", $action_stdout_on_success]
      } else {
        [""]
      })
      (if $build_info { "--build-info" } else { "" })
      (if $display_separate_messages { "--display-separate-messages" } else { "" })
      (if $passive_watch_mode { "--passive-watch-mode" } else { "" })
      (if $watch { "--watch" } else { "" })
    ]
    | where ($it != "" and $it != [""])
    | flatten
  )
  $options | dune build $in
  $options | dune build $in @doc
}

# Pipeable fusermount command. Piped paths are processed before those passed as
# arguments.
def fumount [
    --quiet (-q) # Suppress fusermount output
    --lazy (-z) # Lazy unmount
    ...paths: path # Umount these paths
] {
    let pipe_in = $in
    let args_in = $paths
    let options = (
        [
            (if $quiet { "-q" } else { "" })
            (if $lazy { "-z" } else { "" })
        ]
        | where $it != ""
    )
    $pipe_in
    | append $args_in
    | each { |x| $options | append [$x] | fusermount -u $in }
    return null
}

def lsn [
    --all (-a)
    --short-names (-s)
    --full-paths (-f)
    --directory (-D)
    pattern: string=""
] {
    if $all {
        if $short_names {
            if $full_paths {
                if $directory {
                    if ($pattern != "") {
                        ls -a -s -f -D $pattern
                    } else {
                        ls -a -s -f -D
                    }
                } else {
                }
            } else {
                if $directory {
                    if ($pattern != "") {
                        ls -a -s -D $pattern
                    } else {
                        ls -a -s -D
                    }
                } else {
                    if ($pattern != "") {
                        ls -a -s $pattern
                    } else {
                        ls -a -s
                    }
                }
            }
        } else {
            if $full_paths {
                if $directory {
                    if ($pattern != "") {
                        ls -a -f -D $pattern
                    } else {
                        ls -a -f -D
                    }
                } else {
                    if ($pattern != "") {
                        ls -a -f $pattern
                    } else {
                        ls -a -f
                    }
                }
            } else {
                if $directory {
                    if ($pattern != "") {
                        ls -a -D $pattern
                    } else {
                        ls -a -D
                    }
                } else {
                    if ($pattern != "") {
                        ls -a $pattern
                    } else {
                        ls -a
                    }
                }
            }
        }
    } else {
        if $short_names {
            if $full_paths {
                if $directory {
                    if ($pattern != "") {
                        ls -s -f -D $pattern
                    } else {
                        ls -s -f -D
                    }
                } else {
                }
            } else {
                if $directory {
                    if ($pattern != "") {
                        ls -s -D $pattern
                    } else {
                        ls -s -D
                    }
                } else {
                    if ($pattern != "") {
                        ls -s $pattern
                    } else {
                        ls -s
                    }
                }
            }
        } else {
            if $full_paths {
                if $directory {
                    if ($pattern != "") {
                        ls -f -D $pattern
                    } else {
                        ls -f -D
                    }
                } else {
                    if ($pattern != "") {
                        ls -f $pattern
                    } else {
                        ls -f
                    }
                }
            } else {
                if $directory {
                    if ($pattern != "") {
                        ls -D $pattern
                    } else {
                        ls -D
                    }
                } else {
                    if ($pattern != "") {
                        ls $pattern
                    } else {
                        ls
                    }
                }
            }
        }
    }
    | get name
}

alias tty-clock = run-external "tty-clock" "-cs" "-C" "7" "-f" "'%a %Y.%m.%d'"

def lsblk [] {
    ^lsblk -o "NAME,SIZE,TYPE,STATE,FSTYPE,LABEL,MOUNTPOINT" -r
    | lines
    | skip 1
    | each { |line|
        $line
        | parse -r (
            ["name", "size", "type", "state", "fstype", "label", "mountpoint"]
            | each { |head| $"\(?P<($head)>.*\)" }
            | str join " "
        )
    }
    | flatten
}

# alias lsblk = (
#     ^lsblk -o "NAME,SIZE,TYPE,STATE,FSTYPE,LABEL,MOUNTPOINT" -r
#     | lines
#     | skip 1
#     | each { |line|
#         $line
#         | parse -r "(?P<name>.*) (?P<size>.*) (?P<type>.*) (?P<state>.*) (?P<fstype>.*) (?P<label>.*) (?P<mountpoint>.*)"
#     }
#     | flatten
# )
alias reswap = do { run-external "sudo" "swapoff" "/swapfile"; run-external "sudo" "swapon" "/swapfile" }

alias clone = bash -c "alacritty &"

# alias dev = source "/home/whooie/.config/nushell/env-dev.nu"
# alias dev = do { source "/home/whooie/.config/nushell/env-dev.nu" }
# alias undev = source "/home/whooie/.config/nushell/env.nu"
# alias undev = do { source "/home/whooie/.config/nushell/env.nu" }

alias "netctl list" = do { netctl list | lines | sort }

################################################################################

extern bibpy [
    --aux (-a): path
    --doc (-d): path
    --output (-o): path
    --follow-links (-l)
    --yaml (-y)
    --toml (-t)
    --json (-j)
    --ini (-i)
    --bibtex (-b)
    --export (-E)
    --export-yaml (-Y)
    --export-toml (-T)
    --export-json (-J)
    --export-ini (-I)
    --export-bibtex (-B)
    --formatting (-f)
    --quiet (-q)
    --verbose (-v)
    --help (-h)
]

extern lock [
    -a
    -A
    -x
    -X
    --help (-h)
]

extern logfella [
    -f: path
    -y
    -Y
    -m: string
    -h
]

extern mkgif [
    -i: path
    -o: path
    -a: string
    -t: string
    -f: int
    -d: string
    -s: string
    -S: string
    -D: string
    -b
    -P
    --help (-h)
]

extern mkwebm [
    -i: path
    -o: path
    -a: string
    -t: string
    -f: int
    -d: string
    -s: string
    -q: int
    -S: string
    --help (-h)
]

extern mkproject [
    --notes-format (-n): string
    --programming-languages (-p): string
    --notes-only (-N)
    --programming-only (-P)
    --copy-style-files (-s)
    --copy-lib-files (-l)
    --features (-f)
    --current-directory (-.)
    --gitfiles (-g)
    --init-git (-G)
    --help (-h)
    project_dir: string
]

extern mpv [
    --start: string
    --no-audio
    --no-video
    --fs
    --sub-file: path
    --playlist: path
    --list-options
    --loop: string
    --loop-file: string
    --loop-playlist: string
    --volume: number
    --help (-h)
    ...videos: string
]

extern mpv-s [
    -f
    -F
    --help (-h)
    ...videos: string
]

extern outmux [
    --help (-h)
    output: string
]

extern pacdown [
    -d: string
    -f: path
    -y: string
    --help (-h)
]

extern rcube [
    --interactive (-i)
    --no-preview (-P)
    --help (-h)
]

extern twitcher [
    -S
    -F: string
    -f: string
    -b
    -C
    --help (-h)
    channel: string
]

################################################################################

source "/home/whooie/.nu-whooie/phys.nu"
use phys
source "/home/whooie/.nu-whooie/math.nu"
source "/home/whooie/.nu-whooie/utils.nu"

