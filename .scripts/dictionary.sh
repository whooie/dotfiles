#!/bin/sh

word=$1
case $2 in
    "")
        dict -d gcide "$word"
        echo ""
        ;;
    pon)
        dict -d gcide "$word" | ponysay --pony owlowiscious --wrap 150 --balloon round
        echo ""
        ;;
esac
