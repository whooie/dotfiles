#!/usr/bin/python

import sys
import getopt
import yaml
import pyaml # type: ignore
import dmenu # type: ignore
import subprocess
config = type("Config", (), {})()
config.add_mode = False

linkfile = "/home/whooie/links"

class LinkTree:
    def __init__(self, data, root=None):
        self.root = root
        self.is_root = True if self.root == None else False
        self.data = data
        self.is_leaf = True if isinstance(data, str) else False
        if not self.is_leaf:
            for key in data.keys():
                self.data[key] = LinkTree(data[key], root=self)

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, val):
        self.data[key] = val

    def __delitem__(self, key):
        del self.data[key]

    def __iter__(self):
        return iter(self.data)

    def __reversed__(self):
        return reversed(self.data)

    def __contains__(self, item):
        return item in self.data

    def __len__(self):
        return len(self.data)

    def get_root(self):
        return self.root

    def get_keys(self):
        if not self.is_leaf:
            return list(self.data.keys())
        else:
            return None

    def get_data(self):
        return self.data

    def get_leaves(self):
        if self.is_leaf:
            leaves = [self]
        else:
            leaves = list(filter(lambda x: x.is_leaf, self.data.values()))
        return leaves

    def find_leaves(self):
        leaves = list()
        if self.is_leaf:
            leaves.append(self)
        else:
            for key in self.data.keys():
                leaves += self.data[key].find_leaves()
        return leaves

    def get_dict(self):
        res = dict()
        for key in self.data.keys():
            if self.data[key].is_leaf:
                res[key] = self.data[key].get_data()
            else:
                res[key] = self.data[key].get_dict()
        return res

def zeropad(n, l):
    S = str(n)
    N = len(S)
    return (l-N)*'0' + S

def select_link(linktree):
    N = len(linktree)
    L = len(str(N-1))
    leaves = linktree.get_leaves()
    all_leaves = linktree.find_leaves()
    text_back = ":: GO BACK"
    text_links = f":: OPEN VISIBLE ({len(leaves)})"
    text_everything = f":: OPEN EVERYTHING ({len(all_leaves)})"
    menu = (not linktree.is_root)*[text_back] + [text_links, text_everything]
    menu +=[
        zeropad(i, L)+" "+key+":  "+(linktree[key].get_data() if linktree[key].is_leaf else "[...]")
        for i, key in enumerate(linktree.get_keys())
    ]
    choice = dmenu.show(menu, command="dmenu-links")
    print(choice)
    if not choice:
        return None
    if choice == text_back:
        return select_link(linktree.get_root())
    elif choice == text_links:
        return list(map(lambda x: x.get_data(), leaves))
    elif choice == text_everything:
        return list(map(lambda x: x.get_data(), all_leaves))
    else:
        choice = choice.split(":")[0][L+1:]
        print(choice)
        if choice not in linktree:
            return None
        if linktree[choice].is_leaf:
            return [linktree[choice].get_data()]
        else:
            return select_link(linktree[choice])

def update_conservative(dict1, dict2):
    for key in dict2:
        if key not in dict1:
            dict1[key] = dict2[key]
        elif isinstance(dict1[key], dict) and isinstance(dict2[key], dict):
            update_conservative(dict1[key], dict2[key])
        else:
            if isinstance(dict1[key], dict):
                i = 0
                while "link"+str(i) in dict1[key]:
                    i += 1
                dict1[key]["link"+str(i)] = dict2[key]
            elif isinstance(dict2[key], dict):
                new_dict = dict()
                i = 0
                while "link"+str(i) in dict2:
                    i += 1
                new_dict.update(dict2[key])
                new_dict[":"+str(i)] = dict1[key]
                dict1[key] = new_dict
            else:
                new_dict = dict()
                new_dict["link0"] = dict1[key]
                new_dict["link1"] = dict2[key]
                dict1[key] = new_dict

def main(argv):
    shortopts = "a"
    longopts = [
            "add"
        ]
    try:
        opts, args = getopt.gnu_getopt(argv[1:], shortopts, longopts)
    except getopt.GetoptError as e:
        raise e
    for opt, optarg in opts:
        if opt in ["-a", "--add"]:
            config.add_mode = True

    infile = open(linkfile, 'r')
    linktree = yaml.safe_load(infile)
    infile.close()

    if config.add_mode:
        if len(args) < 2:
            return 1
        url = args[0]
        branch = {args[-1]: url}
        for key in reversed(args[1:-1]):
            branch = {key: branch}
        update_conservative(linktree, branch)
        
        outfile = open(linkfile, 'w')
        pyaml.dump(linktree, outfile, safe=True, indent=2, vspacing=[1, 1], sort_dicts=False)
        outfile.close()
        return 0

    link = select_link(LinkTree(linktree))
    if link and link != list():
        subprocess.run(["qutebrowser"] + link)
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
