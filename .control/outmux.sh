#!/bin/bash

#cat ~/.control/wallpaper | grep -e "^[^#\[]"

#single=$(cat /home/whooie/.control/wallpaper | grep "!single" | awk 'BEGIN{FS="="}{print $2}' | tr -d "'")
single="/home/whooie/Stuff/Images/Wallpapers/Animu/Hibike!/River Crane.png"
#single="/home/whooie/Stuff/Images/Wallpapers/Other/no_air_anasabdin_recrop.png"
#single="/home/whooie/Stuff/Images/Wallpapers/Other/fourier/single_wall_light_shadow.png"
single_extension=$(echo $single | awk -v FS="." '{print $2}')

#double=$(cat /home/whooie/.control/wallpaper | grep "!double" | awk 'BEGIN{FS="="}{print $2}' | tr -d "'")
#double="/home/whooie/Stuff/Images/Wallpapers/Other/Guilin Mountains (resize).png"
#double="/home/whooie/Stuff/Images/Wallpapers/Other/no_air_anasabdin_double.png"
#double="/home/whooie/Stuff/Images/Wallpapers/Other/fourier/double_wall.png"
#double="/home/whooie/Stuff/Images/Wallpapers/Other/Sunset Mountains Cropped.png"
double="/home/whooie/Stuff/Images/Wallpapers/Other/mist_mountains_double.png"
double_extension=$(echo $double | awk -v FS="." '{print $2}')

#triple=$(cat /home/whooie/.control/wallpaper | grep "!triple" | awk 'BEGIN{FS="="}{print $2}' | tr -d "'")
#triple="/home/whooie/Stuff/Images/Wallpapers/Space/Floating Earth (Triple).png"
triple="/home/whooie/Stuff/Images/Wallpapers/Other/river-crane_mist-mountains_triple.png"
#triple="/home/whooie/Stuff/Images/Wallpapers/Other/Pinery (Triple).png"
triple_extension=$(echo $triple | awk -v FS="." '{print $2}')

tile="/home/whooie/Stuff/Images/Wallpapers/blue_clouds/blue-clouds_tile_398_color-adjust.png"

is_help="false"

xrandr_checker="/home/whooie/.scripts/xrandr_parser.py"

reset_blur() {
    if [[ -e /home/whooie/.control/blurbg/blur.jpg ]]; then
        rm /home/whooie/.control/blurbg/*.jpg
    fi
    if [[ -e /home/whooie/.control/blurbg/blur.png ]]; then
        rm /home/whooie/.control/blurbg/*.png
    fi
}

blur_smooth() {
    convert "$1" -blur 0x5 "$2"
}

blur_pixel() {
    convert "$1" -scale 12.5% -scale 800% "$2"
}

blur() {
    blur_smooth "$1" "$2"
    #blur_pixel "$1" "$2"
}

get-home-output() {
    if [[ $(amixer | sed ':a;N;$!ba;s/\n/ /g' | sed 's/Simple/\n/g' | grep "Speaker") = *"[on]"* ]]; then 
        echo "Speaker"
    elif [[ $(amixer | sed ':a;N;$!ba;s/\n/ /g' | sed 's/Simple/\n/g' | grep "Headphone") = *"[on]"* ]]; then
        echo "Headphone"
    fi
}

set-wall-tile() {
    feh --bg-tile --no-xinerama "$tile"
}

set-wall-single() {
    feh --bg-fill "$single"
    #set-wall-tile
}

set-wall-double() {
    #feh --bg-fill --no-xinerama "$double"
    set-wall-tile
}

set-wall-triple() {
    #feh --bg-fill --no-xinerama "$triple"
    set-wall-tile
}

print-help() {
    #echo -e "\e[1mUsage:\e[0m \e[1moutmux\e[0m [ -a|-v|-A \e[4moutput\e[0m ]"
    #echo -e "       \e[1moutmux\e[0m [ -h|--help ]"
    #echo -e "\e[1mOptions:\e[0m"
    #echo -e "  -a(udio) <output>           Defines the .asoundrc settings to send audio to a specified output"
    #echo -e "                                (cat ~/.asoundrc to see the details)"
    #echo -e "                                Accepted arguments: speaker, headphone, home, hdmi, loopback, usb, get-home-output"
    #echo -e "  -v(ideo) <output>           Uses xrandr to begin streaming video through the specified output"
    #echo -e "                                Accepted arguments: home, hdmi, hdmi-o"
    #echo -e "  -A(LL) <output>             Combine -a(udio) and -v(ideo) options into a single option"
    #echo -e "                                Accepted arguments: home, hdmi, hdmi-o"
    #echo -e "  -h,--help                   Displays this text"

    echo   "Usage: outmux [home|hdmi|hdmi-o|triple]"
    echo   "       outmux -h|--help"
}

case $1 in
    -h|--help)
        print-help
        exit 0
        ;;
    "")
        print-help
        exit 1
        ;;
    home)
        xrandr --output eDP1 --primary --auto \
            --output HDMI1 --off \
            --output HDMI2 --off
        set-wall-single
        #reset_blur
        #cp "$single" "/home/whooie/.control/blurbg/no_blur.$single_extension"
        #blur "$single" "/home/whooie/.control/blurbg/blur.$single_extension"
        xset -dpms
        xset s off
        ;;
    hdmi)
        mode=$(python $xrandr_checker "HDMI2" "1920x1080")
        xrandr \
            --output eDP1 \
                --primary \
                --auto \
            --output HDMI1 \
                --off \
            --output HDMI2 \
                --mode "$mode" \
                --right-of eDP1
        set-wall-double
        #reset_blur
        #cp "$double" "/home/whooie/.control/blurbg/no_blur.$double_extension"
        #blur "$double" "/home/whooie/.control/blurbg/blur.$double_extension"
        xset -dpms 
        xset s off
        ;;
    hdmi-u)
        mode=$(python $xrandr_checker "HDMI2" "1920x1080")
        w=$(echo $mode | awk 'BEGIN { FS="x" } { print $1 }')
        h=$(echo $mode | awk 'BEGIN { FS="x" } { print $2 }')
        xrandr \
            --output eDP1 \
                --primary \
                --auto \
            --output HDMI1 \
                --off \
            --output HDMI2 \
                --mode "$mode" \
                --fb "$[${w} - 2 * ${2}]x$[${h} - 2 * ${3}]" \
                --transform "1,0,-${2},0,1,-${3},0,0,1" \
                --right-of eDP1
        set-wall-double
        #reset_blur
        #cp "$double" "/home/whooie/.control/blurbg/no_blur.$double_extension"
        #blur "$double" "/home/whooie/.control/blurbg/blur.$double_extension"
        xset -dpms 
        xset s off
        ;;
    hdmi-o)
        mode=$(python $xrandr_checker "HDMI2" "1920x1080")
        xrandr \
            --output eDP1 \
                --primary \
                --auto \
            --output HDMI1 \
                --off \
            --output HDMI2 \
                --mode "$mode" \
                --right-of eDP1
        xrandr --output eDP1 --off
        set-wall-single
        #reset_blur
        #cp "$single" "/home/whooie/.control/blurbg/no_blur.$single_extension"
        #blur "$single" "/home/whooie/.control/blurbg/blur.$single_extension"
        xset -dpms
        xset s off
        ;;
    hdmi-m)
        mode=$(python $xrandr_checker "HDMI2" "1920x1080")
        xrandr \
            --output eDP1 \
                --primary \
                --auto \
            --output HDMI1 \
                --off \
            --output HDMI2 \
                --mode "$mode" \
                --same-as eDP1
        set-wall-single
        #reset_blur
        #cp "$single" "/home/whooie/.control/blurbg/no_blur.$single_extension"
        #blur "$single" "/home/whooie/.control/blurbg/blur.$single_extension"
        xset -dpms
        xset s off
        ;;
    triple)
        mode=$(python $xrandr_checker "HDMI1" "1920x1080")
        xrandr \
            --output eDP1 \
                --primary \
                --auto \
            --output HDMI1 \
                --mode "$mode" \
                --right-of eDP1

        mode=$(python $xrandr_checker "HDMI2" "1920x1080")
        xrandr \
            --output eDP1 \
                --primary \
                --auto \
            --output HDMI2 \
                --mode "$mode" \
                --right-of HDMI1
        set-wall-triple
        #reset_blur
        #cp "$triple" "/home/whooie/.control/blurbg/no_blur.$triple_extension"
        #blur "$triple" "/home/whooie/.control/blurbg/blur.$triple_extension"
        xset -dpms
        xset s off
        ;;
esac

#if [[ $is_help == "true" ]]; then
#    echo -e "Usage: \e[1mset_output.sh\e[0m [ -a|-v|-A \e[4moutput\e[0m ]"
#    echo -e "       \e[1mset_output.sh\e[0m [ -h|--help ]"
#    echo -e "\e[1mOptions:\e[0m"
#    echo -e "  -a(udio) <output>           Defines the .asoundrc settings to send audio to a specified output"
#    echo -e "                                (cat ~/.asoundrc to see the details)"
#    echo -e "                                Accepted arguments: speaker, headphone, home, hdmi, loopback, usb"
#    echo -e "  -v(ideo) <output>           Uses xrandr to begin streaming video through the specified output"
#    echo -e "                                Accepted arguments: home, hdmi, vga, triple"
#    echo -e "  -A(LL) <output>             Combine -a(udio) and -v(ideo) options into a single option"
#    echo -e "                                Accepted arguments: home, hdmi, triple"
#    echo -e "  -h,--help                   Displays this text"
#fi

