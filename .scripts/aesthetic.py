#!/usr/bin/python

import getopt
import sys

opts, args = getopt.gnu_getopt(sys.argv[1:], 'C')
caps = False
for opt, optarg in opts:
    if opt in ["-C"]:
        caps = True

if len(args) == 0:
    sys.exit(1)
else:
    res = ""
    for arg in args:
        for char in arg:
            res += (char.upper() if caps else char)+" "
        res = res[:-1]+"   "
    print(res[:-3])
    sys.exit(0)
