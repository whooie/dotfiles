#!/bin/bash

name="$1"
if [[ $name == "sudo" ]]; then
    name="$2"
fi
test "${name:-0}" == "0" || echo -e "$(date)\n"
test "${name:-0}" == "0" || time "$@"
test "${name:-0}" == "0" || echo -e "\n$(date)"
notify-send -t 2000 "${name:-bdone}" "process has ended"
if [[ $(amixer -c 0 sget Beep | tail -n 1 | awk '{print $6}') == "[off]" ]]; then
    beep_finish="amixer -qc 0 sset Beep mute"
fi
amixer -qc 0 sset Beep unmute
beep -f 500 -l 100 -d 50 -r 2
sleep 0.05
beep -f 400 -l 100
sleep 0.05
$beep_finish
echo -ne "\a"
