#!/usr/bin/env python

import sys
import getopt
import os
import os.path as path

# os.mkdir
# os.symlink
# os.listdir
# path.isdir
# path.isfile
# path.islink

def make_tree(sourcepath):
    abs_root = path.abspath(sourcepath)
    children = dict()
    diritems = os.listdir(abs_root)
    diritems.sort()
    for diritem in diritems:
        itempath = path.join(abs_root, diritem)
        if path.isfile(itempath) or path.islink(itempath):
            children[diritem] = path.splitext(itempath)[1]
        elif path.isdir(itempath):
            children[diritem] = make_tree(itempath)
        else:
            continue
    return children

def _make_linklist(tree, rootpath, targetpath, linklist):
    for diritem in tree.keys():
        if type(tree[diritem]) == str:
            linkname = input(f"{rootpath}: {diritem}\n> ")
            if linkname == "PASS":
                continue
            elif linkname == "STOP":
                break
            while linkname[-1:] == "/":
                linkname = linkname[:-1]
            linkname = linkname.replace("...", path.split(linkname)[1])
            linklist.append((path.join(rootpath, diritem),
                path.join(targetpath, linkname+tree[diritem])))
        elif type(tree[diritem]) == dict:
            make_linklist(tree[diritem], path.join(rootpath, diritem), targetpath, linklist)
        else:
            raise Exception

def make_linklist(tree, sourcepath, targetpath, linklist=[]):
    _make_linklist(tree, sourcepath, targetpath, linklist)
    return linklist

def mkdir_full(dirpath):
    if not path.isdir(dirpath):
        mkdir_full(path.split(dirpath)[0])
        os.mkdir(dirpath)
    else:
        pass

def make_links(linklist):
    for link in linklist:
        linkdir = path.split(link[1])[0]
        if not path.isdir(linkdir):
            mkdir_full(linkdir)
        os.symlink(link[0], link[1])

def main(argv):
    shortopts = ""
    longopts = [
    ]
    try:
        opts, args = getopt.gnu_getopt(argv[1:], shortopts, longopts)
    except getopt.GetoptError as ERR:
        raise ERR
    for opt, optarg in opts:
        pass
    if len(args) < 2:
        raise Exception("not enough args")

    sourcepath = path.abspath(args[0])
    targetpath = path.abspath(args[1])

    tree = make_tree(sourcepath)
    linklist = make_linklist(tree, sourcepath, targetpath)
    make_links(linklist)
    
    return 0

if __name__ == "__main__":
    main(sys.argv[1:])


