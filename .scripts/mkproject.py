#!/usr/bin/python

import sys
import getopt
from pathlib import Path
import os
import os.path as path
import shutil
import git
import toml
import pyaml

rustlibs = {
    "compute": "*",
    "indexmap": "*",
    "itertools": "*",
    "rand": "*",
    "nanorand": "*",
    "statrs": "*",
    "num-complex": "*",
    "rust_decimal": "*",
    "malachite": "*",
    "mendeleev": "*",
    "ndarray": "*",
    "ndarray-slice": "*",
    "ndarray-linalg": {
        "version": "*",
        "features": ["openblas-system"],
    },
    "ndarray-conv": "*",
    "ndarray-ndimage": "*",
    "ndarray-stats": "*",
    "ndarray-glm": "*",
    "ndarray-histogram": "*",
    "ndarray-rand": "*",
    "ndarray-npy": "*",
    "ndarray-npz": "*",
    "ndarray-csv": "*",
    "ndarray-unit": "*",
    "scilib": "*",
    "rustfft": "*",
    "ndrustfft": "*",
    "rmpfit": "*",
    "wigner-symbols": "*",
    "uom": "*",
    "bpaf": "*",
    "thiserror": "*",
    "anyhow": "*",
    "formatx": "*",
    "plotly": {
        "version": "*",
        "features": ["kaleido", "plotly_ndarray"],
    },
    "whooie": {
        "git": "https://gitlab.com/whooie/rust-lib.git",
        "version": "*",
        "features": [
            "nd",
            "ndarray-utils",
            "math",
            "plotting",
            "pyo3-utils"
        ],
    },
}

def git_config(fpath):
    outfile = open(fpath, 'w')
    outfile.write("""
[core]
    repositoryformatversion = 0
    filemode = true
    bare = false
    logallrefupdates = true
[remote "origin"]
    #url = git@gitlab:whooie/CHANGEME.git
    fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
    remote = origin
    merge = refs/heads/master
[pull]
    rebase = false
    """[1:-1])
    outfile.close()

def mdbook_book_toml(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    toml.dump({
        "book": {
            "authors": [ "whooie" ],
            "language": "en",
            "multilingual": False,
            "src": "src",
            "title": project_name.capitalize(),
            "description": ""
        },
        "preprocessor": {
            "katex": {
                "macros": (
                    "macros.tex" if copy_non_needed \
                        else "/home/whooie/Documents/templates/mdbook/macros.tex"
                ),
                "renderers": [ "html" ],
            }
        },
        "output": {
            "html": {
                "default-theme": "light",
                "preferred-dark-theme": "ayu",
                "theme": "theme",
                "no-section-label": False,
                "fold": {
                    "enable": True,
                },
                "playground": {
                    "editable": True,
                    "line-numbers": True,
                }
            },
            "katex": dict(),
        }
    }, outfile)
    outfile.close()

def mdbook_deploy(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    pyaml.dump({
        "pages": {
            "stage": "deploy",
            "image": "rust",
            "variables": {
                "CARGO_HOME": "$CI_PROJECT_DIR/cargo",
            },
            "before_script": [
                'export PATH="$CARGO_HOME/bin:$PATH"',
                "mdbook --version || cargo install mdbook",
                "mdbook-katex --version || cargo install mdbook-katex",
                "mdbook-toc --version || cargo install mdbook-toc",
            ],
            "script": [
                "test -d notes && cd notes && mdbook build -d ../public && cd .. || mdbook build -d public",
                "mv public/html/* public",
                "test -e public/404.html && rm public/404.html",
                "test -e public/403.html && rm public/403.html",
                "ln -s index.html public/404.html",
                "ln -s index.html public/403.html",
            ],
            "rules": [
                { "if": "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TITLE =~ /^PUBLISH: .*/" },
            ],
            "artifacts": {
                "paths": [ "public" ],
            },
            "cache": {
                "paths": [ "$CARGO_HOME/bin" ],
            },
        },
    }, outfile, safe=True, indent=2, vspacing=[1, 0], sort_dicts=False)
    outfile.close()

def cython_setup_py(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    outfile.write(f"""
from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize
import numpy as np

source_files = [

]
extensions = [
    Extension(
        name=X.replace("/", ".").replace(".pyx", ""),
        sources=[X],
        include_dirs=[np.get_include()]
    ) for X in source_files
]
annotate = False

setup(
    name="lib",
    ext_modules=cythonize(
        module_list=extensions,
        annotate=annotate,
        language_level=3
    ),
    include_dirs=[np.get_include()]
)
    """[1:-1])
    outfile.close()

def rust_cargo_toml(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    cargo_toml = {
        "package": {
            "name": project_name,
            "version": "0.1.0",
            "authors": ["whooie <wcwhuie@gmail.com>"],
            "edition": "2021",
        },
        "dependencies": {
            **rustlibs,
        },
        "bin": [
            {
                "path": "src/main.rs",
                "name": "main",
            },
        ],
        "lib": {
            "path": "lib/lib.rs",
            "name": "lib",
        },
        "profile": {
            "release": {
                "lto": True,
                "panic": "abort",
            },
        },
    }
    # if not copy_non_needed:
    #     cargo_toml["dependencies"].update({
    #         "whooie": {
    #             "path": "/home/whooie/Documents/templates/rust/whooie",
    #             "features": [
    #                 "nd",
    #                 "ndarray-utils",
    #                 "math",
    #                 "plotting",
    #                 "pyo3-utils"
    #             ],
    #         }
    #     })
    # else:
    #     cargo_toml["dependencies"].update({
    #         "ndarray": "*",
    #         "plotly": {
    #             "version": "*",
    #             "features": ["kaleido", "plotly_ndarray"],
    #         },
    #     })
    toml.dump(cargo_toml, outfile)
    outfile.close()

def pyo3_cargo_toml(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    cargo_toml = {
        "package": {
            "name": project_name,
            "version": "0.1.0",
            "authors": ["whooie <wcwhuie@gmail.com>"],
            "edition": "2021"
        },
        "dependencies": {
            **rustlibs,
            "numpy": "*",
            "pyo3": {
                "version": "*",
                "features": ["extension-module", "num-complex"],
            },
        },
        "lib": {
            "path": "lib/lib.rs",
            "name": "lib",
            "crate-type": ["cdylib", "rlib"]
        },
        "profile": {
            "release": {
                "lto": True,
                "panic": "abort",
            },
        },
    }
    if not copy_non_needed:
        pass
        # cargo_toml["dependencies"].update({
        #     "whooie": {
        #         "path": "/home/whooie/Documents/templates/rust/whooie"
        #     }
        # })
    else:
        cargo_toml["dependencies"].update({
            "ndarray": "*",
            "plotly": {
                "version": "*",
                "features": ["kaleido", "plotly_ndarray"],
            },
            "pyo3": {
                "version": "*",
                "features": ["extension-module", "num-complex"],
            },
        })
    toml.dump(cargo_toml, outfile)
    outfile.close()

def pyo3_setup_py(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    outfile.write("""
from setuptools import setup
from setuptools_rust import Binding, RustExtension
import toml

cargo_file = "Cargo.toml"
cargo = toml.load(cargo_file)
setup_requires = [
    "setuptools",
    "setuptools-rust"
]

source_files = [

]
extensions = [
    RustExtension(
        (X
            .replace("/", ".")
            .replace(".rs", "")
        ),
        path=cargo_file,
        binding=Binding.PyO3,
        debug=False
    ) for X in source_files
]

setup(
    name=cargo["package"]["name"],
    version=cargo["package"]["version"],
    packages=[cargo["lib"]["name"]],
    rust_extensions=extensions,
    setup_requires=setup_requires,
    zip_safe=False
)
    """[1:-1])
    outfile.close()

def cpp_makefile(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    outfile.write("""
CC := g++
CFLAGS := -Wall -O3
ROOTDIR := "$(shell pwd)"
SRCDIR := src
OBJDIR := obj
SRCS := $(shell find "$(SRCDIR)" -name "*.cpp")
OBJS := $(addprefix $(OBJDIR)/,$(SRCS:$(SRCDIR)/%.cpp=%.o))
MAIN := main

default: $(MAIN)

$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(MAIN)

$(OBJDIR)%.o: $(SRCDIR)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) $(OBJDIR)/*.o $(MAIN)
    """[1:-1])
    outfile.close()

def dune_project(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    outfile.write(
f"""
(lang dune 3.12)
(generate_opam_files true)
(name {project_name})
(authors "whooie <wchooie@gmail.com>")

; (maintainers "whooie <wchooie@gmail.com>")
; (source (github whooie/CHANGEME))
; (license CHANGEME)
; (documentation https://CHANGEME.com)

(package
 (name {project_name})
 (synopsis "CHANGEME")
 (description "CHANGEME")
 (depends
  ocaml
  dune
  ; owl
 )
 ; (tags (topics ...))
)
"""[1:-1]
    )
    outfile.close()

def dune_lib(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    outfile.write(
f"""
(library
 (name {project_name})
 (public_name {project_name})
 (modules {project_name})
 ; (libraries ...)
)
"""[1:-1]
    )
    outfile.close()
    open(fpath.parent.joinpath(project_name).with_suffix(".ml"), 'w').close()

def dune_src(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    outfile.write(
f"""
(executable
 (name main)
 (public_name main)
 ; (libraries ...)
)
"""[1:-1]
    )
    outfile.close()

def tectonic_tectonic_toml(project_name, fpath, copy_non_needed, *args, **kwargs):
    outfile = open(fpath, 'w')
    toml.dump({
        "doc": {
            "name": project_name,
            "bundle": "https://ttassets.z13.web.core.windows.net/tlextras-2020.0r0.tar",
        },
        "output": [
            {
                "name": "default",
                "type": "pdf",
                "tex_format": "latex",
            },
        ]
    }, outfile)
    outfile.close()

supported_note_formats = ["latex", "typst", "tectonic", "mdbook"]
notes_dirname = "notes"
supported_languages = ["python", "cython", "rust", "pyo3", "ocaml", "mun", "c++"]
programs_dirname = "programs"

class Dir:
    def __init__(self, path, keep=True):
        self.path = Path(path)
        self.keep = keep

class File:
    def __init__(self, name, source, target, needed):
        self.name = name
        self.source = source
        self.target = target
        self.needed = needed

class Feature:
    def __init__(self, method, target):
        self.method = method
        self.target = target

class Structure:
    def __init__(self, dirs, files, ignore, features=None):
        self.dirs = dirs
        self.files = files
        self.ignore = ignore
        self.features = features if features is not None else dict()

    def generate(self, root, project_name, copy_non_needed, gitfiles=False,
            features=None):
        if not root.is_dir():
            root.mkdir()

        for D in self.dirs:
            d = root.joinpath(D.path)
            if not d.is_dir():
                d.mkdir()
            if D.keep and gitfiles:
                d.joinpath(".gitkeep").open('a').close()

        for F in self.files:
            if isinstance(F.source, Path) \
                    and (copy_non_needed or F.needed):
                try:
                    shutil.copy(
                        F.source.joinpath(F.name),
                        root.joinpath(F.target, F.name)
                    )
                except IsADirectoryError:
                    shutil.copytree(
                        F.source.joinpath(F.name),
                        root.joinpath(F.target, F.name),
                        dirs_exist_ok=True
                    )
            elif isinstance(F.source, type(lambda:0)) \
                    and (copy_non_needed or F.needed):
                F.source(
                    project_name,
                    root.joinpath(F.target, F.name),
                    copy_non_needed
                )
            elif isinstance(F.source, type(None)):
                root.joinpath(F.target, F.name).open('a').close()

        if gitfiles:
            root.joinpath(".gitignore").open('a').close()
            gitignore = root.joinpath(".gitignore").open('r+')
            ign_lines = gitignore.readlines()
            for ign in self.ignore:
                if ign+'\n' not in ign_lines:
                    gitignore.write(ign+'\n')
            gitignore.close()

        if features is not None:
            for f in features:
                if f in self.features.keys():
                    if isinstance(self.features[f], Feature):
                        self.features[f].method(
                            project_name,
                            root.joinpath(self.features[f].target),
                            copy_non_needed
                        )
                    elif isinstance(self.features[f], type(None)):
                        self.features[f].open('a').close()
                    else:
                        raise Exception(f"Improperly formatted feature {f}")
                else:
                    continue

config = type("Config", (), dict(
    project_dir=Path("./myproject"),
    notes_format="latex",
    languages=["python"],
    notes=True,
    programming=True,
    copy_sty=False,
    copy_lib=False,
    use_current_dir=False,
    gitfiles=False,
    init_git=False,
    features=list(),
    notes_structures={
        "latex": Structure(
            dirs = [
                Dir("assets"),
                Dir("biblio"),
                Dir("build"),
                Dir("drafts"),
                Dir("subdocs"),
            ],
            files = [
                File("whooie.cls",
                    source = Path("/home/whooie/Documents/templates/latex/whooie"),
                    target = Path(""),
                    needed = False
                ),
                File("main.tex",
                    source = None,
                    target = Path(""),
                    needed = True
                ),
            ],
            ignore = [
                "build/*.aux",
                "build/*.log",
                "build/*.out",
                "build/*.pdf",
                "build/*.nav",
                "build/*.snm",
                "build/*.toc",
                "*.pdf",
                "!assets/*"
            ]
        ),
        "typst": Structure(
            dirs = [
                Dir("assets"),
            ],
            files = [
                File("whooie.typ",
                    source = Path("/home/whooie/Documents/whooie-typst/0.1.0"),
                    target = Path(""),
                    needed = False,
                ),
                File("main.typ",
                    source = None,
                    target = Path(""),
                    needed = True,
                ),
            ],
            ignore = [
                "*.pdf",
                "!assets/*",
            ]
        ),
        "tectonic": Structure(
            dirs = [
                Dir("assets"),
                Dir("biblio"),
                Dir("build"),
                Dir("drafts"),
                Dir("src"),
                Dir("src/subdocs"),
            ],
            files = [
                File("whooie.cls",
                    source = Path("/home/whooie/Documents/templates/latex/whooie"),
                    target = Path("src"),
                    needed = True
                ),
                File("_preamble.tex",
                    source = None,
                    target = Path("src"),
                    needed = True
                ),
                File("_postamble.tex",
                    source = None,
                    target = Path("src"),
                    needed = True
                ),
                File("index.tex",
                    source = None,
                    target = Path("src"),
                    needed = True
                ),
                File("Tectonic.toml",
                    source = tectonic_tectonic_toml,
                    target = Path(""),
                    needed = True
                ),
            ],
            ignore = [
                "build/*.aux",
                "build/*.log",
                "build/*.out",
                "build/*.pdf",
                "build/*.nav",
                "build/*.snm",
                "build/*.toc",
                "*.pdf",
                "!assets/*"
            ]
        ),
        "mdbook": Structure(
            dirs = [
                Dir("assets"),
                Dir("book"),
                Dir("src"),
                Dir("src/css"),
            ],
            files = [
                File("SUMMARY.md",
                    source = None,
                    target = Path("src/"),
                    needed = True
                ),
                File("book.toml",
                    source = mdbook_book_toml,
                    target = Path(""),
                    needed = True
                ),
                File("macros.tex",
                    source = Path("/home/whooie/Documents/templates/mdbook"),
                    target = Path(""),
                    needed = False
                ),
                File("whooie.css",
                    source = Path("/home/whooie/Documents/templates/mdbook"),
                    target = Path("src/css"),
                    needed = True
                ),
            ],
            ignore = [
                "book/*",
            ],
            features = {
                "mdbook_deploy": Feature(
                    method = mdbook_deploy,
                    target = Path(".gitlab-ci.yml")
                ),
            }
        ),
    },
    lang_structures={
        "python": Structure(
            dirs = [
                Dir("lib"),
                Dir("src"),
            ],
            files = [
                File("__init__.py",
                    source = None,
                    target = Path("lib"),
                    needed = False
                ),
                File("analysis.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("colordefs.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("pyplotdefs.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("plotlydefs.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("misc.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("phys.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("main.py",
                    source = None,
                    target = Path("src"),
                    needed = True
                ),
            ],
            ignore = [
                "**/__pycache__/*",
                "**.png",
                "**.svg",
            ]
        ),
        "cython": Structure(
            dirs = [
                Dir("lib"),
                Dir("src"),
            ],
            files = [
                File("__init__.py",
                    source = None,
                    target = Path("lib"),
                    needed = False
                ),
                File("analysis.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("colordefs.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("pyplotdefs.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("plotlydefs.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("misc.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("phys.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("main.py",
                    source = None,
                    target = Path("src"),
                    needed = True
                ),
            ],
            ignore = [
                "**/__pycache__/*",
                "**/build/*",
                "**.so",
                "**.png",
                "**.svg",
            ]
        ),
        "rust": Structure(
            dirs = [
                Dir("lib"),
                Dir("src"),
            ],
            files = [
                File("Cargo.toml",
                    source = rust_cargo_toml,
                    target = Path(""),
                    needed = True
                ),
                File("main.rs",
                    source = None,
                    target = Path("src"),
                    needed = True
                ),
                File("lib.rs",
                    source = None,
                    target = Path("lib"),
                    needed = True
                ),
            ],
            ignore = [
                "**/target/*",
                "**.png",
                "**.svg",
                "**.pdf",
            ],
        ),
        "pyo3": Structure(
            dirs = [
                Dir("lib"),
                Dir("src"),
            ],
            files = [
                File("lib.rs",
                    source = None,
                    target = Path("lib"),
                    needed = True
                ),
                File("Cargo.toml",
                    source = pyo3_cargo_toml,
                    target = Path(""),
                    needed = True
                ),
                File("__init__.py",
                    source = None,
                    target = Path("lib"),
                    needed = False
                ),
                File("analysis.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("colordefs.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("pyplotdefs.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("plotlydefs.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("misc.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("phys.py",
                    source = Path("/home/whooie/.python-whooie"),
                    target = Path("lib"),
                    needed = False
                ),
                File("setup.py",
                    source = pyo3_setup_py,
                    target = Path(""),
                    needed = True
                ),
                File("main.py",
                    source = None,
                    target = Path("src"),
                    needed = True
                ),
            ],
            ignore = [
                "**/__pycache__/*",
                "**/target/*",
                "**.so",
                "**.png",
                "**.svg",
                "**.pdf",
            ]
        ),
        "ocaml": Structure(
            dirs = [
                Dir("lib"),
                Dir("src"),
            ],
            files = [
                File("dune-project",
                    source = dune_project,
                    target = Path(""),
                    needed = True
                ),
                File("dune",
                    source = dune_src,
                    target = Path("src"),
                    needed = True
                ),
                File("main.ml",
                    source = None,
                    target = Path("src"),
                    needed = True,
                ),
                File("dune",
                    source = dune_lib,
                    target = Path("lib"),
                    needed = True
                ),
            ],
            ignore = [
                "**/_build/*",
                "**/_opam/*",
                "**.png",
                "**.svg",
                "**.pdf",
            ],
        ),
        "mun": Structure(
            dirs = list(),
            files = [
                File("main.mun",
                    source = None,
                    target = Path(""),
                    needed = True
                ),
            ],
            ignore = [
                "**.munlib",
            ]
        ),
        "c++": Structure(
            dirs = list(),
            files = [
                File("makefile",
                    source = cpp_makefile,
                    target = Path(""),
                    needed = True
                ),
                File("main.cpp",
                    source = None,
                    target = Path(""),
                    needed = True
                ),
            ],
            ignore = [
                "**.png",
                "**.svg",
            ]
        ),
    },
))()

available_features = list()
for S in config.notes_structures.values():
    if S.features is not None:
        available_features += list(S.features.keys())
for S in config.lang_structures.values():
    if S.features is not None:
        available_features += list(S.features.keys())

help_text = f"""
Create a general project directory to house various relevant files.

Usage: mkproject [ options ] [ project_dir ]

Options:
    -n, --notes-format <str>
        Specify the format for taking notes. Formats currently supported:
            {supported_note_formats}
        Unsupported formats will raise an exception.
        Default: '{config.notes_format}'

    -p, --programming-languages <lang1>[,...]
        Specify the comma-separated programming language(s) for the project.
        Languages currently supported:
            {supported_languages}
        Unsupported languages will not raise exception.
        Default: '{"".join([x+"," for x in config.languages])[:-1]}'

    -N, --notes-only
        Create only notes structures starting at the project root.

    -P, --programming-only
        Create only programming structures starting at the project root.

    -s, --copy-style-files
        Copy all relevant style files for the notes format selected.

    -l, --copy-lib-files
        Copy all relevant library files for the languages selected.

    -f, --features <feature1>[,...]
        Specify special features.
        Available features:
            {available_features}

    -., --current-directory
        Initialize in the current directory.

    -g, --gitfiles
        Generate gitignore and gitkeep files.

    -G, --init-git
        Initialize a git repository. Automatically raises -g, -s, and -l.

    -h, --help
        Display this text and exit.
"""[1:-1]

shortopts = "n:p:NPslf:.gGh"
longopts = [
    "notes-format=",
    "programming-languages=",
    "notes-only",
    "programming-only",
    "copy-style-files",
    "copy-lib-files",
    "features="
    "current-directory",
    "gitfiles",
    "init-git",
    "help"
]
try:
    opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
    for opt, optarg in opts:
        if opt in ["-n", "--notes-format"]:
            if optarg not in supported_note_formats:
                raise Exception("Notes format is not supported.")
            config.notes_format = optarg

        elif opt in ["-p", "--programming-languages"]:
            config.languages = optarg.split(",")

        elif opt in ["-N", "--notes-only"]:
            config.notes = True
            config.programming = False
            notes_dirname = ""

        elif opt in ["-P", "--programming-only"]:
            config.notes = False
            config.programming = True
            programs_dirname = ""

        elif opt in ["-s", "--copy-style-files"]:
            config.copy_sty = True

        elif opt in ["-l", "--copy-lib-files"]:
            config.copy_lib = True

        elif opt in ["-f", "--features"]:
            config.features = optarg.split(",")

        elif opt in ["-.", "--current-directory"]:
            config.use_current_dir = True

        elif opt in ["-g", "--gitfiles"]:
            config.gitfiles = True

        elif opt in ["-G", "--init-git"]:
            config.init_git = True
            config.gitfiles = True
            config.copy_sty = True
            config.copy_lib = True

        elif opt in ["-h", "--help"]:
            print(help_text)
            sys.exit(0)

except Exception as ERR:
    print(help_text)
    raise ERR

if config.use_current_dir:
    config.project_dir = Path.cwd()
elif len(args) > 0:
    config.project_dir = Path(args[0])
if not config.project_dir.is_dir():
    config.project_dir.mkdir()
if config.init_git:
    G = git.Git(config.project_dir)
    G.init()
    git_config(config.project_dir.joinpath(".git/config"))
    config.project_dir.joinpath(".gitignore").open("a").close()

if config.notes:
    notes_dir = config.project_dir.joinpath(notes_dirname)
    config.notes_structures[config.notes_format].generate(
        notes_dir,
        config.project_dir.name,
        config.copy_sty,
        config.gitfiles,
        config.features
    )

if config.programming:
    programs_dir = config.project_dir.joinpath(programs_dirname)
    for lang in config.languages:
        if lang not in config.lang_structures.keys():
            continue
        config.lang_structures[lang].generate(
            programs_dir,
            config.project_dir.name,
            config.copy_lib,
            config.gitfiles,
            config.features
        )

