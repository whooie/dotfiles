#!/bin/bash

ranger --choosedir="$temp_file" -- "${@:-$PWD}"
chosen_dir="$(cat -- "$temp_file")"
rm -f -- "$temp_file"
echo "${chosen_dir:-$PWD}" > /home/whooie/.rcd_tmp
