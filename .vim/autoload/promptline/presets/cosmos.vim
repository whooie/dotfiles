fun! promptline#presets#cosmos#get()
  return {
    \'a'    : [ '\j', '$(echo $HOSTNAME@$USER)' ],
    \'b'    : [ promptline#slices#cwd() ],
    \'c'    : [ '\$' ],
    \'warn' : [ promptline#slices#last_exit_code() ]}
endfun
