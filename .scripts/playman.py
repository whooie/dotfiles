#!/usr/bin/python

import getopt
import subprocess
import sys
import re
import os
import youtube_dl
config = type("Config", (), {})
config.add_mode = False
config.prepend = False
config.keep_url_vars = False
config.clear_after = False
config.playlist_file = os.getenv("HOME")+"/.config/playman"
config.default_format = "bestvideo+bestaudio"
config.no_format = False

helptext = f"""
Play a list of urls contained in a playlist file with mpv.

Usage:  playman [ options... ] [ playlist_file ]
Options:
    -a, --add
        Add a url to the playlist file.
    -f, --format
        Specify a preferred youtube-dl format for the video being added. If the
        format is not available, the fallback is `{config.default_format}`.
    -F, --forget-format
        During playback, don't use the format from the playlist file and instead
        just use `{config.default_format}`.
    -k, --keep-url-vars
        Keep variables in the url marked by `&`.
    -p, --print-playlist
        Print the contents of the playlist file and exit.
    -c, --clear
        Clear the playlist after playing.
    -C, --clear-now
        Clear the playlist and exist.
    -h, --help
        Display this text and exit.
"""

def playlist_fmt(url_format, playlist_file, ytdl, keep_vars=False):
    pattern = re.compile("^([0-9]*)")
    url = url_format[0].replace("&", "\\&") if keep_vars else url_format[0].split("&")[0]
    
    info = ytdl.extract_info(url, download=False)
    title = info["title"]
    fmts = list(map(lambda x: x["format_id"], info["formats"]))

    fmt = url_format[1] if url_format[1] in fmts else config.default_format
    return f"\"{title}\" {fmt} {url}\n"


def playlist_add(url_formats, playlist_file, keep_vars=False, prepend=False):
    ytdl = youtube_dl.YoutubeDL({"quiet": True})
    lines = list()
    for url_format in url_formats:
        line = playlist_fmt(url_format, playlist_file, ytdl, keep_vars)
        if line == None:
            print(f"Invalid url `{url_format[0]}`")
            return 1
        else:
            lines.append(line)
    if prepend:
        outfile = open(playlist_file, 'r')
        prelines = outfile.readlines()
        outfile.close()
        outfile = open(playlist_file, 'w')
        outfile.writelines(lines+prelines)
    else:
        outfile = open(playlist_file, 'a')
        outfile.writelines(lines)
    outfile.close()
    return 0


def print_playlist(playlist_file):
    pattern = re.compile("^\"(.*)\" ([a-z0-9+]+) ([a-zA-Z0-9:/&=?._\-]+)")
    #subprocess.run(["touch", playlist_file])
    if not os.path.isfile(playlist_file):
        open(playlist_file, 'a').close()
    infile = open(playlist_file, 'r')
    lines = infile.readlines()
    infile.close()
    while '' in lines:
        lines.remove('')
    if len(lines) == 0:
        print(f"Playlist `{playlist_file}` is empty")

    for i in range(len(lines)):
        match = pattern.match(lines[i])
        if match:
            print(f"TITLE:  {match.group(1)}")
            print(f"URL:    {match.group(3)}")
            print(f"FORMAT: {match.group(2)}")
        else:
            print(f"Improperly formatted item on line {i+1}:\n`{lines[i]}`")
        print("")


def main(argv):
    shortopts = "a:A:f:FkpcCh"
    longopts = [
        "add=",
        "add-pre=",
        "format=",
        "forget-format",
        "keep-url-vars",
        "print-playlist",
        "clear",
        "clear-now",
        "help"
    ]
    url_formats = list()
    current_format = config.default_format
    try:
        opts, args = getopt.gnu_getopt(argv[1:], shortopts, longopts)
        if len(args) == 0:
            args.append(config.playlist_file)
        playlist_file = args[0]
        for opt, optarg in opts:
            if opt in ["-a", "--add"]:
                config.add_mode = True
                url_formats.append(tuple([optarg, current_format]))
            elif opt in ["-A", "--add-pre"]:
                config.add_mode = True
                config.prepend = True
                url_formats.append(tuple([optarg, current_format]))
            elif opt in ["-f", "--format"]:
                current_format = optarg
            elif opt in ["-F", "--forget-format"]:
                config.no_format = True
            elif opt in ["-k", "--keep-url-vars"]:
                config.keep_url_vars = True
            elif opt in ["-p", "--print-playlist"]:
                print_playlist(playlist_file)
                return 0
            elif opt in ["-c", "--clear"]:
                config.clear_after = True
            elif opt in ["-C", "--clear-now"]:
                os.remove(args[0])
                open(args[0], 'a').close()
                print(f"Playlist `{playlist_file}` has been cleared")
                return 0
            elif opt in ["-h", "--help"]:
                print(helptext)
                return 0
            else:
                print(f"Invalid option `{opt}`")
                print(helptext)
                return 1
    except Exception as e:
        print(f"Error: {e}")
        return 1

    if config.add_mode:
        return playlist_add(url_formats, playlist_file, config.keep_url_vars, config.prepend)

    pattern = re.compile("^\"(.*)\" ([a-z0-9+]+) ([a-zA-Z0-9:/&=?._\-]+)")
    if not os.path.isfile(playlist_file):
        open(playlist_file, 'a').close()
    infile = open(playlist_file, 'r')
    lines = infile.readlines()
    infile.close()
    while '' in lines:
        lines.remove('')

    playlist = list()
    for i in range(len(lines)):
        match = pattern.match(lines[i])
        if match:
            playlist.append(tuple([match.group(3), match.group(2)]))
        else:
            print(f"Improperly formatted item on line {i+1}:\n`{lines[i]}`")
    if len(playlist) < 1:
        print("Playlist is empty.")
        return 0
    if not config.no_format:
        urls = list()
        current_format = playlist[0][1]
        while len(playlist) > 0:
            video = playlist.pop(0)
            if video[1] != current_format:
                subprocess.run(["mpv", f"--ytdl-format={current_format}"]+urls)
                urls.clear()
                urls.append(video[0])
                current_format = video[1]
            else:
                urls.append(video[0])
        if len(urls) > 0:
            subprocess.run(["mpv", f"--ytdl-format={current_format}"]+urls)
    else:
        urls = [video[0] for video in playlist]
        subprocess.run(["mpv", f"--ytdl-format={config.default_format}"]+urls)

    if config.clear_after:
        os.remove(args[0])
        open(args[0], 'a').close()
        print(f"Playlist `{playlist_file}` has been cleared")

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
