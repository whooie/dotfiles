#!/usr/bin/python

import getopt
import sys
import subprocess
import re
import os.path as path
import urllib.request
import json

def main(argv):
    shortopts = ""
    longopts = [
    ]
    try:
        opts, args = getopt.gnu_getopt(argv[1:], shortopts, longopts)
        if len(args) == 0:
            return 1
    except Exception as e:
        print(f"Exception occurred while parsing commandline args:\n{e}")
        return 1

    pattern = re.compile(r'http[s]?://(www\.|i\.|v\.|cdna\.|cdn\.|clips\.|pbs\.)?([a-zA-Z0-9]+)\.(it|com|be|me|net|tv)/(.*)')
    match = pattern.match(args[0])

    if match:

        # hosted by reddit
        if match.group(2) in ["redd"]:

            # video or gif
            if match.group(1) in ["v."] or path.splitext(match.group(4))[1] in [".gif", ".gifv"]:
                subprocess.run(["mpv", "--loop-file=inf", match.group(0)])
                return 0

            # image
            elif match.group(1) in ["i."]:
                subprocess.run(["feh", "-.d", match.group(0)])
                return 0
        
        # gfycat
        elif match.group(2) in ["gfycat"]:
            subprocess.run(["mpv", "--loop-file=inf", match.group(0)])
            return 0

        # imgur
        elif match.group(2) in ["imgur"]:

            # direct link to png or jpg
            if path.splitext(match.group(4))[1] in [".png", ".jpg"]:
                subprocess.run(["feh", "-.d", match.group(0)])
                return 0

            # direct link to gif
            elif path.splitext(match.group(4))[1] in [".gif", ".gifv"]:
                subprocess.run(["mpv", "--loop-file=inf", match.group(0)])
                return 0

            # link to album
            elif match.group(4)[:2] in ["a/", "gallery/"]:
                albumpage = urllib.request.urlopen(match.group(0)).read()

                pattern = re.compile(r'item\: (.*)\}\;')
                json_image_list = json.loads(pattern.findall(str(albumpage))[0].split(r"\n")[0].replace("\\", ''))["album_images"]["images"]

                images = list(map(lambda x: x["hash"]+x["ext"], json_image_list))
                urls = list()
                for image in images:
                    urls.append("https://i.imgur.com/"+image)
                subprocess.run(["feh", "-.d"]+urls)
                return 0

            # last-ditch attempt to find png or jpg (fuck imgur)
            else:
                albumpage = urllib.request.urlopen(match.group(0)).read()
                images = list()
                pattern = re.compile(r'(https://(i\.|www\.)?imgur\.com/[a-zA-Z0-9]+\.(png|jpg))')
                for link in pattern.findall(str(albumpage)):
                    images.append(link[0])
                subprocess.run(["feh", "-.d"]+images[:1])
                return 0

        # youtube
        elif match.group(2) in ["youtube", "youtu"]:
            subprocess.run(["mpv", match.group(0)])
            return 0

        # vimeo
        elif match.group(2) in ["vimeo"]:
            subprocess.run(["mpv", match.group(0)])
            return 0

        # streamable
        elif match.group(2) in ["streamable"]:
            subprocess.run(["mpv", "--loop-file=inf", match.group(0)])
            return 0

        # twitch
        elif match.group(2) in ["twitch"]:
            if match.group(1) in ["clips."]:
                subprocess.run(["mpv", "--loop-file=inf", match.group(0)])
            else:
                subprocess.run(["mpv", match.group(0)])

        # awwnime
        elif match.group(2) in ["awwni"]:
            subprocess.run(["feh", "-.d", match.group(0)])
            return 0

        # twitter media
        elif match.group(2) in ["twimg"]:
            subprocess.run(["feh", "-.d", match.group(0)])
            return 0

        # soundcloud
        elif match.group(2) in ["soundcloud"]:
            subprocess.run(["urxvt", "-e", "mpv", match.group(0)])
            return 0

        # artstation
        elif match.group(2) in ["artstation"]:
            subprocess.run(["feh", "-.d", match.group(0)])
            return 0

        # just in case it's still a direct link to a file
        else:
            if path.splitext(match.group(4))[1] in [".png", ".jpg"]:
                subprocess.run(["feh", "-.d", match.group(0)])
                return 0
            elif path.splitext(match.group(4))[1] in [".gif", ".gifv", ".webm"]:
                subprocess.run(["mpv", "--loop-file=inf", match.group(0)])
                return 0
            else:
                return 1
    else:
        return 1

if __name__ == "__main__":
    sys.exit(main(sys.argv))
