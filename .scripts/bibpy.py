#!/usr/bin/python

import getopt
import sys
import os.path as path
import re
import copy
config = type("Config", (), dict(
    source_format=None,
    available_methods={None: True},
    aux_file=None,
    tex_file=None,
    output=None,
    follow_links=False,
    export=False,
    export_format=None,
    quiet=False,
    verbose=False,
))()

try:
    import pyaml
    config.available_methods["yaml"] = True
    config.available_methods["yml"] = True
except:
    print("WARNING: could not load module `pyaml`.")
    print("Check that package `pyaml` is installed correctly.")
    config.available_methods["yaml"] = False
    config.available_methods["yml"] = False
try:
    import toml
    config.available_methods["toml"] = True
except:
    print("WARNING: could not load module `toml`.")
    print("Check that package `toml` is installed correctly.")
    config.available_methods["toml"] = False
try:
    import json
    config.available_methods["json"] = True
except:
    print("WARNING: could not load module `json`.")
    print("Check that your PYTHON distribution is installed correctly.")
    config.available_methods["json"] = False
try:
    import bibtexparser as bib
    config.available_methods["bibtex"] = True
    config.available_methods["bib"] = True
except:
    print("WARNING: could not load module `bibtexparser`.")
    print("Check that package `bibtexparser` is installed correctly.")
    config.available_methods["bibtex"] = False
    config.available_methods["bib"] = False
try:
    import configparser as ini
    config.available_methods["ini"] = True
except:
    print("WARNING: could not load module `configparser`.")
    print("Check that your PYTHON distribution is installed correctly.")
    config.available_methods["ini"] = False

helptext = """
Generates latex thebibliography environment code for inclusion with the \\input
command, given a YAML, JSON, INI, or BibTeX source file. By default, bibpy will
interpret source files by their extensions.

Usage: bibpy [ options ] <bib_file.ext>[...]
       bibpy -f
       bibpy -h
Options:
    -a, --aux <aux file>
        Supply a .aux file for sorting.
    -d, --doc <tex document file>
        Supply a .tex file for sorting.
    -o, --output
        Specify a path to the output file. If unspecified, the filename (minus
        the extension) will be the same as that of the first source file
        specified.
    -l, --follow-links
        Follow links to other .tex files found in the target .tex file given by
        \\input command calls. If provided, bibpy should be run from the same
        location as the given .tex file.
    -y, --yaml / -t, --toml / -j, --json / -i, --ini / -b, --bibtex 
        Load in YAML/TOML/JSON/INI/BibTeX-only mode. If unspecified, determine
        loading format on a per-file basis by checking the file extensions.
    -E, --export
        Run in export mode.
    -Y, --export-yaml / -T, --export-toml / -J, --export-json / -I, --export-ini
            / -B, --export-bibtex
        If running in export mode, set the output format. If unspecified,
        default is BibTex.
    -f, --formatting
        Print information on expected keys and formatting and exit.
    -q, --quiet
        Suppress all non-fatal, non-import warnings.
    -v, --verbose
        Print extra in-progress messages.
    -h, --help
        Display this text and exit.
"""[1:-1]

class Field:
    """
    A specific field in a citation format. Callable on a dictionary holding
    source information. Pre-processor function `preproc` is passed the value of
    the field only as a single positional argument.

    Attributes
    ----------
    name : str
    fmt : str
    optional : bool
    aliases : [str]
    overridden_by : [str]
    preproc : function or None
    rmchrs : int
    endspace : bool
    """
    def __init__(self, name: str, fmt: str, optional: bool=False,
            aliases: list=[], overridden_by: list=[], preproc=None,
            rmchrs: int=0, endspace: bool=True):
        self.name = name
        self.fmt = fmt
        self.optional = optional
        self.aliases = copy.deepcopy(aliases)
        self.overridden_by = copy.deepcopy(overridden_by)
        self.preproc = preproc
        self.rmchrs = rmchrs
        self.endspace = endspace

    def __call__(self, source) -> str:
        overridden = any(map(lambda x: x in source.keys(), self.overridden_by))
        if overridden:
            return ""
        all_keys = [self.name]+self.aliases
        has_keys = list(map(lambda x: x in source.keys(), all_keys))
        if not any(has_keys):
            if self.optional:
                return ""
            else:
                raise KeyError(self.name)
        key = all_keys[has_keys.index(True)]
        if self.preproc is not None:
            fieldval = self.preproc(source[key])
        else:
            fieldval = source[key]
        out = self.fmt.format(trim_str(str(fieldval)))
        return out

class CitationFormat:
    """
    A citation format for a given source type. Callable on a dictionary holding
    source information.

    Attributes
    ----------
    fields : list[Field]
    postproc : function(dict) or None
    """
    def __init__(self, name: str, fields: list, postproc=None):
        self.name = name
        self.fields = fields
        self.postproc = postproc

    def __call__(self, source, ref="") -> str:
        acc = ""
        for field in self.fields:
            try:
                F = field(source)
            except KeyError as e:
                if not config.quiet:
                    print(f"WARNING: ref '{ref}' lacks required field '{e}'.")
                continue
            if len(F) > 0:
                acc = acc[:len(acc)-field.rmchrs]+F+field.endspace*" "
        if self.postproc is not None:
            return self.postproc(acc[:-1])
        else:
            return acc[:-1]+"."

    def __str__(self) -> str:
        toy_source = {f.name: f.name.upper() for f in self.fields}
        res = self.name.upper()+"\n" + len(self.name)*"-"+"\n"
        res += breaklines(self.__call__(toy_source), 80) + "\n\n"
        res += "    FIELD       OPTIONAL    OVERRIDDEN BY       ALIASES\n"
        res += "    -----       --------    -------------       -------\n"
        fmt = "    {:12s}{:12s}{:20s}{:12s}\n"
        for f in self.fields:
            res += fmt.format(
                f.name,
                str(f.optional),
                str(f.overridden_by if len(f.overridden_by) > 0 else "<None>"),
                str(f.aliases if len(f.aliases) > 0 else "<None>")
            )
        res += "\n"
        return res[:-1]

def breaklines(s: str, n: int, c: str=' ') -> str:
    """
    Insert newline characters into `s` such that when printed as lines, each
    line of `s` is at most `n` characters wide. Respects newline characters
    already present in `s` and only breaks on words delimited by `c`.
    """
    words = s.split(c)
    res = ""
    line = ""
    while len(words) > 0:
        word = words.pop(0)
        subwords = word.split('\n')
        for i in range(len(subwords)):
            if len(line + subwords[i]) > n:
                res += line+'\n'
                line = subwords[i]+' '
            else:
                line += subwords[i]+' '
                if len(line) > n:
                    line = line[:-1]
            if i != len(subwords)-1:
                res += line+'\n'
                line = ""
    res += line
    return res[:-1]

def _concat_multiline(s1: str, s2: str) -> str:
    lines1 = s1.split('\n')
    lines2 = s2.split('\n')
    acc = ""
    while len(lines1) > 0 or len(lines2) > 0:
        line1 = lines1.pop(0) if len(lines1) > 0 else ""
        line2 = lines2.pop(0) if len(lines2) > 0 else ""
        acc = acc + line1 + line2 + '\n'
    return acc

def concat_multiline(*strs) -> str:
    """
    Horizontally concatenate multiline strings.
    """
    acc = ''
    for s in strs:
        acc = _concat_multiline(acc, s)
    return acc

def trim_str(s: str) -> str:
    """
    Remove all newline and trailing or leading space (' ') characters from `s`.
    """
    _s = s.replace('\n', '')
    return re.match(r"^[ ]*(.*)[ ]*$", _s).group(1)

def num_order_suffix(n: int) -> str:
    """
    Append the proper ordering suffix to numbers and return as strs.
    """
    if isinstance(n, int):
        _n = str(n)
        if _n[-1] == '1':
            return _n+"st"
        elif _n == '2':
            return _n+"nd"
        elif _n == '3':
            return _n+"rd"
        else:
            return _n+"th"
    else:
        return n

def author_preproc(authors: str) -> str:
    """
    Perform BibTeX-style processing on the string value of an `author` key
    contained in `source`.
    """
    names = list()
    for a in authors.split(" and "):
        name = [trim_str(x) for x in a.split(',')+[""]]
        name[1] = [trim_str(x) for x in name[1].split(' ')]
        names.append(name)
    acc = "".join([x[:1]+(len(x)>0)*". " for x in names[0][1]]) \
            + names[0][0]
    for name in names[1:-1]:
        acc += ", "+"".join([x[:1]+(len(x)>0)*". " for x in name[1]]) \
                + name[0]
    if len(names) > 1:
        acc += (len(names)>2)*", "+"and " \
                + "".join([x[:1]+(len(x)>0)*". " for x in names[-1][1]]) \
                + names[-1][0]
    acc = acc.replace("and others", "\\textit{et al.}")
    return acc

def page_preproc(pages: str) -> str:
    """
    Perform BibTeX-style processing on the string value of a `page` key.
    """
    if isinstance(pages, str):
        pagenums = pages.split('-')
        while "" in pagenums:
            pagenums.remove("")
        acc = ""
        for pagenum in pagenums:
            acc += pagenum+"--"
        return acc[:-2]
    else:
        return page_preproc(str(pages))

def article_doi_href_left(s: str) -> str:
    # assume DOI if not a complete URL
    pattern = re.compile("http[s]?://(.*)")
    if pattern.match(s):
        return "\\href{"+s+"}{"
    else:
        return "\\href{https://doi.org/"+s+"}{"

def article_doi_href_right(s: str) -> str:
    return "}"

FORMATS = {
    "article": CitationFormat("article",
        fields=[
            Field("author", "{},",
                aliases=["authors"],
                preproc=author_preproc),
            Field("title", "``{}.''"),
            Field("doi", "{}", optional=True,
                aliases=["DOI", "url", "URL"],
                preproc=article_doi_href_left,
                endspace=False),
            Field("journal", "{}"),
            Field("volume", "\\textbf{{{}}}",
                aliases=["vol"]),
            Field("issue", ", {}", optional=True,
               aliases=["number", "num"],
               rmchrs=1),
            Field("page", ", {}", optional=True,
                aliases=["pages", "p", "pp"],
                preproc=page_preproc,
                rmchrs=1),
            Field("year", "({})"),
            Field("doi", "{}", optional=True,
                aliases=["DOI", "url", "URL"],
                preproc=article_doi_href_right,
                rmchrs=1),
        ]
    ),
    "inproceedings": CitationFormat("inproceedings",
        fields=[
            Field("author", "{},",
                aliases=["authors"],
                preproc=author_preproc),
            #Field("title", "``{}.''"),
            Field("doi", "{}", optional=True,
                aliases=["DOI", "url", "URL"],
                preproc=article_doi_href_left,
                endspace=False),
            # Field("booktitle", "In \\textit{{{}}},"),
            Field("booktitle", "{}"),
            #Field("series", "{}"),
            Field("publisher", "({},"),
            Field("location", "{},",
                aliases=["address"]),
            Field("date", "{})",
                aliases=["year"]),
            Field("page", ", {}", optional=True,
                aliases=["pages", "p", "pp"],
                preproc=page_preproc,
                rmchrs=1),
            Field("doi", "{}", optional=True,
                aliases=["DOI", "url", "URL"],
                preproc=article_doi_href_right,
                rmchrs=1),
        ]
    ),
    "phdthesis": CitationFormat("phdthesis",
        fields=[
            Field("author", "{},",
                aliases=["authors"],
                preproc=author_preproc),
            Field("title", "``{}.''"),
            Field("intstitution", "Ph.D. diss., {},",
                aliases=["inst"]),
            Field("year", "{}"),
        ]
    ),
    "mastersthesis": CitationFormat("mastersthesis",
        fields=[
            Field("author", "{},",
                aliases=["authors"],
                preproc=author_preproc),
            Field("title", "``{}.''"),
            Field("intstitution", "master's thesis, {},",
                aliases=["inst"]),
            Field("year", "{}"),
        ]
    ),
    "book": CitationFormat("book",
        fields=[
            Field("author", "{},",
                aliases=["authors"],
                preproc=author_preproc),
            Field("title", "\\textit{{{}}}."),
            Field("edition", ", {} ed.", optional=True,
                aliases=["ed"],
                preproc=num_order_suffix,
                rmchrs=2),
            Field("publisher", "({},",
                aliases=["pub"]),
            Field("location", "{},",
                aliases=["address"]),
            Field("year", "{})"),
            Field("page", ", {}", optional=True,
                aliases=["pages", "p", "pp"],
                preproc=page_preproc,
                rmchrs=1),
        ]
    ),
    "inbook": CitationFormat("inbook",
        fields=[
            Field("author", "{},",
                aliases=["authors"],
                preproc=author_preproc),
            Field("title", "``{},''"),
            Field("booktitle", "in \\textit{{{}}},"),
            Field("volume", "Vol. {},", optional=True,
                aliases=["vol"]),
            Field("series", "of \\textit{{{}}},", optional=True),
            Field("page", "p. {}.",
                aliases=["pages", "p", "pp"],
                preproc=page_preproc),
            Field("publisher", "{},",
                aliases=["pub"]),
            Field("location", "{},", optional=True,
                aliases=["address"]),
            Field("year", "{}"),
        ]
    ),
    "newspaper": CitationFormat("newspaper",
        fields=[
            Field("author", "{},",
                aliases=["authors"],
                preproc=author_preproc),
            Field("title", "``{}.''"),
            Field("publisher", "\\textit{{{}}},",
                aliases=["pub"]),
            Field("date", "{}"),
            Field("page", ", {}", optional=True,
                aliases=["pages", "p", "pp"],
                rmchrs=1),
            Field("accessed", ", accessed {}", optional=True,
                aliases=["acc"],
                rmchrs=1),
        ]
    ),
    "magazine": CitationFormat("magazine",
        fields=[
            Field("author", "{},",
                aliases=["authors"],
                preproc=author_preproc),
            Field("title", "``{}.''"),
            Field("publisher", "\\textit{{{}}},",
                aliases=["pub"]),
            Field("date", "{}"),
            Field("page", ", {}", optional=True,
                aliases=["pages", "p", "pp"],
                rmchrs=1),
            Field("accessed", ", accessed {}", optional=True,
                aliases=["acc"],
                rmchrs=1),
        ]
    ),
    "website": CitationFormat("website",
        fields=[
            Field("author", "{},", optional=True,
                aliases=["authors"],
                preproc=author_preproc),
            Field("title", "``{}.''"),
            Field("publisher", ",'' \\textit{{{}}}.", optional=True,
                aliases=["pub"],
                rmchrs=4),
            Field("modified", "Last modified {}.", optional=True,
                aliases=["mod"]),
            Field("accessed", "Accessed {}.", optional=True,
                aliases=["acc"],
                rmchrs=2),
            Field("url", "\\url{{{}}}",
                aliases=["URL"]),
        ]
    )
}
FORMATS["conference"] = FORMATS["inproceedings"]

def print_formatting():
    for F in FORMATS.keys():
        print(FORMATS[F])

def format_types(O: dict):
    # expect dict[str : dict[str: T]] where T -> {int, float, str}
    return O
    for k1 in O.keys():
        for k2 in O[k1].keys():
            for T in [int, float, str]:
                try:
                    O[k1][k2] = T(O[k1][k2])
                    break
                except ValueError:
                    continue
                except Exception as e:
                    raise e
    return O

def load_sources_yaml(filename: str) -> dict:
    """
    Load the sources dict from `filename`, assumed to be a YAML-formatted file.
    """
    try:
        infile = open(filename, 'r')
        sources = pyaml.yaml.full_load(infile)
        infile.close()
    except Exception as e:
        raise e
    if config.verbose:
        print(f"Loaded {len(sources.keys())} source(s) from YAML file '{filename}':")
        for source in sources.keys():
            print("  "+source)
    return format_types(sources)

def load_sources_toml(filename: str) -> dict:
    """
    Load the sources dict from `filename`, assumed to be a TOML-formatted file.
    """
    try:
        sources = toml.load(filename)
    except Exception as e:
        raise e
    if config.verbose:
        print(f"Loaded {len(sources.keys())} source(s) from TOML file '{filename}':")
        for source in sources.keys():
            print("  "+source)
    return format_types(sources)

def load_sources_json(filename: str) -> dict:
    """
    Load the sources dict from `filename`, assumed to be a JSON-formatted file.
    """
    try:
        infile = open(filename, 'r')
        sources = json.load(infile)
        infile.close()
    except Exception as e:
        raise e
    if config.verbose:
        print(f"Loaded {len(sources.keys())} source(s) from JSON file '{filename}':")
        for source in sources.keys():
            print("  "+source)
    return format_types(sources)

def load_sources_ini(filename: str) -> dict:
    """
    Load the sources dict from `filename`, assumed to be a INI-formatted file.
    """
    try:
        sources = ini.ConfigParser()
        sources.read(filename)
        sources = dict(sources)
        for k in sources.keys():
            sources[k] = dict(sources[k])
    except Exception as e:
        raise e
    if config.verbose:
        print(f"Loaded {len(sources.keys())} source(s) from INI file '{filename}':")
        for source in sources.keys():
            print("  "+source)
    return format_types(sources)

def load_sources_bibtex(filename: str) -> dict:
    """
    Load the sources dict from `filename`, assumed to be a BibTeX-formatted file.
    """
    try:
        infile = open(filename, 'r')
        sources = bib.load(infile).entries_dict
        infile.close()
        for entry in sources:
            sources[entry]["type"] = sources[entry]["ENTRYTYPE"]
            del sources[entry]["ENTRYTYPE"]
            del sources[entry]["ID"]
    except Exception as e:
        raise e
    if config.verbose:
        print(f"Loaded {len(sources.keys())} source(s) from BibTeX file '{filename}':")
        for source in sources.keys():
            print("  "+source)
    return format_types(sources)

def load_sources(filename: str, file_format: str=None) -> dict:
    if file_format in ["yaml", "yml"]:
        return load_sources_yaml(filename)
    elif file_format in ["toml"]:
        return load_sources_toml(filename)
    elif file_format in ["json"]:
        return load_json_sources_json(filename)
    elif file_format in ["ini"]:
        return load_sources_ini(filename)
    elif file_format in ["bibtex", "bib"]:
        return load_sources_bibtex(filename)
    elif file_format == None:
        return load_sources(filename, path.splitext(filename)[1][:1])
    else:
        print(f"WARNING: file '{filename}' is of unexpected format; attempting to read as bibtex")
        return load_sources_bibtex(filename)

def dump_sources_yaml(sources: dict, outfilename: str):
    """
    Dump the sources dict to `outfilename` in YAML format.
    """
    try:
        outfile = open(outfilename, 'w')
        pyaml.dump(sources, outfile, safe=True, indent=4, vspacing=[1,1], sort_dicts=False)
        outfile.close()
    except Exception as e:
        raise e
    if config.verbose:
        print(f"Exported {len(sources.keys())} source(s) to YAML file '{outfilename}'")

def dump_sources_toml(sources: dict, outfilename: str):
    """
    Dump the sources dict to `outfilename` in TOML format.
    """
    try:
        outfile = open(outfilename, 'w')
        toml.dump(sources, outfile)
        outfile.close()
    except Exception as e:
        raise e
    if config.verbose:
        print(f"Exported {len(sources.keys())} source(s) to TOML file '{outfilename}'")

def dump_sources_json(sources: dict, outfilename: str):
    """
    Dump the sources dict to `outfilename` in JSON format.
    """
    try:
        outfile = open(outfilename, 'w')
        json.dump(sources, outfile, indent=4)
        outfile.close()
    except Exception as e:
        raise e
    if config.verbose:
        print(f"Exported {len(sources.keys())} source(s) to JSON file '{outfilename}'")

def dump_sources_ini(sources: dict, outfilename: str):
    """
    Dump the sources dict to `outfilename` in INI format.
    """
    try:
        outobj = ini.ConfigParser()
        outobj.read_dict(sources)
        outfile = open(outfilename, 'w')
        outobj.write(outfile)
        outfile.close()
    except Exception as e:
        raise e
    if config.verbose:
        print(f"Exported {len(sources.keys())} source(s) to INI file '{outfilename}'")

def dump_sources_bibtex(sources: dict, outfilename: str):
    """
    Dump the sources dict to `outfilename` in BibTeX format.
    """
    #try:
    #    outfile = open(outfilename, 'w')
    #    bib.dump(sources, outfile)
    #    outfile.close()
    #except Exception as e:
    #    raise e
    try:
        acc = ""
        for entryname in sources.keys():
            entry = sources[entryname]
            acc += "@"+entry["type"]+"{"+entryname+",\n"
            fieldnames = list(entry.keys())
            fieldnames.remove("type")
            for i, field in enumerate(fieldnames):
                acc += "    "+field+" = "
                for T in [int, float, str]:
                    try:
                        fieldval = T(entry[field])
                        break
                    except ValueError:
                        continue
                    except Exception as e:
                        raise e
                acc += "{"+fieldval+"}" if isinstance(fieldval, str) else str(fieldval)
                acc += "\n" if i == len(fieldnames)-1 else ",\n"
            acc += "}\n\n"
    except Exception as e:
        raise e
    outfile = open(outfilename, 'w')
    outfile.write(acc)
    outfile.close()
    if config.verbose:
        print(f"Exported {len(sources.keys())} source(s) to BibTeX file '{outfilename}'")

def dump_sources(sources: dict, outfilename: str, file_format: str=None):
    if file_format in ["yaml", "yml"]:
        dump_sources_yaml(sources, outfilename)
    elif file_format in ["toml"]:
        dump_sources_toml(sources, outfilename)
    elif file_format in ["json"]:
        dump_json_sources_json(sources, outfilename)
    elif file_format in ["ini"]:
        dump_sources_ini(sources, outfilename)
    elif file_format in ["bibtex", "bib"]:
        dump_sources_bibtex(sources, outfilename)
    elif file_format == None:
        dump_sources(sources, outfilename, path.splitext(outfilename)[1][1:])
    else:
        print(f"WARNING: file '{filename}' is of unexpected format; export as bibtex")
        dump_sources_bibtex(sources, outfilename)

def read_aux_file(filename: str, sources: dict) -> (list, list):
    """
    Determine which references are used and in what order from a LaTeX aux file.
    Also reports references which are undefined in the original sources file.
    """
    try:
        infile = open(config.aux_file, 'r')
        lines = infile.readlines()
        infile.close()
    except Exception as e:
        raise e
    good_refs = list()
    bad_refs = list()
    pattern = re.compile("^\\\citation{(.*)}$")
    matches = filter(lambda x: x is not None, map(lambda x: pattern.match(x), lines))
    for match in matches:
        refs = match.group(1).split(',')
        for ref in refs:
            if ref in sources.keys():
                if ref not in good_refs:
                    good_refs.append(ref)
            else:
                bad_refs.append(ref)
    if config.verbose:
        print(f"Found {len(good_refs)} reference(s) in '{filename}':")
        for ref in good_refs:
            print("  "+ref)
    return (good_refs, bad_refs)

def read_tex_file(
    filename: str,
    sources: dict,
    follow_links: bool=False,
    indent: str=""
) -> (list, list):
    """
    Determine which references are used and in what order from LaTeX tex file.
    Also reports references which are undefined in the original sources file.
    """
    try:
        infile = open(filename, 'r')
        content = infile.read()
        infile.close()
    except Exception as e:
        raise e
    good_refs = list()
    bad_refs = list()
    pattern = re.compile("\\\(cite|input){(.*?)}", re.DOTALL)
    matches = [x for x in pattern.findall(content) if len(x) > 0]
    for match in matches:
        print(match)
        if match[0] == "cite":
            refs = [r.strip() for r in match[1].split(",")]
            for ref in refs:
                if ref in sources.keys():
                    if ref not in good_refs:
                        good_refs.append(ref)
                else:
                    bad_refs.append(ref)
        elif match[0] == "input" and follow_links:
            if path.splitext(match[1])[1] == ".tex":
                if config.verbose:
                    print(f"{indent}Following \\input link {filename.split('/')[-1]} -> {match[1]}:")
                _good_refs, _bad_refs = read_tex_file(match[1], sources, follow_links, indent + "  ")
                good_refs += [r for r in _good_refs if r not in good_refs]
                bad_refs += _bad_refs
            elif not config.quiet:
                print(f"{indent}WATNING: found bad \\input link {filename.split('/')[1]} -> {match[1]}.")
    if config.verbose:
        print(f"{indent}Found {len(good_refs)} sourceable reference(s) in '{filename}':")
        for ref in good_refs:
            print(f"{indent}  " + ref)
    return (good_refs, bad_refs)

def generate_bib_entry(ref: str, source: dict, cite_format: CitationFormat,
        w: int=80) -> str:
    """
    Generate a full, formatted \\bibitem entry with label `ref` and properties
    contained in source dict `source`. Each line of the \\bibitem entry body is
    at most `w` characters wide.
    """
    t = "    "
    acc = t+"\\bibitem{"+ref+"}\n"
    citation = cite_format(source, ref)

    citation = breaklines(citation, w - 2*len(t))
    padding = (len(citation.split('\n'))*(2*t+'\n'))[:-1]
    acc += concat_multiline(padding, citation) + ''
    return acc

def generate_thebibliography(refs: list, sources: dict, cite_formats: dict) \
        -> str:
    acc = "\\begin{thebibliography}{"+str(len(refs))+"}\n"
    acc += "    \\setlength{\\parskip}{0mm}\n"
    acc += "    \\small\n\n"
    for ref in refs:
        if "type" not in sources[ref].keys():
            if not config.quiet:
                print(f"WARNING: 'type' for reference '{ref}' is undefined; assume 'article'.")
            T = "article"
        else:
            T = sources[ref]["type"]
        if T not in cite_formats:
            if not config.quiet:
                print(f"WARNING: type '{T}' for reference '{ref}' is undefined; interpret as 'article'.")
            T = "article"
        acc += generate_bib_entry(ref, sources[ref], cite_formats[T])
    acc += "\\end{thebibliography}"
    if config.verbose:
        print(f"Generated bibliography of {len(refs)} entries.")
    return acc
    
def main(argv):
    if not (
        config.available_methods["yaml"]
            or config.available_methods["json"]
            or config.available_methods["bibtex"]
            or config.available_methods["ini"]
        ):
        print("FATAL: could not load necessary parsing modules.")
        return 1

    shortopts = 'a:d:o:lytjibEYTJIBfqvh'
    longopts = [
        "aux=",
        "doc=",
        "output=",
        "follow-links",
        "yaml",
        "toml",
        "json",
        "ini",
        "bibtex",
        "export-yaml",
        "export-toml",
        "export-json",
        "export-ini",
        "export-bibtex",
        "export",
        "formatting",
        "quiet",
        "verbose",
        "help"
    ]
    try:
        opts, args = getopt.gnu_getopt(argv[1:], shortopts, longopts)
        for opt, optarg in opts:
            if opt in ["-a", "--aux"]:
                config.aux_file = str(optarg)
                config.tex_file = None
            elif opt in ["-d", "--doc"]:
                config.tex_file = str(optarg)
                config.aux_file = None
            elif opt in ["-o", "--output"]:
                config.output = optarg
            elif opt in ["-l", "--follow-links"]:
                config.follow_links = True
            elif opt in ["-y", "--yaml"]:
                config.source_format = "yaml"
            elif opt in ["-t", "--toml"]:
                config.source_format = "toml"
            elif opt in ["-j", "--json"]:
                config.source_format = "json"
            elif opt in ["-i", "--ini"]:
                config.source_format = "ini"
            elif opt in ["-b", "--bibtex"]:
                config.source_format = "bib"
            elif opt in ["-E", "--export"]:
                config.export = True
            elif opt in ["-Y", "--export-yaml"]:
                config.export_format = "yaml"
            elif opt in ["-T", "--export-toml"]:
                config.export_format = "toml"
            elif opt in ["-J", "--export-json"]:
                config.export_format = "json"
            elif opt in ["-I", "--export-ini"]:
                config.export_format = "ini"
            elif opt in ["-B", "--export-bibtex"]:
                config.export_format = "bib"
            elif opt in ["-f", "--formatting"]:
                print_formatting()
                return 0
            elif opt in ["-q", "--quiet"]:
                config.quiet = True
                config.verbose = False
            elif opt in ["-v", "--verbose"]:
                config.verbose = True
                config.quiet = False
            elif opt in ["-h", "--help"]:
                print(helptext)
                return 0
        if len(args) == 0:
            print("Lacking bibliography file(s).")
            print(helptext)
            return 1
    except Exception as e:
        print(f"Exception occurred while parsing commandline args:\n{e}")
        return 1

    if not config.available_methods[config.source_format]:
        print("FATAL: could not load the desired parsing module.")
        return 1
    
    ### load all sources ###
    try:
        if config.source_format is None:
            sources = dict()
            for source_file in args:
                ext = path.splitext(source_file)[1][1:]
                if config.available_methods[ext]:
                    new_source = load_sources(source_file, ext)
                    sources.update(new_source)
                else:
                    if not config.quiet:
                        print(f"WARNING: could not parse file `{source_file}`. Skipping...")
                    continue
        else:
            sources = dict()
            for source_file in args:
                new_source = load_sources(source_file, config.source_format)
                sources.update(new_source)
        for entryname in sources.keys():
            for fieldname in sources[entryname]:
                if isinstance(sources[entryname][fieldname], str):
                    sources[entryname][fieldname] = trim_str(sources[entryname][fieldname])
    except Exception as e:
        print(f"FATAL: Exception occurred while attempting to read `{source_file}`:\n{e}")
        raise e
        return 1

    ### perform export if in export mode ###
    if config.export:
        if config.available_methods[config.source_format]:
            outfile = path.splitext(args[0])[0]+"."+config.export_format if not config.output else config.output
            dump_sources(sources, outfile, config.export_format)
        else:
            print(f"FATAL: could not load modules to export in mode {config.source_format}")
            return 1
        return 0

    ### sort sources based on tex or aux file (or just alphabetically) ###
    # maybe there's a better way to do this, without regexes
    # processing via aux or tex file has the added benefit of including only the
    # references that actually get used in the main tex file
    try:
        if config.aux_file:
            refs, bad_refs = read_aux_file(config.aux_file, sources)
        elif config.tex_file:
            refs, bad_refs = read_tex_file(config.tex_file, sources, config.follow_links)
        else:
            refs = list(sources.keys())
            refs.sort()
            bad_refs = list()
    except Exception as e:
        print(f"Exception occurred while attempting to read file:\n{e}")
        raise e
        return 1
    if not config.quiet:
        for bad_ref in bad_refs:
            print(f"WARNING: ref `{bad_ref}` undefined in bibliography file.")

    ### generate output ###
    outstr = generate_thebibliography(refs, sources, FORMATS)

    ### write to file ###
    bibfile = path.splitext(args[0])[0]+".tex" if not config.output else config.output
    outfile = open(bibfile, 'w')
    outfile.write(outstr)
    outfile.close()
    if config.verbose:
        print(f"Output written to '{bibfile}'.")

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))


