\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{whooie}

%%%%%%%%%%%%%%%%%%%%%%%
%%% package options %%%
%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{kvoptions}
\SetupKeyvalOptions{
    family=whooie,
    prefix=whooie@
}
\DeclareOption{document}{
    \let\asdocument = 1
    \let\ashomework = 0
    \let\asbeamer = 0
}
\DeclareOption{homework}{
    \let\asdocument = 1
    \let\ashomework = 1
    \let\asbeamer = 0
}
\DeclareOption{beamer}{
    \let\asdocument = 0
    \let\ashomework = 0
    \let\asbeamer = 1
}
\DeclareStringOption[11pt]{fontsize}[11pt]
\DeclareStringOption[169]{aspectratio}[169]
\DeclareStringOption[0.8in]{margins}[0.8in]
\DeclareStringOption[8.5in]{docwidth}[8.5in]
\DeclareStringOption[11in]{docheight}[11in]
\DeclareOption{sans-serif}{\let\sansserif = 1}
\DeclareOption{mlmodern}{\let\mlmodern = 1}
\DeclareOption{usetikz}{\let\usetikz = 1}
\DeclareOption{twocolumn}{\let\usetwocolumn = 1}
\DeclareStringOption[0]{cjk}[0]
\DeclareOption*{\PackageWarning{whooie}{Unknown option '\CurrentOption'}}

\DeclareOption{usecjk}{\let\usecjk = 1}
\DeclareOption{useja}{
    \let\usecjk = 1
    \let\useja = 1
}
\DeclareOption{usech-simp}{
    \let\usecjk = 1
    \let\usechsimp = 1
}
\DeclareOption{usech-trad}{
    \let\usecjk = 1
    \let\usechtrad = 1
}
\DeclareOption{chapters}{\let\chapters = 1}
\DeclareOption{sectioneq}{\let\sectioneq = 1}
\DeclareOption{supercite}{\let\supercite = 1}
\ExecuteOptions{document}
\ProcessKeyvalOptions*
\ProcessOptions
\relax

%%%%%%%%%%%%%%%%%%%%%
%%% load packages %%%
%%%%%%%%%%%%%%%%%%%%%

\ifx\asdocument 1
    \ifx\usetwocolumn
        \LoadClass[
            hidelinks,
            \whooie@fontsize,
            titlepage,
            twocolumn
        ]{article}
    \else
        \LoadClass[
            hidelinks,
            \whooie@fontsize,
            titlepage
        ]{article}
    \fi
    \RequirePackage[
        width=\whooie@docwidth,
        height=\whooie@docheight,
        left=\whooie@margins,
        right=\whooie@margins,
        top=\whooie@margins,
        bottom=\whooie@margins
    ]{geometry}
    %\RequirePackage{verse} % poem spacing
    \RequirePackage{xparse}  % | more intelligent commands
    \RequirePackage{xifthen} % |
    \RequirePackage{calc} % add length objects
    \RequirePackage{float} % advanced float placement
    \RequirePackage{pbox} % used by \acom
    \RequirePackage{sectsty} % (re-)style *section commands
    \RequirePackage{titlesec} % custom section titles
    \RequirePackage[ % | rotate figures/tables
        figuresleft  % |
    ]{rotating}      % |
    \RequirePackage[ % | prevent widows
        all          % |
    ]{nowidow}       % |
    %\RequirePackage{ragged2e} % provide commands for ragged text
    \RequirePackage{bold-extra} % bold smallcaps and typewriter fonts
    \RequirePackage{hyperref}
    \RequirePackage{setspace} % adjust line spacings
\fi
\ifx\ashomework 1
    \RequirePackage{fancyhdr} % fancy header/footer styling
\fi
\ifx\asbeamer 1
    \LoadClass[
        hidelinks,
        \whooie@fontsize,
        compress,
        xcolor=dvipsnames,
        aspectratio=\whooie@aspectratio,
        t
    ]{beamer}
    \RequirePackage[
        retainorgcmds
    ]{IEEEtrantools}
    \RequirePackage{pgfplots}
    \RequirePackage{hyperref}
    % have to do some hacing when setspace is loaded because it messes with the
    % definition of \@footnotetext
    % save \@footnotetext
    \makeatletter
    \let\BEAMER@footnotetext\@footnotetext
    \makeatother
    % load the offending package
    \RequirePackage{setspace}
    % restore \@footnotetext
    \makeatletter
    \let\@footnotetext\BEAMER@footnotetext
    % patch the relevant command to do single spacing in footnotes
    \expandafter\patchcmd\csname beamerx@\string\beamer@framefootnotetext\endcsname
      {\reset@font}
      {\def\baselinestretch{\setspace@singlespace}\reset@font}
      {}{}
    \makeatother
\fi
\ifx\sansserif 1
    \RequirePackage{sfmath} % sans-serif math
    \renewcommand{\familydefault}{\sfdefault}
\fi
\ifx\mlmodern 1
    \RequirePackage{mlmodern}
    \RequirePackage[
        T1
    ]{fontenc}
\fi
\RequirePackage{pgffor} % for loops
\RequirePackage{chngcntr} % relationships between counters
\RequirePackage{graphicx} % include images
\RequirePackage{import}      % | pdf_tex graphics
\RequirePackage{xifthen}     % |
\RequirePackage{pdfpages}    % |
\RequirePackage{transparent} % |
\RequirePackage[ % | don't break lines within a citation
    nobreak      % |
]{cite}          % |
\RequirePackage[ % | various emphasis styles
    normalem     % |
]{ulem}          % |
\RequirePackage{tabularx} % extra options for tables
\RequirePackage{booktabs} % better lining options in tables
\RequirePackage{multirow} % | entries covering multiple rows
\RequirePackage{bigdelim} % | + additional marking tools
\RequirePackage{makecell} % automatic styling tools in tables
\RequirePackage{colortbl} % table coloring commands
\RequirePackage[      % | style of figure/table captions
    format=default,   % |
    labelfont=bf,     % |
    font=footnotesize % |
]{caption}            % |
\RequirePackage[      % |
    format=default,   % |
    labelfont=bf,     % |
    font=footnotesize % |
]{subcaption}         % |
%\RequirePackage{stackengine} % math symbol decoration tools
\RequirePackage{latexsym}  % | various mathematical symbols and tools
\RequirePackage{amsmath}   % |
\RequirePackage{amssymb}   % |
\RequirePackage{amsfonts}  % |
\RequirePackage{amsthm}    % |
\RequirePackage{mathtools} % |
\RequirePackage{bm} % bold math
\RequirePackage{esint} % more integral signs
\RequirePackage{xfrac} % more fraction shapes
\RequirePackage{physics} % very useful math macros
\RequirePackage{tensor} % tensor indices with proper spacing
\RequirePackage{slashed} % Feynman slash notation
\RequirePackage{accents} % text decoration; has to be loaded after physics
%\RequirePackage[ % pseudocode
%    tworuled,vlined
%]{algorithm2e}
\RequirePackage{siunitx} % macros for physical units
\RequirePackage{listings} % code blocks
\RequirePackage[        % | enumerate/itemize settings
    shortlabels,        % |
]{enumitem}             % |
%\RequirePackage{pgf}
\setenumerate{leftmargin=1cm,rightmargin=0.375cm}
\ifx\supercite 1
    \PassOptionsToPackage{super}{cite}
\fi
\ifx\usetikz 1
    \RequirePackage{tikz} % make drawings
    \RequirePackage{pgfplots}      % | plotting (axis environment);
    \RequirePackage{pgfplotstable} % | requires tikz
    \RequirePackage[ % draw circuits; requires tikz
        american,
        cute inductors,
        siunitx
    ]{circuitikz}
    \RequirePackage{chemfig} % draw chemical figures; requires tikz
\fi
\ifx\whooie@cjk chtrad
    \RequirePackage{xeCJK}
    \setCJKmainfont{Source ARPL UMing HK}
    \setCJKsansfont{Sarasa Gothic CL}
\fi
\ifx\whooie@cjk ja
    \RequirePackage{xeCJK}
    \RequirePackage{ruby}
    \setCJKmainfont{IPAMincho}
    \setCJKsansfont{IPAGothic}
\fi
\ifx\whooie@cjk chsimp
    \RequirePackage{xeCJK}
    \setCJKmainfont{Source Han Serif CN}
    \setCJKsansfont{Source Han Sans CH}
\fi
\RequirePackage{cleveref} % clever in-text references; has to be loaded last

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% formatting (re)definitions %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\frenchspacing
\ifx\asdocument 1
    \hypersetup{
        colorlinks,
        %backref=page,
        %hyperindex,
        %hyperfootnotes,
        linktoc=page,
        allcolors=blue
    }
    \setlength{\parskip}{0.25em}
    \setlength{\itemsep}{0.0em}
    \renewcommand*{\arraystretch}{1.2} % spacing between lines in tables
    \sectionfont{\Large}        % | sectsty
    \subsectionfont{\large}     % |
    \subsubsectionfont{\large}  % |
    \setcounter{secnumdepth}{5} % |
    \setcounter{tocdepth}{5}    % |
    \ifx\sectioneq 1
        \numberwithin{equation}{section}
        \counterwithin*{equation}{section}
    \fi
    \renewcommand{\refname}{\hspace{0.25\textwidth}\rule{0.5\textwidth}{1pt}\hfill}
    \renewcommand{\cellalign}{tl}          % | makecell
    \renewcommand{\theadalign}{cl}         % |
    \renewcommand{\theadfont}{\normalsize} % |
    \titleclass{\Lecture}{straight}[\section]                             % | cleveref, titlesec
    \newcounter{Lecture}[section]                                         % | define a section-style
    \titleformat{\Lecture}{\Large\bfseries}{}{0em}{Lecture~\#\theLecture} % | heading for lecture
    \titlespacing*{\Lecture}{0pt}{3.5ex plus 1ex}{1.5ex plus 1ex}         % | notes
    \crefalias{lecture}{lecture}                                          % |
    \crefname{lecture}{lecture}{lectures}                                 % |
    \Crefname{lecture}{Lecture}{Lectures}                                 % |
    \makeatletter                                                         % |
    \def\@lecture{}                                                       % |
    \newcommand{\lecture}[2]{                                             % |
        \setcounter{Lecture}{#1}                                          % |
        \addtocounter{Lecture}{-1}                                        % |
        \def\@lecture{Lecture \#\theLecture}                              % |
        \Lecture{~~---~~#2}\label{lec#1}                                  % |
    }                                                                     % |
    \makeatother                                                          % |
\fi
\ifx\chapters 1                                 % | style
    \crefname{section}{chapter}{chapters}       % | redefine in-text labeling of
    \Crefname{section}{Chapter}{Chapters}       % | *sections for chapters
    \crefname{subsection}{section}{sections}    % |
    \Crefname{subsection}{Section}{Sections}    % |
    \crefname{subsubsection}{section}{sections} % |
    \Crefname{subsubsection}{Section}{Sections} % |
\fi                                             % |
\ifx\ashomework 1
    \newcounter{pproblem}
    \newenvironment{pproblem}[1][]{\refstepcounter{pproblem}\par\smallskip\noindent
        {\normalsize \textfb{Problem~\thepproblem{#1}}} \par\smallskip}{\medskip}
    \newcounter{ppart}
    \newenvironment{ppart}[1][]{\refstepcounter{ppart}\par\smallskip\noindent
        {\normalsize \textfb{Part~\theppart{#1}}} \par\smallskip}{\medskip}
    \newcounter{qquestion}
    \newenvironment{qquestion}[1][]{\refstepcounter{qquestion}\par\smallskip\noindent
        {\normalsize \textbf{Question~\theqquestion{#1}}} \par\smallskip}{\medskip}
    \pagestyle{fancy}
    \setlength{\headheight}{30pt}
    \setlength{\footskip}{20pt}
    \setlength{\headsep}{0.5cm}
    \fancyhf{}
    \NewDocumentCommand{\hwhead}{m m m O{1pt}}{
        \lhead{{#1}}\chead{{#2}}\rhead{{#3}}\cfoot{\thepage}
        \title{{#1} {#2}}\author{{#3}}\date{}
        \renewcommand{\headrulewidth}{#4}
    }
    \NewDocumentCommand{\lcrhead}{m m m O{1pt}}{
        \lhead{{#1}}\chead{{#2}}\rhead{{#3}}\cfoot{\thepage}
        \renewcommand{\headrulewidth}{#4}
    }
\fi
\ifx\asbeamer 1
    \usetheme{metropolis}
    \setbeamerfont{section title}{size=\Large}
    \setbeamerfont{section page}{size=\large}
    \makeatletter
    \setlength{\metropolis@titleseparator@linewidth}{1.5pt}
    \setlength{\metropolis@progressonsectionpage@linewidth}{1.5pt}
    \setlength{\metropolis@progressinheadfoot@linewidth}{1.5pt}
    \makeatother
    \metroset{
        titleformat=smallcaps, % regular, smallcaps, allsmallcaps, allcaps
        titleformat plain=regular, % regular, smallcaps, allsmallcaps, allcaps
        sectionpage=progressbar, % none, simple, progressbar
        subsectionpage=none, % none, simple, progressbar
        numbering=fraction, % none, counter, fraction
        progressbar=frametitle, % none, head, frametitle, foot
        block=fill, % transparent fill
        background=light, % dark, light
    }
    \renewcommand{\mathrm}[1]{\mathsf{#1}}
    \setbeamercolor{background canvas}{bg=white}
    \definecolor{mDarkBlueGray}{HTML}{232d39}
    \definecolor{mLightBlueGray}{HTML}{3c506b}
    \definecolor{mCharcoal}{HTML}{101012}
    \definecolor{mDarkGray}{HTML}{888888}
    \definecolor{mLightGray}{HTML}{c0c0c0}
    \definecolor{mDarkOrange}{HTML}{d87100}
    \definecolor{mLightOrange}{HTML}{eb811b}
    \definecolor{mExample}{HTML}{36405c}
    \definecolor{whooieBlue}{HTML}{1f77b4}
    \definecolor{whooieAuburn}{HTML}{d95319}
    \definecolor{whooieCanary}{HTML}{edb120}
    \definecolor{whooiePurple}{HTML}{7d2f8e}
    \definecolor{whooieCyan}{HTML}{46add9}
    \definecolor{whooieTangerine}{HTML}{ff7f0e}
    \definecolor{whooieForest}{HTML}{21613a}
    \definecolor{whooieBurgundy}{HTML}{a2142f}
    \definecolor{whooieGray}{HTML}{505050}
    \definecolor{whooieRed}{HTML}{d62728}
    \definecolor{MetroSectionHeadOutBG}{HTML}{E5E6E7}
    \definecolor{MetroSectionHeadInBG}{HTML}{CFD1D4}
    \setbeamercolor{MetroSectionHeadOut}{bg=MetroSectionHeadOutBG}
    \setbeamercolor{MetroSectionHeadIn}{bg=MetroSectionHeadInBG}
    \setbeamercolor{normal text}{
        fg=black!2,
        bg=mDarkBlueGray
    }
    \usebeamercolor[fg]{normal text}
    \setbeamercolor{normal text}{
        fg=mDarkBlueGray,
        bg=black!2
    }
    \setbeamercolor{alerted text}{
        fg=mLightOrange
    }
    \setbeamercolor{example text}{
        fg=mLightBlueGray
    }
    \setbeamercolor{progress bar}{
        fg=mLightBlueGray,
        bg=mCharcoal!30
    }
    \setbeamercolor{titlelike}{use=normal text, parent=normal text}
    \setbeamercolor{author}{use=normal text, parent=normal text}
    \setbeamercolor{date}{use=normal text, parent=normal text}
    \setbeamercolor{institute}{use=normal text, parent=normal text}
    \setbeamercolor{structure}{use=normal text, fg=normal text.fg}
    \setbeamercolor{palette primary}{%
        use=normal text,
        fg=normal text.bg,
        bg=normal text.fg
    }
    \setbeamercolor{block title}{%
        use=normal text,
        fg=normal text.fg,
        bg=
    }
    \setbeamercolor{block title}{%
        use=normal text,
        fg=normal text.fg,
        bg=normal text.bg!80!fg
    }
    \setbeamercolor{block body}{
        use={block title, normal text},
        bg=block title.bg!50!normal text.bg
    }
    \setbeamercolor{footnote}{fg=normal text.fg!90}
    \setbeamerfont{footnote}{size=\tiny}
    \setbeamerfont{frametitle}{size=\Large}
    \makeatletter
    \setlength{\metropolis@frametitle@padding}{1.5ex} % <- default 2.2ex
    \setbeamertemplate{footline}{
        \begin{beamercolorbox}[wd=\textwidth, sep=1.2ex]{footline} % <- default 3ex
            \usebeamerfont{page number in head/foot}
            \usebeamertemplate*{frame footer}
            \hfill
            \usebeamertemplate*{frame numbering}
        \end{beamercolorbox}
    }
    \makeatother
    \pgfplotscreateplotcyclelist{mbarplot cycle}{
        {draw=whooieBlue,       fill=whooieBlue!70},
        {draw=whooieAuburn,     fill=whooieAuburn!70},
        {draw=whooieCanary,     fill=whooieCanary!70},
        {draw=whooiePurple,     fill=whooiePurple!70},
        {draw=whooieCyan,       fill=whooieCyan!70},
        {draw=whooieTangerine,  fill=whooieTangerine!70},
        {draw=whooieForest,     fill=whooieForest!70},
        {draw=whooieBurgundy,   fill=whooieBurgundy!70},
        {draw=whooieGray,       fill=whooieGray!70},
        {draw=whooieRed,        fill=whooieRed!70}
    }
    \pgfplotscreateplotcyclelist{mlineplot cycle}{
        {whooieBlue,    mark=*,         mark size=1.5pt},
        {whooieAuburn,  mark=square*,   mark size=1.3pt},
        {whooieCanary,  mark=triangle*, mark size=1.5pt},
        {whooiePurple,  mark=diamond*,  mark size=1.5pt}
    }
    \setbeamertemplate{bibliography item}[text]
    \setbeamertemplate{itemize items}{\textbullet}
    \hypersetup{
        bookmarks=true,
        colorlinks,
        backref=page,
        hyperindex,
        hyperfootnotes,
        linktoc=page,
        linkcolor=mLightBlueGray,
        menucolor=mLightBlueGray,
        runcolor=blue,
        urlcolor=blue,
        citecolor=red
    }
    \setenumerate{leftmargin=0.8cm,rightmargin=0cm}
\fi
\ifx\sansserif 1
    \renewcommand{\familydefault}{\sfdefault}
\fi
\makeatletter
\let\@@magyar@captionfix\relax % caption
\makeatother
\ifx\usetikz 1
    \usetikzlibrary{decorations.markings,
                    decorations.pathmorphing,
                    patterns,
                    shapes.geometric,
                    arrows,
                    positioning,
                    calc,
    }
    %\draw[decoration={aspect=0.3, segment length=3mm, amplitude=3mm,coil},decorate]
    \tikzset{ % labels for \draw, shapes for flowcharts, arrows for general use
        label/.style  args={at #1 #2  with #3}{
            postaction={
                decorate,
                decoration={
                    markings,
                    mark=at position #1 with \node [#2] {#3};
                }
            }
        },
        midarrow/.style={
            postaction={
                decorate,
                decoration={
                    markings,
                    mark=at position 0.5 with {\arrow{stealth}};
                }
            }
        },
        spring/.style={
            thick,
            decorate,
            decoration={
                zigzag,
                pre length=0.5cm,
                post length=0.5cm,
                segment length=5
            }
        },
        flowbox/.style={
            minimum width=3cm,
            minimum height=1cm,
            text centered,
            draw=black
        },
        startstop/.style={
            rectangle,
            rounded corners,
            flowbox,
            fill=red!30
        },
        io/.style={
            trapezium,
            trapezium left angle=70,
            trapezium right angle=110,
            flowbox,
            fill=blue!30
        },
        process/.style={
            rectangle,
            flowbox,
            fill=orange!30
        },
        decision/.style={
            diamond,
            flowbox,
            fill=yellow!30
        },
        flowarrow/.style={
            thick,
            ->,
            >=stealth
        },
    }
    \usepackage{pgfplots} % plotting (axis environment), requires tikz
    \usepackage{pgfplotstable} % enable pgfplots to read external textfiles for data
    \pgfplotsset{ % settings for most plots
        width={\textwidth},
        height={0.75\textwidth},
        trig format plots=rad,
        defaultsty/.style={ % invoke defaultsty for most plots
            axis lines={left},
            axis line style={->},
            legend style={font=\footnotesize},
            legend cell align={left},
            grid={both},
            grid style={line width=0.1pt, draw=gray!10},
            major grid style={line width=0.2pt, draw=gray!50},
            minor tick num={5},
            xticklabel style={font=\footnotesize, fill=white},
            yticklabel style={font=\footnotesize, xshift=-0.5ex, fill=white},
        },
        boxsty/.style={ % for scientific plots
            axis line style={-},
            legend style={font=\footnotesize},
            legend cell align={left},
            grid={both},
            grid style={line width=0.1pt, draw=gray!10},
            major grid style={line width=0.2pt, draw=gray!50},
            minor tick num={5},
            xticklabel style={font=\footnotesize, fill=white},
            yticklabel style={font=\footnotesize, xshift=-0.5ex, fill=white},
        },
        planesty/.style={ % invoke planesty for plots on a full 2D plane
            width={0.75\textwidth},
            %height={0.8\textwidth},
            axis lines={middle},
            axis line style={<->},
            axis equal,
            legend style={font=\footnotesize},
            legend cell align={left},
            grid={both},
            grid style={line width=0.1pt, draw=gray!10},
            major grid style={line width=0.2pt, draw=gray!50},
            minor tick num={5},
            xticklabel style={font=\footnotesize, yshift=0.5ex, fill=white},
            yticklabel style={font=\footnotesize, xshift=0.5ex, fill=white},
        },
        parametricsty/.style={ % for parametric plots
            parametric={true},
            variable={t},
        },
        errorsty/.style={ % invoke for when you need to add errors in the plot
            error bars/y dir={both},
            error bars/y explicit,
        },
    }
    \renewcommand*{\printatom}[1]{{\sffamily\cf{#1}}} % for sans serif Lewis diagrams
    \newcommand{\chemcharge}[1]{\chemmove{\node[] at (0pt,50pt) {\footnotesize ${#1}$};}} % Lewis charges
\fi
\lstset{                                 % | listings
    frame=tblr,                          % |
    belowskip=3mm,                       % |
    showstringspaces=false,              % |
    breaklines=true,                     % |
    breakatwhitespace=true,              % |
    numbers=left,                        % |
    numberstyle=\tiny\color{gray},       % |
    rulecolor=\color{black},             % |
    basicstyle={\footnotesize\ttfamily}, % |
    commentstyle=\itshape\color{gray},   % |
    keywordstyle=\color{red},            % |
    stringstyle=\color{orange},          % |
    breaklines=true,                     % |
    breakatwhitespace=true,              % |
    tabsize=4,                           % |
    xleftmargin=0.5\parindent,           % |
    xrightmargin=0.5\parindent,          % |
}                                        % |
%\stackMath % stackengine
\MHInternalSyntaxOn                   % | ams*, mathtools;
\renewcommand{\dcases}{               % | use displaystyle math in cases
    \MT_start_cases:nnnn              % | environment
        {\quad}                       % |
        {$\m@th\displaystyle##$\hfil} % |
        {$\m@th\displaystyle##$\hfil} % |
        {\lbrace}                     % |
}                                     % |
\MHInternalSyntaxOff                  % |
\renewcommand{\qedsymbol}{$\blacksquare$} % ams*
\makeatletter                                      % | ams*, mathtools;
\renewcommand*\env@matrix[1][*\c@MaxMatrixCols c]{ % | extend amsmath matrices
    \hskip -\arraycolsep                           % | to accept column types
    \let\@ifnextchar\new@ifnextchar                % | and separators
    \array{#1}                                     % |
}                                                  % |
\makeatother                                       % |
\creflabelformat{equation}{#2#1#3}              % | cleveref
\crefrangelabelformat{equation}{#3#1#4--#5#2#6} % | in-text equation reference

%%%%%%%%%%%%%%
%%% macros %%%
%%%%%%%%%%%%%%

%%% general %%%
\let\svthefootnote\thefootnote
\newcommand\blankfootnote[1]{
    \let\thefootnote\relax\footnotetext{#1}
    \let\thefootnote\svthefootnote
}
\let\svfootnote\footnote
\renewcommand\footnote[2][?]{
    \if\relax#1\relax
        \blankfootnote{#2}
    \else
        \if?#1\svfootnote{#2}\else\svfootnote[#1]{#2}\fi
    \fi
}
\NewDocumentEnvironment{hangingindent}{O{\parindent}O{0cm}} % hanging indent environment
    {\setlength{\leftskip}{ #1+#2 }\setlength{\parindent}{- #1 }}
    {\par}
\NewDocumentCommand{\withhangingindent}{O{\parindent}O{0cm}}{ % hanging indent command
    \setlength{\leftskip}{ #1+#2 }\setlength{\parindent}{- #1}
    \titlespacing{\section}{0mm}{0.6cm}{0.4cm}
    \setlength{\parskip}{0.5em}
}
\newcommand{\tsb}[1]{\textsubscript{{#1}}} % short text subscript and superscript
\newcommand{\tsp}[1]{\textsuperscript{{#1}}}
\newcommand{\textul}[1]{\underline{#1}}
\newcommand{\textem}[1]{\emph{$1}}
\newcommand{\textbi}[1]{\textbf{\textit{#1}}}
\renewcommand{\t}[1]{\text{{#1}}} % \text in fewer characters
\newcommand{\ph}[1]{\phantom{{#1}}}
\newcommand{\fns}{\footnotesize}
\makeatletter % alternate title commands
\newcommand{\umaketitle}{
    \thispagestyle{plain}
    \begin{center}
        \LARGE\@title \\\vspace{0.5cm}
        \large\@author \vspace{0.5cm}
    \end{center}
}
\newcommand{\udmaketitle}{
    \thispagestyle{plain}
    \begin{center}
        \LARGE\@title \\\vspace{0.5cm}
        \large\@author \\\vspace{0.5cm}
        \large\@date \vspace{0.5cm}
    \end{center}
}
\newcommand{\ulmaketitle}{
    \thispagestyle{plain}
    \noindent\LARGE\@title \\[0.2cm]
    \large\@author \vspace{0.5cm}
}
\newcommand{\udlmaketitle}{
    \thispagestyle{plain}
    \noindent\LARGE\@title \\[0.2cm]
    \large\@author \\[0.2cm]
    \large\@date \vspace{0.5cm}
}
\makeatother
\newcommand{\incfig}[2]{ % use pdf+tex files generated with Inkscape
    \def\svgwidth{#1}
    \import{./}{#2}
}
\newcommand{\incfigh}[2]{
    \def\svgheight{#1}
    \import{./}{#2}
}
\def\changemargin#1#2{\list{}{\rightmargin#2\leftmargin#1}\item[]} % temporarily change margin size
    \let\endchangemargin=\endlist
\newcommand{\furi}[2]{\ruby{#1}{#2}}
\newcommand{\zeropad}[1]{
    \ifnum#1<10\relax%
        0#1
    \else
        #1
    \fi
}
\newcommand{\defn}[1]{\textbf{Def.} \textit{#1}: \\}

%%% math %%%
\renewcommand{\eval}[1]{{#1}\bigg|}
\newcommand*{\defeq}{
    \mathrel{\vcenter{\baselineskip0.5ex \lineskiplimit0pt
    \hbox{\scriptsize.}\hbox{\scriptsize.}}}=
}
\newcommand{\mkeq}{\overset{!}{=}} % sign for setting things equal (! over =)
\newcommand{\inprod}[2]{\left<{#1},{#2}\right>} % Hilbert space inner product
\newcommand{\Lag}{\mathcal{L}} % Lagrangian
\newcommand{\Ham}{\mathcal{H}} % Hamiltonian
% \newcommand{\unit}{\mathds{1}}
\newcommand{\dbar}{d\hspace*{-0.15em}\bar{}\hspace*{0.15em}} % thermo crossed d
\newcommand{\pd}{\partial} % partial derivative d
\newcommand{\meas}[1]{\,\mathcal{D} {#1}\,}
\newcommand{\pphi}{\varphi} % curly one-stroke phi
\newcommand{\eeps}{\varepsilon} % curly one-stroke epsilon
\newcommand{\leads}{~\leadsto~} % squiggle arrow with proper horizontal spacing
\newcommand{\qleads}{\quad\leadsto\quad} % squiggle arrow with greater horizontal spacing
\newcommand{\sothen}{\rotatebox[origin=c]{180}{$\Lsh$}~}
\ifdefined\swap
    \renewcommand{\swap}{\leftrightarrow}
\else
    \newcommand{\swap}{\leftrightarrow}
\fi
\newcommand{\longswap}{\longleftrightarrow}
\newcommand{\dsm}{\displaystyle}
\newcommand{\tightoverset}[2]{ % overset more closely
    \overset{\text{\tiny${#1}$}}{\rule{0pt}{1.25ex}\smash{#2}}
}
\makeatletter
\newcommand{\setR}{\mathbb{R}} % standard sets of numbers
\newcommand{\setZ}{\mathbb{Z}}
\newcommand{\setC}{\mathbb{C}}
\newcommand{\setN}{\mathbb{N}}
\newcommand{\setQ}{\mathbb{Q}}
\newcommand{\numD}{\texttt{D}} % numerical finite difference operator
\newcommand*{\TT}{{\mkern-1.5mu\mathsf{T}}} % matrix transpose
\newcommand*{\CC}{{\mkern-1.5mu\mathsf{C}}} % set complement
\newcommand{\tld}[1]{\widetilde{#1}} % overset squiggle (tilde)
\newcommand{\pheq}{\phantom{=}} % for easy split lines in align environment
\newcommand{\pll}{\parallel}
\newcommand{\prp}{\perp}
\def\acom{\@ifstar\@acom\@@acom} % align comment
\def\@acom#1{\phantom{\leftarrow}~&\pbox{0.3\textwidth}{\raggedright #1}}
\def\@@acom#1{\leftarrow~&\pbox{0.3\textwidth}{\raggedright #1}}
\makeatother
\newcommand{\com}[1]{&\text{#1}} % comment using align
\DeclareMathOperator{\Span}{span} % span of a set of vectors
\DeclareMathOperator{\Null}{\mathcal{N}} % matrix null space
\DeclareMathOperator{\Range}{\mathcal{R}} % matrix range
\DeclareMathOperator{\diag}{diag} % diag
\DeclareMathOperator{\nul}{Nul} % matrix null space
\DeclareMathOperator{\col}{Col} % matrix column space
\DeclareMathOperator{\sgn}{sgn} % set signature
\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator*{\arcsinh}{arcsinh}
\DeclareMathOperator*{\arccosh}{arccosh}
\DeclareMathOperator*{\arctanh}{arctanh}
\DeclareMathOperator*{\arccsch}{arccsch}
\DeclareMathOperator*{\arcsech}{arcsech}
\DeclareMathOperator*{\arccoth}{arccoth}
\DeclareSIUnit{\gauss}{G}
\DeclareSIUnit{\electronvolt}{eV}
\DeclareSIUnit{\erg}{erg}
\DeclareSIUnit{\calorie}{cal}
\newcommand{\bilin}[1]{\stackrel{\leftrightarrow}{#1}} % overset a double-sided arrow for bilinear operators
\newcommand{\vect}[1]{\mathbf{#1}} % write vectors as bold letters, rather than using the hat
\newcommand{\I}{\bm{i}} % special notation for i in math
\newcommand{\Lim}{\lim\limits} % why the hell would \lim not already have limit spacing?!
\newcommand{\Sum}{\sum\limits}
\newcommand{\Prod}{\prod\limits}
\newcommand{\uB}[1]{\underbrace{#1}}
\newcommand{\ish}{\raise.17ex\hbox{$\scriptstyle\sim$}}
\newcommand{\set}[1]{\left\lbrace#1\right\rbrace} % sets
\newcommand{\st}{~\middle|~} % set notation 'such that'
\newcommand{\avg}[1]{\left<{#1}\right>} % <avg>
\newcommand{\uv}[1]{\indices{#1}}
\newcommand{\obar}[1]{ % better math bar
    {\mkern 2.5mu\overline{\mkern-2.75mu#1\mkern0.25mu}\mkern 1.25mu}
}
\newcommand{\ubar}[1]{\underaccent{\rule{1.1ex}{0.1ex}}{#1}}
\newcommand{\uhat}[1]{\underaccent{\hat}{#1}}
\newcommand{\mean}[1]{\obar{{#1}}} % average using the bar instead of angle brackets
\DeclarePairedDelimiter\myceil{\lceil}{\rceil}
\DeclarePairedDelimiter\myfloor{\lfloor}{\rfloor}
\makeatletter
\def\ceil{\@ifstar\@ceil\@@ceil}
\def\@ceil#1{\myceil{#1}}
\def\@@ceil#1{\myceil*{#1}}
\def\floor{\@ifstar\@floor\@@floor}
\def\@floor#1{\myfloor{#1}}
\def\@@floor#1{\myfloor*{#1}}
\makeatother
\makeatletter
\def\localbig#1#2{%
  \sbox\z@{$\m@th#1
    \sbox\tw@{$#1()$}%
    \dimen@=\ht\tw@\advance\dimen@\dp\tw@
    \nulldelimiterspace\z@\left#2\vcenter to1.2\dimen@{}\right.
  $}\box\z@}

\newcommand{\divides}{\mathrel{\mathpalette\dividesaux\relax}}
\newcommand{\ndivides}{\mathrel{\mathpalette\ndividesaux\relax}}

\newcommand{\dividesaux}[2]{\mbox{$\m@th#1\localbig{#1}|$}}
\newcommand{\ndividesaux}[2]{%
  \mkern.5mu
  \ooalign{%
    \hidewidth$\m@th#1\localbig{#1}|$\hidewidth\cr
    $\m@th#1\nmid$\cr%
  }%
}
\makeatother
\newcommand{\sci}[2]{{#1} \times 10^{#2}} % scientific notation
\newcommand{\svec}[1]{\overset{\leadsto}{#1}}
\newcommand{\uveci}{{\hat{\textnormal{\i}}}} % unit vector notation
\newcommand{\uvecj}{{\hat{\textnormal{\j}}}}
\newcommand{\uveck}{{\hat{\textnormal{k}}}}
\DeclareRobustCommand{\uvec}[1]{{
  \ifcsname uvec#1\endcsname
     \csname uvec#1\endcsname
   \else
     \hat{#1}
   \fi
}}
% align environment with only one equation number
\newenvironment{aligns}{
    \begin{equation}
        \begin{aligned}
    }
    {
        \end{aligned}
    \end{equation}
}
% gather environment with only one equation number
\newenvironment{gathers}{
    \begin{equation}
        \begin{gathered}
    }
    {
        \end{gathered}
    \end{equation}
}
\newcommand{\threej}[6]{
    \begin{pmatrix} {#1} & {#3} & {#6} \\ {#2} & {#4} & {#6} \end{pmatrix}
}
\newcommand{\sixj}[6]{
    \begin{Bmatrix} {#1} & {#2} & {#3} \\ {#4} & {#5} & {#6} \end{Bmatrix}
}

%%% tabular %%%
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}} % set-width columns
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{'}{@{\hspace*{1ex}}|@{\hspace*{1ex}}} % for augmented matrices
\newcolumntype{:}{>{\global\let\currentrowstyle\relax}} % enable the setting via \rowstyle of a particular
\newcolumntype{;}{>{\currentrowstyle}} % font style for a whole row; eg. :c;c;c;c
\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}#1\ignorespaces}
\newcommand{\retab}[2]{\multicolumn{1}{#1}{#2}}
\newcommand{\tabl}[1]{\retab{l}{#1}}
\newcolumntype{I}{>{$}l<{$}} % auto-math mode columns
\newcolumntype{J}{>{$}c<{$}}
\newcolumntype{K}{>{$}r<{$}}
