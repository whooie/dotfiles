#
# ~/.bashrc
#
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias wine32="WINEARCH=win32 WINEPREFIX=/home/whooie/.wine32 wine"
alias skype="apulse32 skype"
alias tty-clock="tty-clock -cs -C 7 -f '%a %Y.%m.%d'"
alias xboxdrv-roa="sudo xboxdrv -c /home/whooie/.config/xboxdrv-roa"
alias neofetch="neofetch --block_range '0' '15' --source '/home/whooie/Stuff/Picture Stuffs/Utility/Reaction/Animu/mugi_wtf.jpg'"
alias lsblk="lsblk -o NAME,SIZE,TYPE,STATE,FSTYPE,LABEL,MOUNTPOINT"
alias ponysay-o="ponysay --pony owlowiscious --wrap 150 --balloon round"
alias xargs-n="xargs -d '\n'"
alias sshfs-f="sshfs -F ~/.ssh/config -o reconnect -o allow_other"
alias scp-f="scp -F ~/.ssh/config"
alias sudo="TERM=rxvt sudo"
alias fucking="TERM=rxvt sudo"
alias please='sudo $(fc -ln -1)'
alias matlab-cli="matlab -nodesktop"
alias anger='echo ANGERY | toilet -tf bigmono9'
alias anger-cd='echo ANGERY-cd | toilet -tf bigmono9'
alias rager='echo RAGE | toilet -tf bigmono9'
alias rager-cd='echo RAGE-cd | toilet -tf bigmono9'
alias oneko-f='oneko -fg "#d8d8d8" -bg "#101012"'
alias oneko-g='oneko -fg "#101012" -bg "#d8d8d8"'
alias netctl-a='netctl-auto'
alias pnglatex-x='pnglatex -d 500 -H "macros.sty" -e "align*" -p "physics:amsmath:amssymb:mathtools:esint:bm:siunitx:stackengine" -O -P "5x5"'
alias pnglatex-a='pnglatex -d 500 -H "macros.sty" -e "align" -p "physics:amsmath:amssymb:mathtools:esint:bm:siunitx:stackengine" -O -P "5x5"'
# alias dots='git --git-dir=/home/whooie/.dots/ --work-tree=/home/whooie'
# alias dots-skip='git --git-dir=/home/whooie/.dots/ --work-tree=/home/whooie update-index --skip-worktree'
alias netctl-s='sudo netctl stop-all && sudo netctl start'
alias tmux-a='tmux -2 attach -t'
alias tmux-k='tmux kill-session -t'
alias tmux='tmux -2'
alias cargo-build='cargo build --release -Z unstable-options --out-dir .'
alias mpv-sf='mpv-s -f 22'
alias python-sy='PYTHONSTARTUP=/home/whooie/.pythonrc-sy python'
alias reswap='sudo swapoff /swapfile && sudo swapon /swapfile'
alias nls='nu -c "ls"'
alias ndu='nu -c "du -a"'
alias ndu-sort='nu -c "du -a | sort-by physical | reverse"'
alias ndu-10='nu -c "du -a | sort-by physical | reverse | first 10"'
alias nu-dev='nu --env-config /home/whooie/.config/nushell/env-dev.nu'
alias mpv-sync='syncplay --no-gui -a syncplay.pl:8999 --player-path $( which mpv ) --name whooie -r'
alias mpv-sync-s='syncplay --no-gui -a syncplay.pl:8999 --player-path $( which mpv ) --name whooie -r srcdfa'
alias clearall="clear && printf '\\e[3J'"
alias CL="clear && printf '\\e[3J' &&"
alias rclone-coveylab='rclone --vfs-cache-mode full mount "coveylab": --dir-cache-time 2m30s'
alias rclone-coveylab-notes='rclone --vfs-cache-mode full --dir-cache-time 10s mount "coveylab":Lab\ Notes/notes'
alias rclone-box='rclone --vfs-cache-mode writes mount "box": --dir-cache-time 2m30s'
alias sshfs-f-ew='sshfs -F ~/.ssh/config -o reconnect -o allow_other -o auto_cache -o no_readahead coveylab:/c/Users/EW/'
alias sshfs-f-labnotes='sshfs -F ~/.ssh/config -o reconnect -o allow_other caudex:mnt/coveylab-labnotes/notes'
alias clone='urxvt -pt Root &'

eval "$(thefuck --alias)"

set -o vi

export RANGER_LOAD_DEFAULT_RC=FALSE
export EDITOR=vim
export TERM=rxvt-unicode
export VMAIL_HTML_PART_READER='qutebrowser'
export VMAIL_BROWSER='qutebrowser'
export PYTHONSTARTUP='/home/whooie/.pythonrc'
export QT_LOGGING_RULES='*=false'
export PATH="$PATH:/home/whooie/.cargo/bin:/home/whooie/.local/bin"
export BROWSER="qutebrowser"
export WINEARCH=win32
export WINEPREFIX=/home/whooie/.wine-default
export HISTSIZE=""
export HISTFILESIZE=""
export XDG_CONFIG_HOME=/home/whooie/.config
export PYTHON_PRETTY_ERRORS=0

OPAM_SWITCH_PREFIX='/home/whooie/.opam/5.1.0'; export OPAM_SWITCH_PREFIX;
CAML_LD_LIBRARY_PATH='/home/whooie/.opam/5.1.0/lib/stublibs:/home/whooie/.opam/5.1.0/lib/ocaml/stublibs:/home/whooie/.opam/5.1.0/lib/ocaml'; export CAML_LD_LIBRARY_PATH;
OCAML_TOPLEVEL_PATH='/home/whooie/.opam/5.1.0/lib/toplevel'; export OCAML_TOPLEVEL_PATH;
MANPATH=':/home/whooie/.opam/5.1.0/man'; export MANPATH;
PATH='/home/whooie/.opam/5.1.0/bin:/home/whooie/.opam/default/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/lib/jvm/java-8-openjdk/jre/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/lib/rustup/bin:/home/whooie/.local/bin:/home/whooie/.pyenv/shims:/home/whooie/.pyenv/bin:/home/whooie/.cargo/bin:/home/whooie/.nu-whooie'; export PATH;

#export PYENV_ROOT="$HOME/.pyenv"
#export PYENV_VIRTUALENV_DISABLE_PROMPT=1
#export PATH="$PYENV_ROOT/bin:$PATH"
#eval "$(pyenv init --path)"
#eval "$(pyenv init -)"

eval `dircolors -b $HOME/.dir_colors`

#source /usr/share/doc/ranger/examples/shell_automatic_cd.sh
#bind '"\C-i":"ranger\C-m"'
source /home/whooie/.scripts/mklatexdoc.sh
function ranger-cd() {
    temp_file="$(mktemp -t "ranger_cd.XXXXXXXXXX")"
    ranger --choosedir="$temp_file" -- "${@:-$PWD}"
    if chosen_dir="$(cat -- "$temp_file")" && [ -n "$chosen_dir" ] && [ "$chosen_dir" != "$PWD" ]; then
        cd -- "$chosen_dir"
    fi
    rm -f -- "$temp_file"
}

# This binds Ctrl-O to ranger-cd:
bind '"\C-o":"ranger-cd\C-m"'

bind '"\C-p":"python\C-m"'

bind '"\C-n":"nu\C-m"'

function nudo() {
    nu -c "$@"
}

export GTK_THEME=Arc-Darker
export QUOTING_STYLE=literal

#source ~/.scripts/shell_prompt.sh

#PROMPT_COMMAND=__prompt_command
#__prompt_command() {
#    local EXIT="$?"
#    PS1="\[\e[38;5;245m\] ┄╌─[\[\e[0m\]\j\[\e[38;5;245m\]|\[\e[0m\]\W\[\e[38;5;245m\]]"
#
#    if [[ $EXIT != 0 ]]; then
#        PS1+="\[\e[38;5;160m\][\[\e[0m\]$EXIT\[\e[38;5;160m\]]\[\e[0m\] \$ "
#    else
#        PS1+="\[\e[0m\] $ "
#    fi
#}

#PS1='[\u@\h \W]\$ '

#PS1=' ┄╌── \W \$ '

PROMPT_COMMAND=__prompt_command
# __prompt_command() {
#     if [[ $shell_mode == "dev" ]]; then
#         prompt_start=" ┄╌─dev─"
#     else
#         prompt_start=" ┄╌──"
#     fi
#     local EXIT="$?"
#     if [[ $(jobs) != "" ]]; then
#         PS1="$prompt_start\[\e[0m\] \j "
#     else
#         PS1="$prompt_start\[\e[0m\] "
#     fi
#     PS1+="\W "
#     if [[ $EXIT != 0 ]]; then
#         PS1+="\[\e[38;5;160m\]$EXIT\[\e[0m\] \$ "
#     else
#         PS1+="\$ "
#     fi
# }
__prompt_command() {
    local EXIT="$?"
    PS1="\[\e[0m\]::"
    if [[ $(pwd) == $HOME ]]; then
        PS1+="[\W] "
    else
        PS1+="[ \W ] "
    fi
    if [[ $(jobs) != "" ]]; then
        PS1+="\[\e[36m\]\j\[\e[0m\] "
    fi
    if [[ $EXIT != 0 ]]; then
        PS1+="\[\e[38;5;160m\]$EXIT\[\e[0m\] "
    fi
    PS1+="\$ "
}

function dev() {
    shell_mode='dev' \
    HISTFILE='/home/whooie/.bash_history_dev' \
    HISTSIZE=1500 \
    HISTFILESIZE=1500 \
    bash --rcfile "/home/whooie/.bashrc-dev"
}

function undev() {
    if [[ "$shell_mode" == "dev" ]]; then
        exit
    fi
}

function mkcd(){
    mkdir "$1" && cd "$1"
}

#eval "$(starship init bash)"
