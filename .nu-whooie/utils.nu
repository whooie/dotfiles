# Super command for to-vec operations
def to-vec [] { }

# Convert a 1D list to a column vector
def "to-vec col" [] {
    let data = $in
    if ($data | length) == 0 {
        [[]]
    } else {
        $data | each { |X| [$X] }
    }
}

# Convert a 1D list to a row vector
def "to-vec row" [] {
    let data = $in
    if ($data | length) == 0 {
        [[]]
    } else {
        [$data]
    }
}

# Transpose a 2D list of lists as a matrix
def matrix-transpose [] {
    let data = $in
    if ($data | length) == 0 {
        error make { msg: "encountered empty dataset" }
    }
    if not ($data | all { |x| $x | describe | str starts-with "list" }) {
        error make { msg: "input stream type must be a list of lists" }
    }
    let Nvars = ($data.0 | length)
    if not ($data | all { |x| ($x | length) == $Nvars }) {
        error make { msg: "all rows must be of equal length" }
    }
    0..($Nvars - 1)
    | each { |k| $data | each { |row| $row | get $k } }
}

def _as-matrix-table_ [
    data: list
    numerical_columns: bool
] {
    let Nvars = ($data | length)
    let c = if $numerical_columns { "" } else { "c" }
    if $Nvars == 0 {
        error make { msg: "encountered empty dataset" }
    } else if not ($data.0 | describe | str starts-with "list") {
        $data | each { |colit| { $"($c)0": $colit } }
    } else if $Nvars == 1 {
        $data | get 0 | each { |colit| { $"($c)0": $colit } }
    } else {
        (_as-matrix-table_ ($data | first ($Nvars - 1)) $numerical_columns)
        | merge { ||
            $data
            | get ($Nvars - 1)
            | each { |colit| { $"($c)($Nvars - 1)": $colit } }
        }
    }
}

# Converts a 2D list of lists to a table in the form of a matrix
def as-matrix-table [
    --numerical-columns (-n) # Use bare numbers for column names
] {
    let data = $in
    if ($data | length) == 0 {
        error make { msg: "encountered empty dataset" }
    }
    if not ($data | all { |x| $x | describe | str starts-with "list" }) {
        error make { msg: "input stream type must be a list of lists" }
    }
    let Nvars = ($data.0 | length)
    if not ($data | all { |x| ($x | length) == $Nvars }) {
        error make { msg: "all rows must be of equal length" }
    }

    let transposed = (
        0..($Nvars - 1)
        | each { |k| $data | each { |row| $row | get $k } }
    )
    _as-matrix-table_ $transposed $numerical_columns
}

