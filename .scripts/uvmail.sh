#!/bin/bash

acc_dir="/home/whooie/.vmail/accounts"
acc_def="primary"
rcfile="/home/whooie/.vmailrc"
vmail_default="/home/whooie/.vmail/default"

print-help(){
    n="\e[0m"
    b="\e[1m"
    u="\e[4m"

    echo -e "Frontend for vmail."
    echo -e "Usage: uvmail [ -a account ] [ -g ]"
    echo -e "       uvmail [ -h ]"
    echo -e "Options:"
    echo -e "  -a account   Choose a mail account."
    echo -e "               Available accounts:"
    for dir in $acc_dir/*; do
        echo "                 ${dir#$acc_dir/}"
    done
    echo -e "  -g           Generate a list of contacts from the last 500 messages."
    echo -e "  -s file      Send the message contained in file using the account defined with -a and exit."
    echo -e "  -h           Print this text."
}

while [[ ${#@} -gt 0 ]]; do
    if [[ "${1:0:1}" == "-" ]]; then
        opt="${1:1}"
        for ((i = 0; i < ${#opt}; i++)); do
            case ${opt:$i:1} in
                a)
                    shift 1
                    case $1 in
                        1)
                            account="primary"
                            ;;
                        2)
                            account="secondary"
                            ;;
                        3)
                            account="school"
                            ;;
                        *|"")
                            if [[ -d $acc_dir/$1 ]]; then
                                account="$1"
                            else
                                echo "Invalid account."
                                print-help
                                exit 1
                            fi
                            ;;
                    esac
                    ;;
                g)
                    contacts=1
                    ;;
                s)
                    shift 1
                    file="$1"
                    ;;
                h)
                    print-help
                    exit 0
                    ;;
                *)
                    echo "Invalid option '-${opt:$i:1}'."
                    print-help
                    exit 1
                    ;;
            esac
        done
        shift 1
    else
        echo "Invalid operand '$1'."
        print-help
        exit 1
    fi
done
[[ -n "$account" ]] || account="$acc_def"
gpg -dq "$acc_dir/$account/$account.gpg" > "$rcfile"

#test "$file" && vmailsend < "$file"
test "$contacts" && vmail -g || vmail
rm "$rcfile"
rm -rf $vmail_default/*
