#!/bin/bash

dfscp(){ # 1:sourcedir 2:targetdir 3:offset
    if [[ ! -e "$2" ]]; then
        echo -e "$3${2##*/}/"
        mkdir "$2"
    else
        echo -e "\e[90m$3${2##*/}/"
    fi
    for item in "$1"/*; do
        if [[ -d "$item" ]] && [[ ! -z "$(ls "$item")" ]]; then
            dfscp "$item" "$2/${item##*/}" "$3  "
        else
            if [[ -e "$2/${item##*/}" ]]; then
                echo -e "\e[90m$3  ${item##*/}\e[0m"
            else
                echo -e "$3  ${item##*/}"
                cp "$item" "$2"
            fi
        fi
    done
}

dfscp "$1" "$2" ""
