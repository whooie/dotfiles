#!/bin/bash

id_num_line=$(xinput | grep Synaptics | awk '{FS=" "}{print $6}')
id_num=${id_num_line:3:4}
echo $id_num

prop_num_line=$(xinput --list-props $id_num | grep "Tapping Enabled (" | awk '{FS=" "}{print $4}')
prop_num=${prop_num_line:1:3}
echo $prop_num

xinput --set-int-prop $id_num $prop_num 8 1
