#!/usr/bin/python

import sys
import os
import os.path as path

first = r"""<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>images</title>
    <style>
*, *::after, *::before {
    box-sizing: border-box;
}

img {
    vertical-align: middle;
}

html, body {
    display: flex;
    background-color: #e8e6e6;
    height: 100%;
    width: 100%;
    padding: 0;
    margin: 0;
    font-family: sans-serif;
}

#list {
    height: 100%;
    overflow: auto;
    width: 260px;
    text-align: center;
}

#list img {
    width: 200px;
    padding: 10px;
    border-radius: 10px;
    margin: 15px 0;
    cursor: pointer;
}

#list img.current {
    background: #0003;
}

#image-container {
    flex: auto;
    height: 100vh;
    background: #222;
    color: #fff;
    text-align: center;
    cursor: pointer;
    -webkit-user-select: none;
    user-select: none;
    position: relative;
}

#image-container #dest {
    height: 100%;
    width: 100%;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
}

#image-container #page-num {
    position: absolute;
    font-size: 18pt;
    left: 10px;
    bottom: 5px;
    font-weight: bold;
    opacity: 0.75;
    text-shadow: /* Duplicate the same shadow to make it very strong */
        0 0 2px #222,
        0 0 2px #222,
        0 0 2px #222;
}
    </style>
</head>
<body>

<nav id="list">
"""

last = r"""</nav>

<div id="image-container">
    <span id="page-num"></span>
    <div id="dest"></div>
</div>

<script>
const pages = Array.from(document.querySelectorAll('img.image-item'));
let currentPage = 0;

function changePage(pageNum) {
    const previous = pages[currentPage];
    const current = pages[pageNum];

    if (current == null) {
        return;
    }
    
    previous.classList.remove('current');
    current.classList.add('current');

    currentPage = pageNum;

    const display = document.getElementById('dest');
    display.style.backgroundImage = `url("${current.src}")`;

    document.getElementById('page-num')
        .innerText = [
                (pageNum + 1).toLocaleString(),
                pages.length.toLocaleString()
            ].join('\u200a/\u200a');
}

changePage(0);

document.getElementById('list').onclick = event => {
    if (pages.includes(event.target)) {
        changePage(pages.indexOf(event.target));
    }
};

document.getElementById('image-container').onclick = event => {
    const width = document.getElementById('image-container').clientWidth;
    const clickPos = event.clientX / width;

    if (clickPos < 0.5) {
        changePage(currentPage - 1);
    } else {
        changePage(currentPage + 1);
    }
};

document.onkeypress = event => {
    switch (event.key.toLowerCase()) {
        // Previous Image
        case 'a':
            changePage(currentPage - 1);
            break;
        case 'w':
            changePage(currentPage - 1);
            break;
        case 's':
            changePage(currentPage + 1);
            break;
        case 'd':
            changePage(currentPage + 1);
            break;
        case 'h':
            changePage(currentPage - 1);
            break;
        case 'k':
            changePage(currentPage - 1);
            break;
        case 'j':
            changePage(currentPage + 1);
            break;
        case 'l':
            changePage(currentPage + 1);
            break;
    }// remove arrow cause it won't work
};

document.onkeydown = event =>{
    switch (event.keyCode) {
        case 37: //left
            changePage(currentPage - 1);
            break;
        case 38: //up
            changePage(currentPage - 1);
            break;
        case 39: //right
            changePage(currentPage + 1);
            break;
        case 40: //down
            changePage(currentPage + 1);
            break;
    }
};
</script>
</body>
</html>
"""

def main():
    images = list(filter(lambda x: path.splitext(x)[1] in [".png", ".jpg", ".gif", ".jpeg"], os.listdir()))
    images.sort()

    outfile = open("00index.html", 'w')
    outfile.write(first)
    for filename in images:
        outfile.write(f'<img src="{filename}" class="image-item"/>\n')
    outfile.write(last)
    outfile.close()
    return 0

if __name__ == "__main__":
    sys.exit(main())
