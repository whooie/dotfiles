" .vimrc
" See: http://vimdoc.sourceforge.net/htmldoc/options.html for details

" For multi-byte character support (CJK support, for example):
"set fileencodings=ucs-bom,utf-8,cp936,big5,euc-jp,euc-kr,gb18030,latin1
       
set tabstop=4       " Number of spaces that a <Tab> in the file counts for.
 
set shiftwidth=4    " Number of spaces to use for each step of (auto)indent.
 
set expandtab       " Use the appropriate number of spaces to insert a <Tab>.
                    " Spaces are used in indents with the '>' and '<' commands
                    " and when 'autoindent' is on. To insert a real tab when
                    " 'expandtab' is on, use CTRL-V <Tab>.
 
set smarttab        " When on, a <Tab> in front of a line inserts blanks
                    " according to 'shiftwidth'. 'tabstop' is used in other
                    " places. A <BS> will delete a 'shiftwidth' worth of space
                    " at the start of the line.
 
set showcmd         " Show (partial) command in status line.

set number          " Show line numbers.

set showmatch       " When a bracket is inserted, briefly jump to the matching
                    " one. The jump is only done if the match can be seen on the
                    " screen. The time to show the match can be set with
                    " 'matchtime'.
 
set hlsearch        " When there is a previous search pattern, highlight all
                    " its matches.
 
set incsearch       " While typing a search command, show immediately where the
                    " so far typed pattern matches.
 
set ignorecase      " Ignore case in search patterns.
 
set smartcase       " Override the 'ignorecase' option if the search pattern
                    " contains upper case characters.
 
set backspace=2     " Influences the working of <BS>, <Del>, CTRL-W
                    " and CTRL-U in Insert mode. This is a list of items,
                    " separated by commas. Each item allows a way to backspace
                    " over something.
 
set autoindent      " Copy indent from current line when starting a new line
                    " (typing <CR> in Insert mode or when using the "o" or "O"
                    " command).
 
set textwidth=0     " Maximum width of text that is being inserted. A longer
                    " line will be broken after white space to get this width.
set wrapmargin=0
set wrap
set nolinebreak
 
set formatoptions=cqto " This is a sequence of letters which describes how
                    " automatic formatting is to be done.
                    "
                    " letter    meaning when present in 'formatoptions'
                    " ------    ---------------------------------------
                    " c         Auto-wrap comments using textwidth, inserting
                    "           the current comment leader automatically.
                    " q         Allow formatting of comments with "gq".
                    " r         Automatically insert the current comment leader
                    "           after hitting <Enter> in Insert mode. 
                    " t         Auto-wrap text using textwidth (does not apply
                    "           to comments)
 
set ruler           " Show the line and column number of the cursor position,
                    " separated by a comma.
 
set background=dark " When set to "dark", Vim will try to use colors that look
                    " good on a dark background. When set to "light", Vim will
                    " try to use colors that look good on a light background.
                    " Any other value is illegal.
 
set mouse=a         " Enable the use of the mouse.

set tabpagemax=0

augroup vimrc
    "autocmd BufEnter * highlight OverLength ctermbg=black
    "autocmd BufEnter * match OverLength /\%101v.*/
    "autocmd BufEnter,BufRead * set tw=0
    "autocmd BufEnter,BufRead * set wrapmargin=0
    "autocmd BufEnter,BufRead * set nolinebreak
    set nospell
    set foldcolumn=0
    set foldmethod=manual
augroup END

" folding
command ToggleFoldColumn if &foldcolumn == 0 | set foldcolumn=1 | else | set foldcolumn=0 | endif
map zs <Esc>:ToggleFoldColumn<CR>
map ZZ <Esc>:ToggleFoldColumn<CR>

" compile LaTeX file
command LatexC  ![[ "%" == *".tex" ]] && if [[ -d ./build ]]; then xelatex -output-directory=./build -shell-escape "%"; else xelatex -shell-escape "%"; fi
map <C-l>c <Esc>:LatexC<CR>
" view compiled pdf
command LatexV  silent ![[ -d ./build ]] && ([[ -e "build/$(echo % | sed 's/\.tex$//').pdf" ]] && zathura "build/$(echo % | sed 's/\.tex$//').pdf" &) || ([[ -e "$(echo % | sed 's/\.tex$//').pdf" ]] && zathura "$(echo % | sed 's/\.tex$//').pdf" &)
map <C-l>v <Esc>:LatexV<CR>
" compile LaTeX with latexmk
command LatexMK silent ![[ "%" == *".tex" ]] && if [[ -d ./build ]]; then urxvt -e latexmk -output-directory=./build -pdf -xelatex -pvc -new-viewer -time "%" &; else urxvt -e latexmk -pdf -xelatex -pvc -new-viewer -time "%" &; fi
map <C-l>m <Esc>:LatexMK<CR>

" rmarkdown equivalents
"command RmdC !test "$(echo %)" == "$(echo % | sed 's/\.rmd$//').rmd" && R -e "rmarkdown::render('%')"
"map <C-R>c <Esc>:RmdC<CR>
"command RmdV silent !test -e "$(echo % | sed 's/\.rmd$//').pdf" && zathura "$(echo % | sed 's/\.rmd$//').pdf" &
"map <C-R>v <Esc>:RmdV<CR>

" show/hide vertical bar at the 81st character on a line
command CCShow set cc=81
map <C-C>s <Esc>:CCShow<CR>
command CCHide set cc=0
map <C-C>h <Esc>:CCHide<CR>

" show/hide spellcheck
command SpellShow set spell spelllang=en
command SpellHide set nospell

" turn wrapping on/off
command WrapOn set tw=80 | set wrapmargin=80
map AWi <Esc>:WrapOn<CR>
map AWI <Esc>:WrapOn<CR>a
command WrapOff set tw=0 | set wrapmargin=0
map AWo <Esc>:WrapOff<CR>
map AWO <Esc>:WrapOff<CR>a

" clear search highlighting
command ClearSearch let @/ = ""
map C/ <Esc>:let @/ = ""<CR>

" function to wrap a visual selection to max 80 characters on each line, broken
" at word boundaries
function RewrapRange() range
    for linenum in range(a:firstline, a:lastline-1)
        call setline(a:firstline, getline(a:firstline)." ".getline(a:firstline+1))
        call deletebufline(bufname(), a:firstline+1)
    endfor
    :s/\v([^ \^])(    |\t)* /\1 /ge
    :s/\v[ ]*$//ge
    while (len(getline(".")) > 80)
        normal! 0
        " Find the first white-space character before the 82nd character.
        call search('\(\%82v.*\)\@<!\s\(.*\s.\{-}\%82v\)\@!', 'c', line('.'))
        " Replace it with a new line.
        exe "normal! r\<CR>"
        " If the next line has words, join it to avoid weird paragraph breaks.
        "if (getline(line('.')+1) =~ '\w')
        "   normal! J
        "endif
    endwhile
    " Trim any accidental trailing whitespace
    :s/\s\+$//e
endfunction

command! -range Rewrap <line1>,<line2>call RewrapRange()

let fortran_fixed_source=1
"let fortran_more_precise=1
let fortran_do_enddo=1
"let fortran_indent_less=1
if has('syntax')
    syntax on
endif
if has('filetype')
    filetype plugin indent on
endif
"syntax enable
"filetype on
"filetype plugin on
"filetype plugin indent on

syntax on
colorscheme cosmos
set whichwrap=b,s,<,>,[,]
set clipboard=unnamed
set clipboard=unnamedplus
set clipboard+=unnamed
set clipboard+=unnamedplus
set go+=a
set cursorline
set scrolloff=1
set hid
nnoremap [t <Esc>:tabprev<CR>
nnoremap ]k <Esc>:tabnext<CR>
nnoremap +t <Esc>:tabnew
nnoremap -t <Esc>:tabclose<CR>
nnoremap _t <Esc>:tabclose!<CR>
nnoremap +b <Esc>:enew 
nnoremap [b <Esc>:bp<CR>
nnoremap ]b <Esc>:bn<CR>
nnoremap -b <Esc>:bd<CR>
nnoremap _b <Esc>:bd!<CR>
nnoremap {w <Esc>:wincmd j<CR>
nnoremap }w <Esc>:wincmd k<CR>
nnoremap [w <Esc>:wincmd h<CR>
nnoremap ]w <Esc>:wincmd l<CR>
nnoremap -w <Esc>:wincmd q<CR>
nnoremap _w <Esc>:wincmd q!<CR>
nnoremap +w <Esc>:vsp 
nnoremap =w <Esc>:sp 
nnoremap J 5<CR>
nnoremap K 5-
nnoremap L <C-E>M
nnoremap H <C-Y>M
nmap q b
vmap q b
imap <F1> <Esc>:WrapOff<CR>a
nmap <F1> <Esc>:echo strftime('%c')<CR>
imap <F2> <Esc>:WrapOn<CR>a
imap <F3> <Esc>
vmap <F3> :Rewrap<CR>
imap <F4> <Esc>
nmap <F4> <Esc>:set list!<CR>
vmap <F5> :'<,'>MDTableArray<CR>
imap <F5> <Esc>l%i
imap <F6> <Esc>
imap <F7> <Esc>:WrapOff<CR>
imap <F8> <Esc>:WrapOn<CR>
imap <F9> <Esc>
vmap <F9> :Rewrap<CR>
imap <F10> <Esc>
imap <F11> <Esc>
nmap <F11> <Esc>:TagbarToggle<CR>
imap <F12> <Esc>
nmap <F12> <Esc>:NERDTreeToggle<CR>
map ,tw <Esc>:set tw=0<CR>
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
set laststatus=2
set statusline+=%F
let g:airline_theme='cosmos'
"let g:airline_left_sep = '▒░'
"let g:airline_right_sep = '░▒'
"let g:airline_left_sep = '▒'
"let g:airline_right_sep = '▒'
let g:airline_left_sep = ''
let g:airline_right_sep = ''
set t_Co=256
let g:airline_powerline_fonts=1
let g:promptline_theme = 'cosmos'
let g:promptline_preset = 'cosmos'
"let g:pandoc#spell#enabled = 0
"let g:pandoc#folding#fdc = 0
"let g:pandoc#modules#disabled = ["folding"]
let g:tex_nospell=0
let g:tex_comment_nospell=1
au BufNewFile,BufRead *.tex set filetype=tex | set tw=80 | set wrapmargin=80 | set spell spelllang=en | sy spell toplevel
au BufNewFile,BufRead *.cls set filetype=tex
au BufNewFile,BufRead *.sty set filetype=tex
au BufNewFile,BufRead *.txt set filetype=text
au BufNewFile,BufRead *.plt set filetype=gnuplot
au BufNewFile,BufRead *.m set filetype=matlab
au BufNewFile,BufRead *.mat set filetype=matlab
au BufNewFile,BufRead *.oct set filetype=octave
au BufNewFile,BufRead *.mma set filetype=mma
au BufNewFile,BufRead *.wls set filetype=mma
au BufNewFile,BufRead *.mail set filetype=mail | set spell spelllang=en
au BufNewFile,BufRead aerc*.eml set tw=0
au BufNewFile,BufRead *.pnglatex set filetype=pnglatex
au BufNewFile,BufRead *.md set filetype=markdown | set spell spelllang=en
au BufNewFile,BufRead *.rst set tabstop=3 | set shiftwidth=3 | set spell spelllang=en | set tw=80 | set wrapmargin=80
au BufWinLeave * silent! mkview
au BufWinEnter * silent! loadview
au BufNewFile,BufRead *.rmd set foldcolumn=0

au BufNewFile,BufEnter,BufRead * set foldmethod=indent | set foldlevel=10 | set foldcolumn=0 | set cc=81 | silent! loadview | set foldtext=MyFoldText()
hi FoldColumn ctermfg=White ctermbg=Black
hi Folded ctermfg=Magenta ctermbg=None
function MyFoldText()
    let linetextstart = substitute(getline(v:foldstart), '^\s*', '', '')
    let linetextend = substitute(getline(v:foldend), '^\s*', '', '')
    let startnr = string(v:foldstart)
    let endnr = string(v:foldend)
    let nrlines = string(v:foldend - v:foldstart + 1)
    let fl = string(v:foldlevel)
    return v:folddashes . " " . endnr . " " . "(" . nrlines . ")" . " // " . linetextstart . " ... " . linetextend . ""
endfunction
set conceallevel=0
set fillchars=fold:\ 
set fillchars=foldclose:=
"au BufWritePre /media/killian/** :!mv "%" "%.old"
let g:mma_candy = 0

set encoding=utf-8
set fileencoding=utf-8
set fileencodings=ucs-mob,utf-8,prc

augroup vmail | exe "au BufEnter,BufRead compose_message.txt set tw=0" | augroup END

set maxmempattern=10000

"snippets
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetDirectories=['/home/whooie/.vim/UltiSnips']

"tagbar
let g:tagbar_autofocus = 0
let g:tagbar_autoclose = 0

"rust.vim
let g:rust_recommended_style = 0
let g:rust_fold = 0
let g:rust_use_default_ctags_defs = 1

"sleuth
let g:sleuth_automatic = 0

set listchars=eol:$,tab:--,extends:>,precedes:<,space:·

"vim-visual-multi
let g:VM_mouse_mappings = 1

"pyindent
let g:pyindent_open_paren = 'shiftwidth()'
let g:pyindent_nested_paren = 'shiftwidth()'
let g:pyindent_continue = 'shiftwidth()'

"vim-markdown
let g:vim_markdown_math = 1
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 2

" NERDTree
let g:NERDTreeGlyphReadOnly = "R"
let g:NERDTreeNodeDelimiter = "-"

