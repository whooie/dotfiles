"""
Some nice shell functions from within ipython.
"""

import os

def pwd():
    return os.getcwd()

def cd(path):
    os.chdir(path)

def shell(command):
    os.system(command)

def run(filename):
    os.system('python '+filename)

