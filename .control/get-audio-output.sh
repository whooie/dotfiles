#!/bin/bash

if [[ $(amixer | sed ':a;N;$!ba;s/\n/ /g' | sed 's/Simple/\n/g' | grep "Speaker") = *"[on]"* ]]; then 
    echo "Speaker"
elif [[ $(amixer | sed ':a;N;$!ba;s/\n/ /g' | sed 's/Simple/\n/g' | grep "Headphone") = *"[on]"* ]]; then
    echo "Headphone"
fi
