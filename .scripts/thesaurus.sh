#!/bin/sh

word=$1
case $2 in
    "")
        dict -d moby-thesaurus "$word"
        ;;
    pon)
        dict -d moby-thesaurus "$word" | ponysay --pony owlowiscious --wrap 150 --balloon round
        ;;
esac
