#!/bin/bash

print-help(){
    echo -e "Mounts virtual SD cards. Must be run as root."
    echo -e "Usage: \e[1mdolphin-sd\e[0m \e[4mvirtual SD card file\e[0m \e[4mmount point\e[0m"
    echo -e "       \e[1mdolphin-sd\e[0m [ -h|--help ]"
}

case $1 in
    "")
        print-help
        exit 1
        ;;
    -h|--help)
        print-help
        exit 0
        ;;
    *)
        mount -o defaults,umask=000 $1 $2
        ;;
esac
